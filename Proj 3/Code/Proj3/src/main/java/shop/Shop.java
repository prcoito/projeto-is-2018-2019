package shop;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import data.Product;
import kafkaProperties.KafkaProperties;

public class Shop {
	static EntityManager em;
	static KafkaProducer<String, String> producer;
	static String shopName = "ShopName";
	
	public static void main(String[] args) {
		
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Proj3Database");
    	em = emf.createEntityManager();
    	
		String topicName = "Shipments_Topic";
		
		Properties props = KafkaProperties.getProperties(Shop.class.getName());
		
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        producer = new KafkaProducer<>(props);
        
		//Kafka Consumer subscribes list of topics here.
		consumer.subscribe(Arrays.asList(topicName, "Purchases_Topic"));
	
		//print the topic name
		System.out.println("Subscribed to topic "+ Arrays.asList(topicName, "Purchases_Topic") );
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				consumer.close();
				producer.close();
				em.clear();
				emf.close();
			}
		}));
		
		
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
			for (ConsumerRecord<String, String> record : records)
			{
				if (record.topic().equals(topicName)) {
					processReceivedShippment(record);
				}
				else if(record.topic().equals("Purchases_Topic")) {
					processOrder(record);
					checkStocks();
				}
				else {
					System.out.println("!!! ATENTTION !!! " + record.topic()+ " yet to implement.");
				}
			}
		}
	
	}

	private static void checkStocks() {
		@SuppressWarnings("unchecked")
		List<Product> productList = em.createNamedQuery("Product.getAllProducts").getResultList();
		
		for (Product p : productList) {
			if ( p.getStock() < p.getMaxStock() * 0.25 ) {
				System.out.println("Stock of " + p.getName() + " is inferior to 25% of max stock. Reordering now...");
				orderProductToSupplier(p.getName(), p.getMaxStock() - p.getStock());
				System.out.println("Reorder complete.");
			}
		}
		
	}

	private static void orderProductToSupplier(String product, int quantity) {
		producer.send(new ProducerRecord<String, String>("Reorder_Topic", product, Integer.toString(quantity)));
	}

	private static void processReceivedShippment(ConsumerRecord<String, String> record) {
		String product 	= record.key();
		int quantity 	= Integer.parseInt(record.value().split(";")[0]);
		double price	= Double.parseDouble(record.value().split(";")[1]);
		
		System.out.println("Received shippment of " + product + ": " + quantity +" at price " + price);					
		
		// insert shipment into database;
		EntityTransaction t = em.getTransaction();
		t.begin();
		Product p = em.find(Product.class, product);
		if (p == null) {
			System.out.println("Adding new product ("+product+") to database");
			p = new Product(product);
			em.persist(p);
		}
		
		p.setPrice(Math.round((price + 0.3*price) *100.0 )/100.0); // para que fique com 2 menos casas decimais
		p.setStock(p.getStock() + quantity );
				
		em.merge(p);
		
		t.commit();
	}

	private static void processOrder(ConsumerRecord<String, String> record) {
		// record = [ key = topicToSend, value = Order ]
		// Order = "(Product_i:Quantity_i\n)+"    for all i = 0 ... inf
		String topicToSend 	= record.key();
		String toProcess	= record.value();
		System.out.println("Received Order from "+topicToSend + " with: "+ toProcess);
		
		String[] productsAndQuantities = toProcess.split("\n");
		
		String product;
		int quantity;
		double price;
		Product p;
		SimpleDateFormat df = new SimpleDateFormat( "dd-MM-yyyy" );
		
		for(String pq : productsAndQuantities) {
			product 	= pq.split(":")[0];
			quantity	= Integer.parseInt( pq.split(":")[1] );
			price		= Double.parseDouble( pq.split(":")[2] );
			
			System.out.println("Checking availability of "+product + " quantity "+quantity);
			
			// check availability
			p = em.find(Product.class, product);
			// if not available
			if ( p==null || p.getStock() < quantity )
			{
				System.out.println("Item "+ product + "not avaiable. Ordering now..." );
				// 		order to suplier
				orderProductToSupplier(p.getName(), p.getMaxStock() - p.getStock());
				
				try {
					Thread.sleep(1000);		// simulation of waiting for order to arrive
				} catch (InterruptedException e) {
					
				}
				p = em.find(Product.class, product);
			}
			// continue processing
			if ( price < p.getPrice() ) {
				producer.send(new ProducerRecord<String, String>(topicToSend, shopName,
						"Order rejected because price given to product " + product + " is inferior than labeled at the time " + df.format(Calendar.getInstance().getTime()) ));
				return ;
			}
		}
		
		System.out.println("Sending order to costumer "+topicToSend);
		
		
		
		producer.send(new ProducerRecord<String, String>(topicToSend, "ShopName",
														"Order sent at " + df.format(Calendar.getInstance().getTime()) ));
		
		// Update database and send to Sales_Topic the Sale
		EntityTransaction t = em.getTransaction();
		t.begin();
		for(String pq : productsAndQuantities) {
			product 	= pq.split(":")[0];
			quantity	= Integer.parseInt( pq.split(":")[1] );
			price		= Float.parseFloat( pq.split(":")[2] );
			
			p = em.find(Product.class, product);
			p.setStock(p.getStock() - quantity);
			em.merge(p);
			
			producer.send(new ProducerRecord<String, String>("Sales_Topic", product, quantity+";"+price) );
		}
		t.commit();
		System.out.println("Order sent sucessfuly");
		
		
	}

}
