package owner;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import data.Product;
import kafkaProperties.KafkaProperties;

public class Owner {
	private static final String TOPIC_NAME 	= "Reorder_Topic";
	private static final int 	EXIT 		= 0;
	private static final int 	NEW_ORDER 	= 1;
	private static final int 	AUTO		= 2;
	
	
	private static EntityManagerFactory emf ;
	private static EntityManager em ;
	private static KafkaProducer<String, String> producer;
	
	
	public static void main(String[] args) {
		emf = Persistence.createEntityManagerFactory("Proj3Database");
    	em = emf.createEntityManager();
		
    	resetDatabase();
    	
		producer = new KafkaProducer<>(KafkaProperties.getProperties(Owner.class.getName()));
		
		int op = 100;
		Scanner sc = new Scanner(System.in);
		
		do {
			try{
				System.out.println("0 - Sair");
				System.out.println("1 - Nova encomenda");
				System.out.println("2 - Auto");
				System.out.println("Opcao: ");
				
				op = Integer.parseInt( sc.nextLine() );
			
				switch (op) {
					case EXIT:
						break;
					case NEW_ORDER:
						newOrder(sc);
						break;
					case AUTO:
						sendDefaultOrder();
						break;
					default:
						System.out.println("Opcao inserida é invalida.");
						break;
					}
				
			}catch (NumberFormatException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		} while(op!=EXIT);
		
		sc.close();

		producer.close();

		em.close();
		emf.close();
		
		//System.out.println("Done");

	}

	private static void newOrder(Scanner sc) {
		
		System.out.print("Insira o nome do produto e a quantidade a encomendar e o stock maximo para esse produto: ");
		String input = sc.nextLine();
		String[] info = input.split(" ");
		try {
			
			String product	= info[0];
			int quantity 	= Integer.parseInt(info[1]);
			int maxStock	= Integer.parseInt(info[2]);
			
			setMaxStock(product, maxStock);
			producer.send(new ProducerRecord<String, String>(TOPIC_NAME, product, Integer.toString(quantity)));
			
		} catch(Exception e) {
			System.out.println("Erro!\nInput incorreto.");
		}
		
	}

	private static void sendDefaultOrder() {
		int maxStock = 100;
		String[] listProducts = {"Banana", "Pera", "Maca", "Uva", "Pessego" }; 
		for (String product : listProducts)
		{
			setMaxStock(product, maxStock);
			producer.send(new ProducerRecord<String, String>(TOPIC_NAME, product, Integer.toString(maxStock) ));
		}
	}
	
	private static void resetDatabase() {
		EntityTransaction t = em.getTransaction();
		t.begin();
		Query q = em.createNamedQuery("Product.deleteAll");
		q.executeUpdate();
		t.commit();
	}

	private static void setMaxStock(String product, int maxStock) {
		
    	EntityTransaction t = em.getTransaction();
		t.begin();
		Product p = em.find(Product.class, product);
		if (p == null) {
			System.out.println("Adding new product ("+product+") to database");
			p = new Product(product);
			em.persist(p);
			System.out.println("Product ("+product+") added to database");
		}
		
		p.setMaxStock( maxStock );
		p.setStock( 0 );		
		em.merge(p);
		
		t.commit();
		
	}

}
