package kafkaProperties;

import java.util.Properties;

public class KafkaProperties {
	
	private KafkaProperties() {}
	
	public static Properties getProperties(String groupID) {
		// create instance for properties to access producer configs   
		Properties props = new Properties();
		  
		//Assign localhost id
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", 			groupID);
		
		//Set acknowledgements for producer requests.      
		props.put("acks", "all");
		  
		//If the request fails, the producer can automatically retry,
		props.put("retries", 0);
		  
		//Specify buffer size in config
		props.put("batch.size", 16384);
		  
		//Reduce the no of requests less than 0   
		props.put("linger.ms", 1);
		  
		//The buffer.memory controls the total amount of memory available to the producer for buffering.   
		props.put("buffer.memory", 33554432);
		  
		props.put("key.serializer",		"org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", 	"org.apache.kafka.common.serialization.StringSerializer");
		props.put("key.deserializer",	"org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");  
		
		
		//props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		//props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		//props.put(StreamsConfig.APPLICATION_ID_CONFIG, "Rest");
		
		return props;
	}
}
