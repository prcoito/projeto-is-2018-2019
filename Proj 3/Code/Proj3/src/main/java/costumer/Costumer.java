package costumer;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import kafkaProperties.KafkaProperties;

public class Costumer {
	public static void main(String[] args) throws InterruptedException {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Proj3Database");
		EntityManager em = emf.createEntityManager();
    	
    	
    	//Assign topicName to string variable
		String topicName = "Purchases_Topic";
		  
		// create instance for properties to access producer configs   
		Properties props = KafkaProperties.getProperties(Costumer.class.getName()); 
		
		CostumerProducer producer = new CostumerProducer(props);
		    
		Scanner sc = new Scanner(System.in);
		String consumerTopicName ;
		do {
			System.out.print("Insira o Nome do topico a receber mensagem: ");
			consumerTopicName = sc.nextLine();
			
			if(consumerTopicName.isEmpty())
				System.out.println("ERRO!\nNome inserido invalido.");
			
		} while(consumerTopicName.isEmpty());
		
		CostumerConsumer consumer = new CostumerConsumer(sc, consumerTopicName, props);  
		consumer.start();
		
		Thread.sleep(100);
		
		String message;
		StringBuilder order = new StringBuilder();
		String option;
		
		int product;
		int quantity;
		double price;
		
		while(true) {
			order.setLength(0);
			
			do{
				// fetch from database list of products and list to the user (  )
				@SuppressWarnings("unchecked")
				List<String> productList = (List<String>) em.createNamedQuery("Product.getAllProductsNames").getResultList();
				
				int i = 0;
				for(String p : productList)
					System.out.println( (i++) +"- "+p);
				
				System.out.print("Insira o numero do produto, a quantidade e o preço ou Enter para concluir a ordem: ");
				message = sc.nextLine();
				
				try {
					product	= Integer.parseInt	(message.split(" ")[0]);
					quantity= Integer.parseInt	(message.split(" ")[1]);
					price	= Double.parseDouble(message.split(" ")[2]);
					order.append( productList.get(product)+":"+quantity+":"+price + "\n");  
				
				} catch(ArrayIndexOutOfBoundsException | NumberFormatException e) {
					if (!message.isEmpty())
						System.out.println("Erro! Input incorreto.");
				}
				
				
			} while( !message.isEmpty() );
			
			
			if ( !order.toString().isEmpty() )
			{
				//System.out.println("[DEBUG] sending order = "+order.toString());
				producer.sendOrder(topicName, consumerTopicName , order.toString());
				System.out.println("Ordem enviada com sucesso.");
			}
			 
			do{
				System.out.print("Pretende sair (s/n)? ");
				option = sc.nextLine().toLowerCase();
				
			} while(!option.equals("s") && !option.equals("n"));
			
			if (option.equals("s"))
				break;
		}
		  
		
		em.close();
		emf.close();
		sc.close();
		producer.close();
		consumer.shutdown();
		
	}
}
