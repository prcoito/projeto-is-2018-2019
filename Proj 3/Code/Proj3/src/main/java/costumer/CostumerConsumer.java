package costumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;

class CostumerConsumer extends Thread{
	Scanner sc;
	String topicName;
	Properties props;
	KafkaConsumer<String, String> consumer;
	
	public CostumerConsumer(Scanner scIn, String topicName, Properties p) {
		this.sc = scIn;
		this.topicName = topicName;
		this.props = p;
		consumer = new KafkaConsumer<>(props);
	}
	
	@Override
	public void run() {
		try {
			//Kafka Consumer subscribes list of topics here.
			consumer.subscribe(Arrays.asList(topicName));
			
			//print the topic name
			System.out.println("Subscribed to topic "+ topicName);
			
			while (true) {
				ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord<String, String> record : records)
				{
					System.out.println("\nMessage received from " + record.key() + ": " + record.value()+"\n");		
				}
			}
		} catch (WakeupException e) {
			stopConsumer();
		}
	}
	public void shutdown() {
	    consumer.wakeup();
	}
	
	public void stopConsumer() {
		consumer.close();
		
	}
}