package costumer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class CostumerProducer {
	KafkaProducer<String, String> producer;
	
	public CostumerProducer(Properties props){
		producer = new KafkaProducer<>(props);
	}
	
	public void sendOrder(String topicName, String receiveTopic, String order) {
		producer.send(new ProducerRecord<String, String>(topicName, receiveTopic,  order ));
	}

	public void close() {
		producer.close();
	}
	
}
