package suplier;

import java.time.Duration;
import java.util.Random;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class SuplierAuto extends Thread {
	private static final String TOPIC_NAME 	= "Shipments_Topic";
	KafkaConsumer<String, String> consumer;
	KafkaProducer<String, String> producer;
	
	public SuplierAuto(KafkaConsumer<String, String> c, KafkaProducer<String, String> p) {
		consumer = c;
		producer = p;
	}
	
	@Override
	public void run() {
		Random r = new Random();
		double price;
		try {
			while (true) {
				ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord<String, String> record : records)
				{
					System.out.println("Received order of " + record.key() + ": " + record.value());
					
					// price between 0.0 and 100.0
					price = Math.round( (r.nextDouble()*r.nextInt(100))*100.0 )/100.0;
					
					System.out.println("Sending order of " + record.key() + ": " + record.value() + " at price "+ price +" to Shop");
					
					producer.send(new ProducerRecord<String, String>(TOPIC_NAME, record.key(), record.value()+";"+price ));
				}		
			}
		} catch(Exception e) {
			//
		}
	}
}
