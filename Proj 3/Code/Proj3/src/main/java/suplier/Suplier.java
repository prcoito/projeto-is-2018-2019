package suplier;

import java.time.Duration;
import java.util.Arrays;
import java.util.Scanner;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import kafkaProperties.KafkaProperties;

public class Suplier {
	private static final String TOPIC_NAME 	= "Shipments_Topic";
	private static final int 	EXIT 		= 0;
	private static final int 	NEW_ORDER 	= 1;
	private static final int 	AUTO		= 2;
	
	private static KafkaConsumer<String, String> consumer;
	private static KafkaProducer<String, String> producer;
	
	public static void main(String[] args) {
		
		
		//TODO fazer interface para resposta de pedidos
		
		consumer = new KafkaConsumer<>(KafkaProperties.getProperties(Suplier.class.getName()));
		producer = new KafkaProducer<>(KafkaProperties.getProperties(Suplier.class.getName()));
		
		//Kafka Consumer subscribes list of topics here.
		consumer.subscribe(Arrays.asList("Reorder_Topic"));
	
		//print the topic name
		System.out.println("Subscribed to topic Reorder_Topic");
		
		int op = 100;
		Scanner sc = new Scanner(System.in);
		
		do {
			try{
				System.out.println("0 - Sair");
				System.out.println("1 - responder a encomendas");
				System.out.println("2 - Auto");
				System.out.println("Opcao: ");
				
				op = Integer.parseInt( sc.nextLine() );
			
				switch (op) {
					case EXIT:
						break;
					case NEW_ORDER:
						respondToOrders(sc);
						break;
					case AUTO:
						System.out.println("Prima enter para terminar modo automatico");
						SuplierAuto a = new SuplierAuto(consumer, producer);
						a.start();
						sc.nextLine();
						a.interrupt();
						checkConsumerProducer();
						break;
					default:
						System.out.println("Opcao inserida é invalida.");
						break;
					}
				
			}catch (NumberFormatException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		} while(op!=EXIT);
		
		sc.close();
		consumer.close();
		producer.close();
		
	}

	private static void checkConsumerProducer() {
		if (consumer == null)
			consumer = new KafkaConsumer<>(KafkaProperties.getProperties(Suplier.class.getName()));
		if (producer == null)
			producer = new KafkaProducer<>(KafkaProperties.getProperties(Suplier.class.getName()));
	}

	private static void respondToOrders(Scanner sc) {
		double price;
		
		while (true) {
			ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord<String, String> record : records)
			{
				System.out.println("Received order of " + record.key() + ": " + record.value());
				
				System.out.println("Insira o preço a que pretende verder o producto " + record.key());
				do {
				
					try{
						price = Math.round( Double.parseDouble(sc.nextLine()) *100.0 )/100.0;
					} catch (Exception e) {
						System.out.println("Preço inserido invalido.");
						price = -100;
					}
					
				} while(price<0);
				System.out.println("Sending order of " + record.key() + ": " + record.value() + " at price "+ price +" to Shop");
				
				producer.send(new ProducerRecord<String, String>(TOPIC_NAME, record.key(), record.value()+";"+price ));
			}
			
			String option;
			do{
				System.out.print("Pretende voltar ao menu anterior (s/n)? ");
				option = sc.nextLine().toLowerCase();
				
			} while(!option.equals("s") && !option.equals("n"));
			
			if (option.equals("s"))
				break;
		}
		
	}

	
}
