package rest;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class RestServer {
	private static final int 	PORT = 9998;
	private static final String HOST ="http://localhost/";

	public static void main(String[] args) {
		URI baseUri = UriBuilder.fromUri(HOST).port(PORT).build();
		ResourceConfig config = new ResourceConfig(RestFunctions.class);
		JdkHttpServerFactory.createHttpServer(baseUri, config);
	}
	
}
