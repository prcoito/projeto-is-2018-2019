package rest;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.InvalidStateStoreException;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.apache.kafka.streams.state.WindowStore;


@Path("/admin")
public class RestFunctions {
	private static final String THREAD 	= "Thread = ";
	private static final String ARROW	= " --> ";

	private static final String NUMBER_UNITS_EACH_ITEM_SOLD 			= "NumberUnitsEachItemSold";
	private static final String NUMBER_UNITS_EACH_ITEM_SOLD_WINDOWED 	= "NumberUnitsEachItemSoldWindowed";
	private static final String MAX_PRICE_BY_ITEM						= "MaxPriceByItem";
	private static final String MAX_PRICE_BY_ITEM_WINDOWED 				= "MaxPriceByItemWindowed";
	private static final String TOTAL_OF_ITEMS_EVER_SOLD 				= "TotalOfItemsEverSold";
	private static final String AVG_PURCHASES 							= "AvgPurchases";
	private static final String AVG_PRICE_BY_ITEM						= "AvgPriceByItem";
	private static final String HIGHEST_PROFIT_ITEM 					= "HighestProfitItem";
	private static final String HIGHEST_PROFIT_ITEM_WINDOWED 			= "HighestProfitItemWindowed";
	private static final String TOTAL_EXPENSES 							= "TotalExpenses";
	private static final String TOTAL_REVENUE 							= "TotalRevenue";
	private static final String STATS									= "Stats";
	
	private static final String PATH_NUM_ITEMS_SOLD				= "/admin/NumItemsSold";
	private static final String PATH_NUM_EACH_ITEMS_SOLD		= "/admin/NumEachItemsSold";
	private static final String PATH_MAX_PRICE_BY_TIME			= "/admin/MaxPriceByItem";
	private static final String PATH_AVG_PURCHASES				= "/admin/AvgPurchases";
	private static final String PATH_HIGHEST_PROFIT				= "/admin/HighestProfit";
	private static final String PATH_AVG_SALES_PRICE_OF_ITEMS	= "/admin/AvgSalesPriceOfItems";
	private static final String PATH_STATS 						= "/admin/stats";
	


	private StreamsBuilder builder = null;
	private KStream<String, String> salesTopicStream = null;
	private KStream<String, String> shipmentsTopicStream = null;
	private KStream<String, String> purchasesTopicStream = null;
	KTable<String, String> table = null ;
	KTable<String, String> table1 = null ;
	KTable<String, String> table2 = null ;
	KTable<Windowed<String>, String> windowedTable = null;
	KafkaStreams streams = null;
	List<KTable<String, String>> listTables;
	
	
	public void start(String resource, String topic, int timeDelta, List<String> productList) throws Exception {
		if ( streams != null ) {
		    streams.close();
		}
		
		
		
		builder = new StreamsBuilder();
		
		salesTopicStream 	 = getTopicStream("Sales_Topic");
		purchasesTopicStream = getTopicStream("Purchases_Topic");
		shipmentsTopicStream = getTopicStream("Shipments_Topic");
		
		switch (resource) {
			case PATH_NUM_ITEMS_SOLD:
				table = setTotalOfItemsEverSoldStream();
				break;
				
			case PATH_NUM_EACH_ITEMS_SOLD:
				if (timeDelta <= 0)
					table = setTotalOfEachItemSoldStream();
				else
					windowedTable = setTotalOfEachItemSoldWindowedStream(timeDelta);
				break;
				
			case PATH_MAX_PRICE_BY_TIME:
				if (timeDelta <= 0)
					table = setMaxPriceByItemStream();
				else
					windowedTable = setMaxPriceByItemWindowedStream(timeDelta);
				break;
				
			case PATH_AVG_PURCHASES:
				table = setAvgPurchasesStream();
				break;
				
			case PATH_HIGHEST_PROFIT:
				if (timeDelta <= 0)
					table = setHighestProfitItemStream();
				else
					windowedTable = setHighestProfitWindowedStream(timeDelta);
				break;
				
			case PATH_AVG_SALES_PRICE_OF_ITEMS:
				table = avgSalesPriceOfItemsStream(productList);
				break;
				
			case PATH_STATS:
				listTables = setStatsStream();
				table1 = listTables.get(0);
				table2 = listTables.get(1);
				break;
				
			default:
				break;
		}
		
		streams = new KafkaStreams(builder.build(), getProps(topic));
		Thread.sleep(1000); // to give time to fully start

		streams.start();
	  
		Thread.sleep(1000); // to give time to fully start
	  
		// add shutdown hook to stop the Kafka Streams threads
		Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
		
	}

	public Properties getProps(String appID) {
		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, appID);
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(StreamsConfig.APPLICATION_SERVER_CONFIG, "localhost:9998");
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		return props;
	}
	
	
	private String readKeyValueStore(String storeName) throws InterruptedException {
		int count = 10;
		while (true) {	
			try {
				StringBuilder info = new StringBuilder();
				
				ReadOnlyKeyValueStore<String, String> kvs = streams.store(storeName, QueryableStoreTypes.keyValueStore());
				KeyValueIterator<String, String> all = kvs.all();
				while(all.hasNext()) {
					KeyValue<String, String> keyValue = all.next();
					info.append(keyValue.key +": " + keyValue.value + "\n");
				}
				all.close();
				return info.toString();
				
			} catch (InvalidStateStoreException ignored) {
				// streams.store not yet ready for querying
				Thread.sleep(2000);  
				System.out.println(streams.state() + " " +ignored);
				if ( count <= 0)
					return "Não foi possivel concluir o pedido. Tente novamente mais tarde";
				count--;
		    }
		}
		
	}
	
	private String readWindowStore(String storeName) throws InterruptedException {
		int count = 10;
		while (true) {	
			try {
				StringBuilder info = new StringBuilder();
				
				ReadOnlyWindowStore<String, String> ws = streams.store(storeName, QueryableStoreTypes.windowStore());
				KeyValueIterator<Windowed<String>, String> all = ws.all();
					
				while(all.hasNext()) {
					KeyValue<Windowed<String>, String> keyValue = all.next();
					info.append(keyValue.key +": " + keyValue.value + "\n");
				}
			    all.close();
			    
				return info.toString();
				
			} catch (InvalidStateStoreException ignored) {
				// streams.store not yet ready for querying
				Thread.sleep(2000);  
				System.out.println(streams.state() + " " +ignored);
				if ( count <= 0)
					return "Não foi possivel concluir o pedido. Tente novamente mais tarde";
				count--;
		    }
		}
		
	}
	private KStream<String, String> getTopicStream(String topic) {
		return builder.stream(topic);
	}
	
	private KTable<String, String> setTotalOfItemsEverSoldStream() {
		return salesTopicStream
				.map( (key,value) -> new KeyValue<String, String>("Total_of_items_ever_sold", value.split(";")[0]) )
				.groupByKey()
				.reduce( (oldval, newval) -> "" + (Long.parseLong(oldval) + Long.parseLong(newval) ) ,
						 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(TOTAL_OF_ITEMS_EVER_SOLD));
	}
	
	private KTable<String, String>  setTotalOfEachItemSoldStream() {
		return salesTopicStream
				.mapValues(value ->  value.split(";")[0] )
				.groupByKey()
				.reduce( (oldval, newval) -> "" + (Long.parseLong(oldval) + Long.parseLong(newval)),
						 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(NUMBER_UNITS_EACH_ITEM_SOLD));
		
		
	}

	private KTable<Windowed<String>, String> setTotalOfEachItemSoldWindowedStream(int timeDelta) {
		return salesTopicStream
				.mapValues(value -> value.split(";")[0] )
				.groupByKey()
				.windowedBy(TimeWindows.of(TimeUnit.MINUTES.toMillis(timeDelta)))
				.reduce( (oldval, newval) -> "" + (Long.parseLong(oldval) + Long.parseLong(newval)) ,
						 Materialized.<String, String, WindowStore<Bytes,byte[]>>as(NUMBER_UNITS_EACH_ITEM_SOLD_WINDOWED));
	}

	private KTable<String, String> setMaxPriceByItemStream() {
		return salesTopicStream
				.mapValues(value -> value.split(";")[1] )
				.groupByKey()
				.reduce( (oldval, newval) -> "" + Math.max(Double.parseDouble(oldval), Double.parseDouble(newval)) ,
						 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(MAX_PRICE_BY_ITEM));
	}

	private KTable<Windowed<String>, String> setMaxPriceByItemWindowedStream(int timeDelta) {
		
		return salesTopicStream
				.mapValues(value -> value.split(";")[1] )
				.groupByKey()
				.windowedBy(TimeWindows.of(TimeUnit.MINUTES.toMillis(timeDelta)))
				.reduce( (oldval, newval) -> "" + Math.max(Double.parseDouble(oldval), Double.parseDouble(newval)) ,
						 Materialized.<String, String, WindowStore<Bytes,byte[]>>as(MAX_PRICE_BY_ITEM_WINDOWED));
	}


	private KTable<String, String> setAvgPurchasesStream() {
		// value = "product:quantity:price"
		return purchasesTopicStream
				.mapValues(value -> value.split(":")[1] )
				.groupByKey()
				.reduce( (oldval, newval) -> "" + ( (Long.parseLong(oldval) + Long.parseLong(newval) )/2 ) ,
						 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(AVG_PURCHASES));
	}
	
	private KTable<String, String> setHighestProfitItemStream() {
		KStream<String, String> purchases = shipmentsTopicStream
											.map( (key, value) -> new KeyValue<String, String>(key, value.split(";")[1] ) );
		
		KStream<String, String> sales = salesTopicStream
											.map( (key, value) -> new KeyValue<String, String>(key, value.split(";")[1] ) );
		
		
		return sales.join(	purchases, 
							(leftValue, rightValue) -> leftValue + ":" + rightValue /* ValueJoiner */,
							JoinWindows.of(TimeUnit.HOURS.toMillis(1))
						 )
					.map( (key,value) -> new KeyValue<String, String>(HIGHEST_PROFIT_ITEM, key + ARROW + value ) )
					.groupByKey()
					// se preço de venda oldval - preço de compra oldval > se preço de venda newval - preço de compra newval entao guarda-se o oldval senao newval
					.reduce( (oldval, newval) -> ""+ (Double.parseDouble(oldval.split(ARROW)[1].split(":")[0]) - Double.parseDouble(oldval.split(ARROW)[1].split(":")[1]) > 
													  Double.parseDouble(newval.split(ARROW)[1].split(":")[0]) - Double.parseDouble(newval.split(ARROW)[1].split(":")[1])  
													? oldval : newval),	
													
							 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(HIGHEST_PROFIT_ITEM));
	}
	
	private KTable<Windowed<String>, String> setHighestProfitWindowedStream(int timeDelta) {
		KStream<String, String> purchases = shipmentsTopicStream
													.map( (key, value) -> new KeyValue<String, String>(key, value.split(";")[1] ) );

		KStream<String, String> sales = salesTopicStream
													.map( (key, value) -> new KeyValue<String, String>(key, value.split(";")[1] ) );
		
		
		return sales.join(	purchases, 
							(leftValue, rightValue) -> leftValue + ":" + rightValue /* ValueJoiner */,
							JoinWindows.of(TimeUnit.HOURS.toMillis(1))
							)
					.map( (key,value) -> new KeyValue<String, String>(HIGHEST_PROFIT_ITEM_WINDOWED, key + ARROW + value ) )
					.groupByKey()
					.windowedBy(TimeWindows.of(TimeUnit.MINUTES.toMillis(timeDelta)))
					// se preço de venda oldval - preço de compra oldval > se preço de venda newval - preço de compra newval entao guarda-se o oldval senao newval
					.reduce( (oldval, newval) -> ""+ (Double.parseDouble(oldval.split(ARROW)[1].split(":")[0]) - Double.parseDouble(oldval.split(ARROW)[1].split(":")[1]) > 
													  Double.parseDouble(newval.split(ARROW)[1].split(":")[0]) - Double.parseDouble(newval.split(ARROW)[1].split(":")[1])  
													? oldval : newval),	
													
							 Materialized.<String, String, WindowStore<Bytes, byte[]>>as(HIGHEST_PROFIT_ITEM_WINDOWED));
	}
	
	private KTable<String, String> avgSalesPriceOfItemsStream(List<String> listProducts) {
		System.out.println(":::DEBUG:::");
		System.out.println("listProducts = " + listProducts);
		// listProducts.contains(key)
		return salesTopicStream
				.map( (key,value) -> new KeyValue<String, String>( listProducts.contains(key) ? key : null, value.split(";")[1]) ) 
				.groupByKey()
				.reduce( (oldval, newval) -> "" + ( (Double.parseDouble(oldval) + Double.parseDouble(newval) )/2 ) ,
						 Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(AVG_PRICE_BY_ITEM));
	}

	private List< KTable<String, String> > setStatsStream() {
		KTable<String, String> tableRevenue = salesTopicStream
			.map( (key,value) -> new KeyValue<String, String>("Total Revenue", "" + (Long.parseLong(value.split(";")[0]) * Double.parseDouble(value.split(";")[1]))
															 )
				)
			.groupByKey()
			.reduce( (oldval, newval) -> "" + ( Double.parseDouble(oldval) + Double.parseDouble(newval) ) ,
					Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(TOTAL_REVENUE));
		
		KTable<String, String> tableExpenses = shipmentsTopicStream
			.map( (key,value) -> new KeyValue<String, String>("Total Expenses", "" + (Long.parseLong(value.split(";")[0]) * Double.parseDouble(value.split(";")[1])) 
															 )
				)
			.groupByKey()
			.reduce( (oldval, newval) -> "" + ( Double.parseDouble(oldval) + Double.parseDouble(newval) ) ,
					Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as(TOTAL_EXPENSES));
		
		
		return Arrays.asList(tableRevenue, tableExpenses);
	}

	
	/*
	 *  Beginning of Rest functions available to Administrator	
	 */
		
	// Number of items ever sold.
	@GET
	@Path(PATH_NUM_ITEMS_SOLD)
	@Produces(MediaType.TEXT_PLAIN)
	public String getTotalOfItemsEverSold() {		
		try{
			start(PATH_NUM_ITEMS_SOLD, TOTAL_OF_ITEMS_EVER_SOLD, 0, null);
			
			System.out.println("Called getNumberOfItemsEverSold");
			System.out.println(THREAD + Thread.currentThread().getName());		
			
			//.replace("Total_of_items_ever_sold: ", "") para remover a key(= "Total_of_items_ever_sold")
			return "Total Of Items Ever Sold: " + readKeyValueStore(TOTAL_OF_ITEMS_EVER_SOLD).replace("Total_of_items_ever_sold: ", "") ;
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getTotalOfItemsEverSold()");
			e.printStackTrace();
			return "";
		}
		
	}
	
	
	// Number of units sold of each item1. Overall and for the last x minutes, being x a variable.
	@GET
	@Path(PATH_NUM_EACH_ITEMS_SOLD)
	@Produces(MediaType.TEXT_PLAIN)
	public String getNumberUnitsEachItemSold(@QueryParam("timeDelta") int timeDelta) {
		try{
			start(PATH_NUM_EACH_ITEMS_SOLD, NUMBER_UNITS_EACH_ITEM_SOLD, timeDelta, null);
			
			System.out.println("Called getNumberUnitsEachItemSold with timeDelta "+timeDelta);
			System.out.println(THREAD + Thread.currentThread().getName());
			
			return "Number Units Each Item Sold:\n" + ((timeDelta<=0) ? readKeyValueStore(NUMBER_UNITS_EACH_ITEM_SOLD) :
																	   	readWindowStore  (NUMBER_UNITS_EACH_ITEM_SOLD_WINDOWED) );
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getNumberUnitsEachItemSold()");
			e.printStackTrace();
			return "";
		}

	}
	
	
	
	// Maximum price of each item sold so far. Overall and for the last x minutes.
	@GET
	@Path(PATH_MAX_PRICE_BY_TIME)
	@Produces(MediaType.TEXT_PLAIN)
	public String getMaxPriceSold(@QueryParam("timeDelta") int timeDelta) {
	
		try{
			start(PATH_MAX_PRICE_BY_TIME, MAX_PRICE_BY_ITEM, timeDelta, null);
			
			System.out.println("Called getMaxPriceSold with timeDelta "+timeDelta);
			System.out.println(THREAD + Thread.currentThread().getName());
			
			return "Max Price Sold:\n" + ((timeDelta<=0) ?  readKeyValueStore(MAX_PRICE_BY_ITEM) :
															readWindowStore  (MAX_PRICE_BY_ITEM_WINDOWED) );
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getMaxPriceSold()");
			e.printStackTrace();
			return "";
		}
	}
	
	
	
	
	// Average number of purchases(=quantity ??) per order of supplies (for each item).
	@GET
	@Path(PATH_AVG_PURCHASES)
	@Produces(MediaType.TEXT_PLAIN)
	public String getAVGOfPurchasesPerOrder() {
		System.out.println("Called getAVGOfPurchasesPerOrder");
		System.out.println(THREAD + Thread.currentThread().getName());
		
		try {
			start(PATH_AVG_PURCHASES, AVG_PURCHASES, 0, null);
			
			return "Average purchases:\n" + readKeyValueStore(AVG_PURCHASES);
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getAVGOfPurchasesPerOrder()");
			e.printStackTrace();
			return "";
		}
	}
	
	// Revenue, expenses, and profit of the shop so far.
	@GET
	@Path(PATH_STATS)
	@Produces(MediaType.TEXT_PLAIN)
	public String getStats() {
		System.out.println("Called getStats");
		System.out.println(THREAD + Thread.currentThread().getName());
		
		try {
			start(PATH_STATS, STATS, 0, null);
			
			String expenses = readKeyValueStore(TOTAL_EXPENSES).split("\\n")[0];
			String revenue  = readKeyValueStore(TOTAL_REVENUE ).split("\\n")[0];
			String profit   = "Profit: " + (Double.parseDouble(revenue.split("Total Revenue: ")[1]) - Double.parseDouble(expenses.split("Total Expenses: ")[1]) );
			
			return "Shop Statistics:\n" +
					expenses	+ "€\n" +
					revenue		+ "€\n" +
					profit 		+ "€\n" ;
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getStats()");
			e.printStackTrace();
			return "";
		}
	}
	
	
	// Item providing the highest profit over the last x minutes.
	@GET
	@Path(PATH_HIGHEST_PROFIT)
	@Produces(MediaType.TEXT_PLAIN)
	public String getHighestProfitProduct(@QueryParam("timeDelta") int timeDelta) {
		System.out.println("Called getHighestProfitProduct with timeDelta "+timeDelta);
		System.out.println(THREAD + Thread.currentThread().getName());
		
		try {
			start(PATH_HIGHEST_PROFIT, HIGHEST_PROFIT_ITEM, timeDelta, null);
			// replace("HighestProfitItem: ", "") para remover a key(= HighestProfitItem)
			return "[Produto --> preco_venda:preco_compra]\n"
				  + "Highest profit product: " + ((timeDelta<=0) ? readKeyValueStore(HIGHEST_PROFIT_ITEM         ).replace("HighestProfitItem: ", "") :
																   readWindowStore  (HIGHEST_PROFIT_ITEM_WINDOWED).replace("HighestProfitItem: ", "") );
			
		} catch (Exception e) {
			System.out.println("Exception untreated at getHighestProfitProduct()");
			e.printStackTrace();
			return "";
		}
	}
	
	// Query over a range of products for average sales price.
	@GET
	@Path(PATH_AVG_SALES_PRICE_OF_ITEMS)
	@Produces(MediaType.TEXT_PLAIN)
	public String avgSalesPriceOfItems(@QueryParam("productList") String productList) {
		System.out.println("Called AvgSalesPriceOfItems with productList = {" + productList + "}");
		System.out.println(THREAD + Thread.currentThread().getName());
		
		try {
			start(PATH_AVG_SALES_PRICE_OF_ITEMS, AVG_PRICE_BY_ITEM, 0, Arrays.asList(productList.split(";")));

			return "Average Sale Price Of Items:\n" + readKeyValueStore(AVG_PRICE_BY_ITEM);
			
		} catch (Exception e) {
			System.out.println("Exception untreated at avgSalesPriceOfItems()");
			e.printStackTrace();
			return "";
		}
	}
	
	
}
