package data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
	@NamedQuery(name="Product.getAllProductsNames", query="select name from Product"),
	@NamedQuery(name="Product.getAllProducts", 		query="select p from Product p"),
	@NamedQuery(name="Product.deleteAll",			query="DELETE FROM Product")			
})

@Entity
public class Product {
	@Id
	String name;
	double price;
	int stock;
	private int maxStock;
	
	public Product() {}
	
	public Product(String name) {
		this.setName(name);
		this.setPrice(0.0);
		this.setStock(0);
		this.setMaxStock(0);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getMaxStock() {
		return maxStock;
	}

	public void setMaxStock(int maxStock) {
		this.maxStock = maxStock;
	}
	
	
}
