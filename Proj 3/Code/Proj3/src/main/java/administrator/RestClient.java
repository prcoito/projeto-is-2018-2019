package administrator;


import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class RestClient {
	private static final String TARGET 				= "http://localhost:9998/admin/";
	private static final String TIME_DELTA_PARAM 	= "timeDelta";
	private static final int 	EXIT 				= 0;
	private static final int 	MENU 				= 1;
	private static final int 	TEST_NO_TIME 		= 2;
	private static final int 	TEST_TIME 			= 3;
	
	
	private static Client client;
	private static WebTarget webTarget;
	private static final String[] resources = {	"/admin/NumItemsSold",
												"/admin/NumEachItemsSold",		
												"/admin/MaxPriceByItem", 
												"/admin/AvgPurchases",
												"/admin/HighestProfit", 
												"/admin/AvgSalesPriceOfItems",  
												"/admin/stats" 					};
	
	private static final String[] resourcesPretty =  {	"Obter num de items vendidos",
														"Obter num de items vendidos por item",		
														"Obter preco maximo de venda de um item", 
														"Obter a media de compras por cliente",
														"Obter item com maior percentagem de lucro", 
														"Obter media de preço de venda de lista de produtos",  
														"Obter estatisticas"		};
	
	private static WebTarget getWebTarget(String resource, int delta, String productList) {
		
		
		switch (resource) {
			case "/admin/NumItemsSold":
				webTarget = client.target(TARGET).path(resource);
				break;
			
			case "/admin/NumEachItemsSold":
				webTarget = client.target(TARGET).path(resource).queryParam(TIME_DELTA_PARAM, delta);
				break;
			
			case "/admin/MaxPriceByItem":
				webTarget = client.target(TARGET).path(resource).queryParam(TIME_DELTA_PARAM, delta);
				break;
			
			case "/admin/AvgPurchases":
				webTarget = client.target(TARGET).path(resource);
				break;
			
			case "/admin/HighestProfit":
				webTarget = client.target(TARGET).path(resource).queryParam(TIME_DELTA_PARAM, delta);
				break;
			
			case "/admin/AvgSalesPriceOfItems":
				webTarget = client.target(TARGET).path(resource).queryParam("productList", productList);
				break;
			
			case "/admin/stats":
				webTarget = client.target(TARGET).path(resource);
				break;
				
			default:
				webTarget = null;
				break;
		}
		
		return webTarget;
	}
	
	
	private static void testRun(int delta, String productList) {
		
		for(String r : resources ) {
			webTarget = getWebTarget(r, delta, productList);
			callRest(r);
		}		
		
	}
	
	public static void callRest(String resource) {
		if ( webTarget != null) {
			Invocation.Builder invocationBuilder =  webTarget.request(MediaType.TEXT_PLAIN);
			
			String response = invocationBuilder.get(String.class);

			System.out.println(resource);
			System.out.println(response);
		}
	}
	
	public static void main(String[] args) {
		
		client = ClientBuilder.newClient();
		
		String resource;
        int delta;
		String productList;
		int op = 100;
		Scanner sc = new Scanner(System.in);
		
		do {
			try{
				System.out.println("0 - Sair");
				System.out.println("1 - Inserir opcao personalizada");
				System.out.println("2 - Correr teste sem janela temporal");
				System.out.println("3 - Correr teste com janela temporal (10 minutos)");
				System.out.println("Opcao: ");
				
				op = Integer.parseInt( sc.nextLine() );
			
				switch (op) {
				case EXIT:
					break;
				case MENU:
					resource 	= getResource(sc);
					delta 		= getDelta(sc, resource);
					productList = getProductList(sc, resource);
					webTarget 	= getWebTarget(resource, delta, productList);
					callRest(resource);
					break;
				case TEST_NO_TIME:
					delta = 0;
					productList = "Banana;Uva";
					testRun(delta, productList);
					break;
				case TEST_TIME:
					delta = 10;
					productList = "Banana;Uva";
					testRun(delta, productList);
					break;
				default:
					System.out.println("Opcao inserida é invalida.");
					break;
				}
				
			}catch (NumberFormatException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		} while(op!=EXIT);
		
		sc.close();
	}


	private static String getResource(Scanner sc) {
		System.out.println("==================================");
		int i;
		int opcao;
		
		while (true) {
			try {
				i = 0;
				for (String r : resourcesPretty)
					System.out.println( (i++) + " - " + r );
				
				System.out.println("Insira opcao: ");
				
				opcao = Integer.parseInt( sc.nextLine() );
				return resources[opcao];
			} catch (NumberFormatException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		}
	}


	private static String getProductList(Scanner sc, String resource) {
		List<String> listDeltaResources = Arrays.asList( "/admin/AvgSalesPriceOfItems");
		
		if ( !listDeltaResources.contains(resource) )
			return "";
		
		System.out.println("==================================");
		StringBuilder toReturn = new StringBuilder();
		String product;
		while (true) {
			try {
				System.out.println("Insira nome do produto ou enter para terminar: ");
				product = sc.nextLine();
				
				if (product.isEmpty())
				{
					if (toReturn.length() == 0)
						return "";
					
					//.substring(0, toReturn.length()-1)  para eliminar ultimo ";"
					return toReturn.toString().substring(0, toReturn.length()-1); 
				}
				toReturn.append(product+";");
				
			} catch (InputMismatchException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		}
	}


	private static int getDelta(Scanner sc, String resource) {
		int delta;
		
		List<String> listDeltaResources = Arrays.asList( "/admin/NumEachItemsSold", "/admin/MaxPriceByItem", "/admin/HighestProfit" );
		
		if ( !listDeltaResources.contains(resource) )
			return 0;
		
		System.out.println("==================================");
		while (true) {
			try {
				System.out.println("Insira o tempo da janela ou 0 para nao ter janela: ");
				delta = Integer.parseInt( sc.nextLine() );
				return delta;
				
			} catch (NumberFormatException e) {
				System.out.println("Opcao inserida é invalida.");
			}
		}
	}
}
