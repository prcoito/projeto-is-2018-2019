package com.mycompany.assignment_1;



import org.msgpack.annotation.Message;

@Message
public class Telefone {

    private String number, type;
    
    public Telefone(){
        
    }
    public Telefone(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    @Override
    public String toString(){
        return String.format("number: %s, type: %s", number, type);
    }
}
