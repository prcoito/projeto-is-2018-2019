package com.mycompany.assignment_1;

import java.util.*;
import org.msgpack.annotation.Message;

@Message
public class Turma {

	private String name;
        private Pessoa director;
	private List <Pessoa> students;
	
        public Turma(){
            
        }
        public Turma(String name, Pessoa director, List<Pessoa> students) {
		this.name = name;
		this.director = director;
		this.students = students;
	}
        
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Pessoa getDirector() {
		return director;
	}
	public void setDirector(Pessoa director) {
		this.director = director;
	}
	public List<Pessoa> getStudents() {
		return students;
	}
	public void setStudents(List<Pessoa> students) {
		this.students = students;
	}
        
	@Override
	public String toString(){
		return String.format( "Nome turma: %s\n"
							+ "Diretor: %s\n"
							+ "Lista alunos: %s"
							+ "\n", this.name, this.director, this.students);
	}
	
}

