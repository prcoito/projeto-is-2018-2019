package com.mycompany.assignment_1;



import java.util.List;
import org.msgpack.annotation.Message;

@Message
public class Pessoa {
	private String id, name, email;
	private List <Telefone> telephones;
	
	public Pessoa(){
            
        }
	public Pessoa(String id, String name, String email, List<Telefone> telephones) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.telephones = telephones;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public List<Telefone> getTelephones() {
		return telephones;
	}


	public void setTelephones(List<Telefone> telephones) {
		this.telephones = telephones;
	}
        
	@Override
	public String toString(){
		return String.format("id: %s name: %s, email %s , telephones: %s", id, name, email, telephones);
	}
}
