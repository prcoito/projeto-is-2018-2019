package data;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries({
	@NamedQuery(name="User.getUserByID", 					query="SELECT u FROM User u WHERE u.id=:id"),
	@NamedQuery(name="User.getUserByUsername", 					query="SELECT u FROM User u WHERE u.username=:username"),
	@NamedQuery(name="User.getTotalOfWatchListMembersWithID", 	query="SELECT COUNT(u) FROM User u "
																	+ "WHERE u.username=:username and :movie MEMBER OF u.watchListMovie"),
	@NamedQuery(name="User.checkTVShowInWatchlist",			 	query="SELECT COUNT(u) FROM User u "
																	+ "WHERE u.username=:username and "
																	+ "(SELECT COUNT(t.episodes) FROM TVShow t WHERE t.id=:id) = " // numTotalEpisodios
																	+ "(SELECT COUNT(e) FROM TVShowEpisode e WHERE e MEMBER OF u.watchListTV)"),
	
	@NamedQuery(name="User.getTVShowWatchlist",			 	query="SELECT u.watchListTV FROM User u where u.id=:id" ),
	@NamedQuery(name="User.getMovieWatchList", 				query="SELECT u.watchListMovie FROM User u where u.id=:id")	,		
})
@Entity
public class User extends Users{
	@Column(unique=true,nullable=false)
	private int creditCardNumber;
	
	@Temporal(TemporalType.DATE)
	private Date registrationDate;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Movie> watchListMovie;	
	@ManyToMany (cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<TVShowEpisode> watchListTV;	


	public User() {
		super();
		this.watchListMovie = null;
		this.watchListTV = null;
		this.registrationDate = Calendar.getInstance().getTime();
	}

	public User(String username, String firstName, String lastName,
			String email, String password, int creditCardNumber, Date birthday){
		super(username, firstName, lastName,
				email, password, birthday);
		
		this.watchListMovie = null;
		this.watchListTV = null;
		this.registrationDate = Calendar.getInstance().getTime();
		this.creditCardNumber = creditCardNumber;
		
	}
	public int getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(int creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public List<Movie> getWatchListMovie() {
		return watchListMovie;
	}

	public void setWatchListMovie(List<Movie> watchListMovie) {
		this.watchListMovie = watchListMovie;
	}

	public List<TVShowEpisode> getWatchListTV() {
		return watchListTV;
	}

	public void setWatchListTV(List<TVShowEpisode> watchListTV) {
		this.watchListTV = watchListTV;
	}
	
	/*
	 * 	Mesmo que os setters mas retorna a classe para que seja mais facil entender o codigo a construir 
	 *  as classes visto que contem alguma info e usando o construtor seria menos legivel
	 * 
	 */
	
	public User Username(String username) {
		setUsername(username);
		return this;
	}

	public User FirstName(String firstName) {
		setFirstName(firstName);
		return this;
	}

	public User LastName(String lastName) {
		setLastName(lastName);
		return this;
	}

	public User Email(String email) {
		setEmail(email);
		return this;
	}

	public User CreditCardNumber(int creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
		return this;
	}
	
	public User BirthDate(Date birthday) {
		setBirthDate(birthday);
		return this;
	}

	public User Password(String password){
		setPassword(password) ;
		return this;
	}
	
	public User RegistrationDate(Date date) {
		this.registrationDate = date;
		return this;
	}
	
	/*
	 * adaptado de http://www.anyexample.com/programming/java/java_simple_class_to_compute_sha_1_hash.xml
	 * */
	/*public static String SHA256(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException  { 
	    MessageDigest md = MessageDigest.getInstance("SHA-256");
	    md.update(text.getBytes());
	    byte[] sha256hash = md.digest();
	    return convertToHex(sha256hash);
    } 
	
	private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    }

	*/
	
}
