package data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
	@NamedQuery(name="TVShowEpisode.getTVShowEpisodeByID", query="SELECT e FROM TVShowEpisode e WHERE e.id=:episodeID"),
	@NamedQuery(name="TVShowEpisode.getIDByShowID_Season_and_Episode",	query="SELECT e.id FROM TVShowEpisode e "
																			+ "WHERE e.t.id=:showid "
																			+ "and e.season=:season and e.episode=:episode"),
	
	@NamedQuery(name="TVShowEpisode.getNumberSeasonsByShowID", query="SELECT COUNT(DISTINCT e.season) FROM TVShowEpisode e "
																			+ "WHERE e.t.id=:showid "),
																			
	@NamedQuery(name="TVShowEpisode.getTitleByShowID_NumberSeason_NumberEpisode" ,
																	query="SELECT e.title "
																		+ "FROM TVShowEpisode e "
																		+ "WHERE e.season=:season and e.episode=:episode "
																		+ 		"and e.t.id=:showid"),
					
	@NamedQuery(name="TVShowEpisode.getSinopseByShowID_NumberSeason_NumberEpisode", 
																	query="SELECT e.sinopse "
																		+ "FROM TVShowEpisode e "
																		+ "WHERE e.season=:season and e.episode=:episode " 
																		+ 		"and e.t.id=:showid"),
	@NamedQuery(name="TVShowEpisode.getWallpaperByShowID_Season_and_Episode",
																	query="SELECT e.wallpaper "
																		+ "FROM TVShowEpisode e "
																		+ "WHERE e.season=:season and e.episode=:episode " 
																		+ 		"and e.t.id=:showid"),
																		
	@NamedQuery(name="TVShowEpisode.getNumberEpisodesInSeasonByShowID", query="SELECT count(e.id) "
																		+ "FROM TVShowEpisode e "
																		+ "WHERE e.season=:season " 
																		+ 		"and e.t.id=:showid"),//TVShowID

	
})

@Entity
public class TVShowEpisode extends Media{
	
	private int season;
	private int episode;
	private String wallpaper;
	private String fileLocation;
	@ManyToOne
	private TVShow t;
	
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="watchListTV")
	private List<User> users;	
	
	
	public TVShowEpisode() {
	}


	public TVShowEpisode(String title, int year, float rating,
			int season, int episode, String sinopse, int runtime,
			String urlCover, String fileLocation, String wallpaper) {
		/*this.title = title;
		this.year = year;
		this.rating = rating;
		this.season = season;
		this.episode = episode;
		this.sinopse = sinopse;
		this.runtime = runtime;
		this.urlCover = urlCover;
		this.fileLocation = fileLocation;
		this.setWallpaper(wallpaper);*/
	}

/*
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getGenre() {
		return genre;
	}


	public void setGenre(String[] genre) {
		this.genre = genre;
	}
	
	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public double getRating() {
		return rating;
	}


	public void setRating(double rating) {
		this.rating = rating;
	}


	public void setEpisode(int episode) {
		this.episode = episode;
	}


	public String getSinopse() {
		return sinopse;
	}


	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}


	public int getRuntime() {
		return runtime;
	}


	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}


	public String getUrlCover() {
		return urlCover;
	}


	public void setUrlCover(String urlCover) {
		this.urlCover = urlCover;
	}


	public String getFileLocation() {
		return fileLocation;
	}


	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
	public String getWallpaper() {
		return wallpaper;
	}


	public void setWallpaper(String wallpaper) {
		this.wallpaper = wallpaper;
	}
	*/
	public int getSeason() {
		return season;
	}


	public void setSeason(int season) {
		this.season = season;
	}


	public int getEpisode() {
		return episode;
	}
	
	public TVShow getT() {
		return t;
	}


	public void setT(TVShow t) {
		this.t = t;
	}
	
	
	/*
	 * 	Mesmo que os setters mas retorna a classe para que seja mais facil entender o codigo a construir 
	 *  as classes visto que contem alguma info e usando o construtor seria menos legivel
	 * 
	 */
	
	public TVShowEpisode Title(String title) {
		setTitle(title);
		return this;
	}
	
	public TVShowEpisode Genre(String[] genre) {
		setGenre(genre);
		return this;
	}

	public TVShowEpisode Year(int year) {
		setYear(year);
		return this;
	}

	public TVShowEpisode Rating(double d) {
		setRating(d);
		return this;
	}

	public TVShowEpisode Season(int season) {
		this.season = season;
		return this;
	}

	public TVShowEpisode Episode(int episode) {
		this.episode = episode;
		return this;
	}

	public TVShowEpisode Sinopse(String sinopse) {
		setSinopse(sinopse);
		return this;
	}

	public TVShowEpisode Runtime(int i) {
		setRuntime(i);
		return this;
	}

	public TVShowEpisode URLCoverImage(String urlCoverImage) {
		setUrlCoverImage(urlCoverImage);
		return this;
	}
	
	public TVShowEpisode Wallpaper(String w) {
		this.setWallpaper(w);
		return this;
	}
	
	public TVShowEpisode FileLocation(String fileLocation) {
		this.setFileLocation(fileLocation);
		return this;
	}

	public TVShowEpisode TVShow(TVShow t) {
		this.t = t;
		return this;
	}


	public String getWallpaper() {
		return wallpaper;
	}


	public void setWallpaper(String wallpaper) {
		this.wallpaper = wallpaper;
	}


	public String getFileLocation() {
		return fileLocation;
	}


	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
}
