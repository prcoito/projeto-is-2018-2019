package data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Media {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String title;
	private int year;
	private String[] genre;
	private double rating;
	@Lob @Column(columnDefinition = "TEXT")
	private String sinopse;
	private String director;
	private int runtime;
	private String urlCoverImage;
	private String MPAA;
	
	public Media() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String[] getGenre() {
		return genre;
	}

	public void setGenre(String[] genre) {
		this.genre = genre;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public String getUrlCoverImage() {
		return urlCoverImage;
	}

	public void setUrlCoverImage(String urlCoverImage) {
		this.urlCoverImage = urlCoverImage;
	}

	public String getMPAA() {
		return MPAA;
	}

	public void setMPAA(String mPAA) {
		MPAA = mPAA;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
