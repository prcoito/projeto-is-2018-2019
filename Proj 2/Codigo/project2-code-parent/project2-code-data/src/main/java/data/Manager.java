package data;

import java.util.Date;

import javax.persistence.Entity;


@Entity
public class Manager extends Users {

	public Manager() {
		super();
	}

	public Manager(String username, String firstName, String lastName,
			String email, String password, Date birthDate) {
		super(username, firstName, lastName, email, password, birthDate);
	}

	
	/*
	 * 	Mesmo que os setters mas retorna a classe para que seja mais facil entender o codigo a construir 
	 *  as classes visto que contem alguma info e usando o construtor seria menos legivel
	 * 
	 */
	
	public Manager Username(String username) {
		setUsername(username);
		return this;
	}

	public Manager FirstName(String firstName) {
		setFirstName(firstName);
		return this;
	}

	public Manager LastName(String lastName) {
		setLastName(lastName);
		return this;
	}

	public Manager Email(String email) {
		setEmail(email);
		return this;
	}

	
	public Manager BirthDate(Date birthday) {
		setBirthDate(birthday);
		return this;
	}

	public Manager Password(String password){
		setPassword(password);
		return this;
	}
}
