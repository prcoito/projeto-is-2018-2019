package data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
	@NamedQuery(name="Movie.getRandom", 				query="FROM Movie ORDER BY RAND()"),
	@NamedQuery(name="Movie.getMovieByTitleAndYear",	query="FROM Movie m WHERE m.title=:title and m.year=:year"),
	@NamedQuery(name="Movie.getMovieByID", 				query="FROM Movie m WHERE m.id=:id"),
	@NamedQuery(name="Movie.SearchByTitle",	 			query="SELECT m FROM Movie m WHERE m.title LIKE :mTitle")
})
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"title", "year"})})
public class Movie extends Media{
	
	private String fileLocation;
	private String wallpaper;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private List<Person> cast;					// Pessoa no indice 0 tem role no indice 0
	
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="watchListMovie")
	private List<User> users;		
	
	private String[] roles;
	
	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public Movie(String title, int year, String[] genre, double rating,
			String sinopse, String director, int runtime,
			String urlCoverImage, String mpaa, String fileLocation,
			List<Person> cast, String[] roles, String wallpaper) {
		/*this.title = title;
		this.year = year;
		this.genre = genre;
		this.rating = rating;
		this.sinopse = sinopse;
		this.director = director;
		this.runtime = runtime;
		this.urlCoverImage = urlCoverImage;
		this.MPAA = mpaa;*/
		this.fileLocation = fileLocation;
		this.cast = cast;
		this.roles = roles;
		this.wallpaper = wallpaper;
	}
	/*
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String[] getGenre() {
		return genre;
	}

	public void setGenre(String[] genre) {
		this.genre = genre;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public String getUrlCoverImage() {
		return urlCoverImage;
	}

	public void setUrlCoverImage(String urlCoverImage) {
		this.urlCoverImage = urlCoverImage;
	}

	public String getMPAA() {
		return MPAA;
	}

	public void setMPAA(String mPAA) {
		MPAA = mPAA;
	}
	*/
	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public List<Person> getCast() {
		return cast;
	}

	public void setCast(List<Person> cast) {
		this.cast = cast;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	
	public String getWallpaper() {
		return wallpaper;
	}

	public void setWallpaper(String movieWallpaper) {
		this.wallpaper = movieWallpaper;
	}
	
	/*
	 * 	Mesmo que os setters mas retorna a classe para que seja mais facil entender o codigo a construir 
	 *  as classes visto que contem alguma info e usando o construtor seria menos legivel
	 * 
	 */
	
	public Movie Title(String title) {
		setTitle(title);
		return this;
	}

	public Movie Year(int year) {
		setYear(year);
		return this;
	}

	public Movie Genre(String[] genre) {
		setGenre(genre);
		return this;
	}

	public Movie Rating(double rating) {
		setRating(rating);
		return this;
	}

	public Movie Sinopse(String sinopse) {
		setSinopse(sinopse);
		return this;
	}

	public Movie Director(String director) {
		setDirector(director);
		return this;
	}

	public Movie Runtime(int runtime) {
		setRuntime(runtime);
		return this;
	}

	public Movie URLCoverImage(String urlCoverImage) {
		setUrlCoverImage(urlCoverImage);
		return this;
	}
	
	public Movie Wallpaper(String w) {
		setWallpaper(w);
		return this;
	}

	public Movie MPAA(String mPAA) {
		setMPAA(mPAA);
		return this;
	}

	public Movie FileLocation(String fileLocation) {
		setFileLocation(fileLocation);
		return this;
	}

	public Movie Cast(List<Person> cast) {
		setCast(cast);
		return this;
	}

	public Movie Roles(String[] roles) {
		setRoles(roles);
		return this;
	}

	
	
}
