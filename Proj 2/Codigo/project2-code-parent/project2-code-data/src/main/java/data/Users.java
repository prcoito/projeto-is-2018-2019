package data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class Users {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String username;
	private String firstName;
	private String lastName;
	
	/*
	@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
					+"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
					+"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
             message="{invalid.email}")
	*/
	@Column(unique=true,nullable=false)
	private String email;
	private String password;

	@Temporal(TemporalType.DATE)
	private Date birthDate;
	
	public Users(){}
	public Users(String username, String firstName, String lastName, String email, String password, Date birthDate){
		setUsername(username);
		setFirstName(firstName);
		setLastName(lastName);;
		setEmail(email);
		setPassword(SHA256(password));
		setBirthDate(birthDate);
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = SHA256(password);
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public int getID(){
		return this.id;
	}
	

	/*
	 * adaptado de http://www.anyexample.com/programming/java/java_simple_class_to_compute_sha_1_hash.xml
	 * */
	public static String SHA256(String text){ 
	    MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
		    md.update(text.getBytes());
		    byte[] sha256hash = md.digest();
		    return convertToHex(sha256hash);
		} catch (NoSuchAlgorithmException e) {
			return text;
		}
    } 
	
	private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    }
}
