package data;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;



import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@NamedQueries({
	@NamedQuery(name="TVShow.getRandom",				query="FROM TVShow ORDER BY RAND()"),
	@NamedQuery(name="TVShow.getTVShowByTitleAndYear",	query="SELECT t FROM TVShow t WHERE t.title=:title and t.year=:year"),
	@NamedQuery(name="TVShow.getTVShowByID", 			query="SELECT t FROM TVShow t WHERE t.id=:id"),																			
	@NamedQuery(name="TVShow.SearchByTitle", 			query="SELECT t FROM TVShow t WHERE t.title LIKE :tTitle"),	
	@NamedQuery(name="TVShow.getTVShowByEpisodeID", 	query="SELECT t FROM TVShow t WHERE :ep MEMBER OF t.episodes"),
})

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"title", "year"})})
public class TVShow extends Media{
	private String network;
	@ManyToMany(fetch=FetchType.EAGER)
	private List<Person> cast;
	
	private String[] roles;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<TVShowEpisode> episodes;
	
	public TVShow() {
		// TODO Auto-generated constructor stub
	}

	public TVShow(String title, int year, String[] genre, double rating,
			String sinopse, String[] director, String network, int runtime,
			String urlCoverImage, String mpaa, List<Person> cast,
			String[] roles, List<TVShowEpisode> episodes) {
		/*
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.rating = rating;
		this.sinopse = sinopse;
		this.director = director;
		this.runtime = runtime;
		this.urlCoverImage = urlCoverImage;
		this.MPAA = mpaa;*/
		this.network = network;
		this.cast = cast;
		this.roles = roles;
		this.episodes = episodes;
	}
	/*
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String[] getGenre() {
		return genre;
	}

	public void setGenre(String[] genre) {
		this.genre = genre;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getSinopse() {
		return sinopse;
	}

	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}

	public String[] getDirector() {
		return director;
	}

	public void setDirector(String[] director) {
		this.director = director;
	}
	 
	
	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public String getUrlCoverImage() {
		return urlCoverImage;
	}

	public void setUrlCoverImage(String urlCoverImage) {
		this.urlCoverImage = urlCoverImage;
	}

	public String getMPAA() {
		return MPAA;
	}

	public void setMPAA(String mPAA) {
		MPAA = mPAA;
	}
	 */
	
	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public List<Person> getCast() {
		return cast;
	}

	public void setCast(List<Person> cast) {
		this.cast = cast;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public List<TVShowEpisode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(List<TVShowEpisode> episodes) {
		this.episodes = episodes;
	}
	
	/*
	 * 	Mesmo que os setters mas retorna a classe para que seja mais facil entender o codigo a construir 
	 *  as classes visto que contem alguma info e usando o construtor seria menos legivel
	 * 
	 */
	
	public TVShow Title(String title) {
		setTitle(title);
		return this;
	}

	public TVShow Year(int year) {
		setYear(year);
		return this;
	}

	public TVShow Genre(String[] genre) {
		setGenre(genre);
		return this;
	}

	public TVShow Rating(double d) {
		setRating(d);
		return this;
	}

	public TVShow Sinopse(String sinopse) {
		setSinopse(sinopse);
		return this;
	}

	public TVShow Director(String director) {
		setDirector(director);
		return this;
	}

	public TVShow Network(String network) {
		setNetwork(network);
		return this;
	}

	public TVShow Runtime(int runtime) {
		setRuntime(runtime);
		return this;
	}

	public TVShow URLCoverImage(String urlCoverImage) {
		setUrlCoverImage(urlCoverImage);
		return this;
	}

	public TVShow MPAA(String mPAA) {
		setMPAA(mPAA);
		return this;
	}

	public TVShow Cast(List<Person> cast) {
		this.cast = cast;
		return this;
	}

	public TVShow Roles(String[] roles) {
		this.roles = roles;
		return this;
	}

	public TVShow Episodes(List<TVShowEpisode> episodes) {
		this.episodes = episodes;
		return this;
	}
	
}
