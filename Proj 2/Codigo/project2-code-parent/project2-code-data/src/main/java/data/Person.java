package data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Person {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String urlPersonImage;
	
	@ManyToMany(mappedBy="cast")
	private List<Movie> movieList;
	@ManyToMany(mappedBy="cast")
	private List<TVShow> tvshowList;
	
	public Person() {
		
	}

	public Person(String name, String url) {
		this.name = name;
		this.urlPersonImage = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrlPersonImage() {
		return urlPersonImage;
	}

	public void setUrlPersonImage(String urlPersonImage) {
		this.urlPersonImage = urlPersonImage;
	}

	public List<TVShow> getTvshowList() {
		return tvshowList;
	}

	public void setTvshowList(List<TVShow> tvshowList) {
		this.tvshowList = tvshowList;
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}
	
}
