/**
 * 
 */

function setMaxDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear() -18; // tem de ter mais de 18 anos;
	 if(dd<10){
	        dd='0'+dd;
	    } 
	    if(mm<10){
	        mm='0'+mm;
	    } 
	
	maxAllowed = yyyy+'-'+mm+'-'+dd;
	document.getElementById("birthDate").setAttribute("max", maxAllowed);
	
}

window.onload = setMaxDate;