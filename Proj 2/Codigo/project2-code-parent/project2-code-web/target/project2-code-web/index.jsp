<%@ taglib prefix="s" uri="WEB-INF/struts-tags.tld" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file= "WEB-INF/header.html" %>
		
	<title>WebFlix - Página de Login</title>
    </head>

    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="#">Inicio</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="login" method="post" class="ink-form all-50 small-100 tiny-100">
                    <fieldset>
                        <div class="control-group required column-group gutters">
                            <label for="email" class="all-20 align-right">Credenciais</label>
                            <div class="control all-40 small-100 tiny-100">
                                <input type="text" name="email">
                                <p class="tip">Email do Utilizador</p>
                            </div>
                            <div class="control all-40 small-100 tiny-100">
                                <input type="password" name="password">
                                <p class="tip">Password</p>
                            </div>
                        </div>
                    	<input type="submit" value="Login" class="ink-button">
                    </fieldset>
            	</form>
            	
                
			</div>
			<div class="ink-form all-50 small-100 tiny-100">
				<s:a class="ink-button" href='linkToRegistar'>Registar</s:a>
			</div>
			<p></p>			
		</div>

<%@ include file= "WEB-INF/footer.html" %>