<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   
   
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>

<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página de Edição de Episodio</title>
	<script type="text/javascript" src="JS/date.js"></script>	
    </head>
	
    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="login">Inicio</a></li>
                        <li><a href="Logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="EditarEpisodio" method="post" class="ink-form all-100 small-100 tiny-100">
                    <fieldset>
                        <div class="control-group required column-group gutters">
                        	<input type="hidden" name="id" value="${Admin.id}">
                            <label for="first-name" class="all-20 align-right">Titulo</label>
                            <div class="control all-25">
                                <input type="text" name="title" required value="${Admin.title}"> 
                            </div>
                        	<label for="first-name" class="all-20 align-right">Ano</label>
                            <div class="control all-25">
                                <input type="number" min="1900" name="year" required value="${Admin.year}">
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                        	<label for="first-name" class="all-20 align-right">Temporada</label>
                            <div class="control all-25">
                                <input type="number" min="0" name="season" required" value="${Admin.season}"> 
                            </div>
                        	<label for="first-name" class="all-20 align-right">Episodio</label>
                            <div class="control all-25">
                                <input type="number" min="0" name="episode" required value="${Admin.episode}">
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Genero</label>
                            <div class="control all-25">
                                <input type="text" name="genre" required value="${Admin.genre}">
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Rating</label>
                            <div class="control all-25">
                                <input type="text" min="0" name="rating" required value="${Admin.rating}">
                                <p class="tip">de 0-10</p>
                            </div>
                            <label for="first-name" class="all-20 align-right">Runtime</label>
	                        <p class="tip">em minuntos</p>
	                        <div class="control all-25">
		                    	<input type="number" min="0" name="runtime" required value="${Admin.runtime}">
		                    </div>
		                </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Sinopse</label>
                            <div class="control all-80">
                                <input type="text" name="sinopse" required value="${Admin.sinopse}">
                            </div>
                        </div>
                    	
		                <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">URL Cover Image</label>
	                        <div class="control all-25">
		                    	<input type="url" name="urlCoverImage" required value="${Admin.urlCoverImage}">
		                    </div>
		                </div>
	                    <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Localizacao do ficheiro</label>
	                        <div class="control all-25">
		                    	<input type="text" name="fileLocation" required value="${Admin.fileLocation}">
		                    </div>
		                </div>
		                <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Wallpaper</label>
	                        <div class="control all-25">
		                    	<input type="text" name="wallpaper" required value="${Admin.wallpaper}">
		                    </div>
		                </div>
		                <div class="control-group required column-group gutters">
		                	<div class="control all-25">
		                		<c:set var="ShowID" value="${ Admin.tvShowID }" />
		                		<p> DEBUG : <c:out value = "${ShowID}"/> </p>
		                		<select name="tvShowID">
				                	<c:forEach items="${AdminAction.IDTitleandYearSerie}" var="info">
		                        		<c:set var="id"				value="${fn:split(info, ';')[0]}"/>
		                        		<c:set var="titleAndYear"	value="${fn:split(info, ';')[1]}"/>
		                  				
		                  				<c:if test="${ ShowID.equals( id ) }">
		                  					<option selected="selected" value="${id}">${titleAndYear}</option>
										</c:if>
										<c:if test="${ not ShowID.equals( id ) }">
											<option value="${id}">${titleAndYear}</option>
										</c:if>
									
									</c:forEach>
								</select>
							</div>
		                </div>

                    	<input type="submit" value="Editar" class="ink-button">    
                    </fieldset>
                </form>
			</div>		

<%@ include file= "../footer.html" %>