<%@page import="struts.ligacao.Ligacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>


    
<%@ include file= "../header.html" %>
	
	<title>WebFlix - Página da Série <c:out value="${Ligacao.TVShowTitle}"/></title>
	
	</head>

	<body>
		<div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

			<header>
            	<h1>WebFlix Project<small>IS Paulo Coito</small></h1>
            	<nav class="ink-navigation">
                	<ul class="menu horizontal black">
                    	<li><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                    	<li>
                        	<s:url var="url" action="userProfile" >
		                 	 	<s:param name="username"><c:out value="${Ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'><c:out value="${Ligacao.username}"/></s:a>
			             </li>
			             <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
            
            <div class="column-group gutters">
           		<div class="xlarge-25 large-40 medium-50 small-100 tiny-100">
		        	<img src="<c:out value="${Ligacao.TVShowPoster}"/>" >
		        </div>
		        <div class="xlarge-75 large-60 medium-50 small-100 tiny-100">
		        	<p> <b>Título:</b> <c:out value="${Ligacao.TVShowTitle}" /> (<c:out value="${Ligacao.TVShowYear}" />)</p>
		            <p> <b>Rating:</b> <c:out value="${Ligacao.TVShowRating}" /> </p>
		            <p> <b>Sinopse:</b> <c:out value="${Ligacao.TVShowSinopse}" /> </p>
		            <p>	<b>Diretor:</b> <c:out value="${Ligacao.TVShowDirector}" /> </p>
		            <p>	<b>Género:</b> <c:out value="${Ligacao.TVShowGenre}" /> </p>
		            <p>	<b>Classificação Etária:</b> <c:out value="${Ligacao.TVShowMPAA}" /> </p>
		            <p>	<b>Duração:</b> <c:out value="${Ligacao.TVShowRuntime}" /> minutos</p>
		            <div class="xlarge-100 large-10 medium-10 small-10 tiny-10">
						<s:url var="addwatchlist" action="addShowToWatchList" >
							<s:param name="TVShowID"><c:out value="${Ligacao.IDTVShow}"/></s:param>
						</s:url> 
						<s:a href='%{#addwatchlist}'>
							<c:set var="imgURL" value="${Ligacao.TVShowWatchListIcon}"/>
							<img style="height:2em;" src="<c:out value="${imgURL}"/>" >
							
							<c:if test = "${fn:startsWith( imgURL, 'Icons/Add' )}" >
								<p style="color:black;">Adicionar <c:out value="${Ligacao.TVShowTitle}"/> à watchlist</p>
		                 	</c:if>
		                 	<c:if test = "${fn:startsWith( imgURL, 'Icons/Remo' )}" >
								<p style="color:black;">Remover <c:out value="${Ligacao.TVShowTitle}"/> da watchlist</p>
		                 	</c:if>
						</s:a>
					</div>

				</div>   
		   	</div>   
		   	<div class="column-group gutters"> 
				<div class="xlarge-100 large-100 medium-100 small-100 tiny-100">
					<p> <b>Elenco Principal:</b></p> 	
			        <div class="column-group gutters">
			        	<c:forEach items="${Ligacao.TVShowCast}" var="info">
			        		<c:set var = "full" 	value = "${info}"/>
			     			<c:set var = "url" 		value = "${fn:split(full, ';')[0]}"/>
			     			<c:set var = "name"		value = "${fn:split(full, ';')[1]}"/>
			     			<c:set var = "role"		value = "${fn:split(full, ';')[2]}"/>
			     			<div class="xlarge-20 large-20 medium-25 small-50 tiny-100">
			     				<img src="<c:out value="${url}"/>" >
			        			<p><b><c:out value="${name}"/></b><br>
			     				<c:out value="${role}"/></p>
			     			</div>
			     		</c:forEach>
			     	</div>
		     	</div>
		   	</div>
		   	
		   	<c:set var ="showID" value="${Ligacao.IDTVShow}" />
		   	
		   	<% 
		   	int showID = Integer.parseInt((String)pageContext.getAttribute("showID")); 
		   	int numSeasons = Ligacao.getNumberSeasons(showID);
		   	
		   	%>
		   	<% for(int season = 1; season < numSeasons +1; season+=1) { %>
		   		<h1 style="margin-bottom:0.5em; margin-top:2em;"><b>Season <%= season %></b></h1>
			   	<div class="panel">
			        <div id="season<%= season %>" class="ink-carousel" data-space-after-last-slide="false" data-autoload="false">
			            <ul class="stage column-group half-gutters" style="width: 100%; left: 0%;">
			            	<% int numEpisodes = Ligacao.getTVShowNumberEpisodesInSeason(showID, season);
			            		for(int episode = 1; episode < numEpisodes +1; episode+=1) { %>
				            	<li class="slide xlarge-20 large-20 medium-33 small-50 tiny-100">
				            		<%
				                    String epID = Ligacao.getEpisodeID(showID, season, episode);
				                    pageContext.setAttribute("episodeID", epID);
				                    %>
				                    
				                    <s:url var="url" action="goToTVShowEpisodePage" >
				                 		<s:param name="episodeID"><c:out value="${episodeID}"/></s:param>
					                </s:url> 
					                <s:a href='%{#url}'>
				                 		<img class="half-bottom-space" src="<%= Ligacao.getEpisodeWallpaper(showID, season, episode)%>">
					                    <div class="description">
					                        <h4 class="no-margin">Episódio <%= episode %></h4>
					                        <h5 class="slab"><%= Ligacao.getEpisodeTitle(showID, season, episode) %></h5>
					                        <p align="justify" style="color:black; height=1em; overflow:hidden;"><%= Ligacao.getEpisodeSinopse(showID, season, episode) %></p>
					                    </div>
				                    </s:a>
				                </li>
				            <% } %>   	
			            </ul>
			        </div>
					
					<nav id="p<%= season %>" class="ink-navigation text-align:center">
			            <ul class="pagination black"></ul>
			        </nav>
			    </div>
			    <script>
			        Ink.requireModules(['Ink.UI.Carousel_1'], function(InkCarousel) {
			            new InkCarousel('#season'+<%= season %>, {pagination: '#p'+<%= season %>});
			        });
			    </script>
			<% } %>   	
		   	
           
            
<%@ include file= "../footer.html" %>