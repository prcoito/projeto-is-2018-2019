<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>


    
<%@ include file= "../header.html" %>
	<script type="text/javascript" src="JS/player.js"></script>		
		
	<title>WebFlix - Página do Filme <c:out value="${Ligacao.movieTitle}" /></title>
	
	</head>

    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                    	<li>
                        	<s:url var="url" action="userProfile" >
		                 	 	<s:param name="username"><c:out value="${Ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'><c:out value="${Ligacao.username}"/></s:a>
			             </li>
			             <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
            
            <div class="column-group gutters">
           		<div class="xlarge-25 large-40 medium-50 small-100 tiny-100">
		            <img src="<c:out value="${Ligacao.moviePoster}"/>" >
		        </div>
		        <div class="xlarge-75 large-60 medium-50 small-100 tiny-100">
		            <p> <b>Título:</b> <c:out value="${Ligacao.movieTitle}" /> (<c:out value="${Ligacao.movieYear}" />)</p>
		            <p> <b>Rating:</b> <c:out value="${Ligacao.movieRating}" /> </p>
		            <p> <b>Sinopse:</b> <c:out value="${Ligacao.movieSinopse}" /> </p>
		            <p>	<b>Diretor:</b> <c:out value="${Ligacao.movieDirector}" /> </p>
		            <p>	<b>Género:</b> <c:out value="${Ligacao.movieGenre}" /> </p>
		            <p>	<b>Classificação Etária:</b> <c:out value="${Ligacao.movieMPAA}" /> </p>
		            <p>	<b>Duração:</b> <c:out value="${Ligacao.movieRuntime}" /> minutos</p>
		            <div class="xlarge-100 large-10 medium-10 small-10 tiny-10">
						<s:url var="addwatchlist" action="addMovieToWatchList" >
							<s:param name="movieID"><c:out value="${ligacao.IDMovie}"/></s:param>
						</s:url> 
						<s:a href='%{#addwatchlist}'>
							<c:set var="imgURL" value="${ligacao.movieWatchListIcon}"/>
							<img style="height:2em;" src="<c:out value="${imgURL}"/>" >
							
							<c:if test = "${fn:startsWith( imgURL, 'Icons/Add' )}" >
								<p style="color:black;">Adicionar <c:out value="${Ligacao.movieTitle}"/> à watchlist</p>
		                 	</c:if>
		                 	<c:if test = "${fn:startsWith( imgURL, 'Icons/Remo' )}" >
								<p style="color:black;">Remover <c:out value="${Ligacao.movieTitle}"/> da watchlist</p>
		                 	</c:if>
							
						</s:a>
					</div>
		        </div>   
		    </div>   
		   <div class="column-group gutters"> 
				<div class="xlarge-100 large-100 medium-100 small-100 tiny-100">
			        <p> <b>Elenco Principal:</b></p> 	
		        	<div class="column-group gutters">
		           		<c:forEach items="${Ligacao.movieCast}" var="info">
		            		<c:set var = "full" 	value = "${info}"/>
		     				<c:set var = "url" 		value = "${fn:split(full, ';')[0]}"/>
		     				<c:set var = "name"		value = "${fn:split(full, ';')[1]}"/>
		     				<c:set var = "role"		value = "${fn:split(full, ';')[2]}"/>
		     				<div class="xlarge-20 large-20 medium-25 small-50 tiny-100">
		     					<img src="<c:out value="${url}"/>" >
		                    	<p><b><c:out value="${name}"/></b><br>
		     					<c:out value="${role}"/></p>
		     				</div>
		     			</c:forEach>
		     		</div>
		 		</div>
			</div>
		   	<input type="file" accept="video/*"/>
		   	<div class="column-group">
				<video 	class="xlarge-100 large-100 medium-100 small-100 tiny-100" controls  
						poster=<c:out value="${Ligacao.movieWallpaper}"/> >
						
					<source src="<c:out value="${Ligacao.movieFileLocation}"/>" type="video/mp4">
				</video>
			</div> 	
	            
            
<%@ include file= "../footer.html" %>