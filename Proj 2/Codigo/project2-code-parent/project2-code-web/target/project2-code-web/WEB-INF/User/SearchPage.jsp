<%@page import="struts.ligacao.Ligacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>


    
<%@ include file= "../header.html" %>
	
	<title>WebFlix - Página de Pesquisa</title>
	
	</head>

	<body>
		<div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

			<header>
            	<h1>WebFlix Project<small>IS Paulo Coito</small></h1>
            	<nav class="ink-navigation">
                	<ul class="menu horizontal black">
                    	<li><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                    	<li>
                        	<s:url var="url" action="userProfile" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'><c:out value="${ligacao.username}"/></s:a>
			             </li>
			             <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
            
    		<div>
	    		<form action="search" method="post" class="ink-form all-100 small-100 tiny-100">
	    			<fieldset>
	    				<label for="first-name" class="all-20 align-right">Tipo Media</label>
                        <div class="control small-100 tiny-100">
		                	<select name="type">
								<option value="ALL">Todos</option>
			                	<option value="Movie">Filme</option>
								<option value="TVShow">Série</option>
								<option value="TVShowEpisode">Episódio de Série</option>
			                </select>
			                <label class="all-20 align-right">Titulo</label>
							<input class="control small-100 tiny-100" type="text" placeholder="Titulo" name="title"></input>
			            </div>
			            <div class="control small-100 tiny-100">
		                	<label class="all-20 align-right">Diretor</label>
							<input class="control small-100 tiny-100" type="text" placeholder="Diretor" name="director"></input>
		                </div>
			            <div class="control small-100 tiny-100">
		                	<label class="all-20 align-right">Género</label>
							<input class="control small-100 tiny-100" type="text" placeholder="Genero" name="gen"></input>
		                </div>
		                <div class="control small-100 tiny-100">
		                	<label class="all-20 align-right">Ano de Lançamento</label>
							<input class="control small-100 tiny-100" type="number" min="1990" placeholder="Ano de Lançamento menor" name="startYear"></input>
		                	<input class="control small-100 tiny-100" type="number" min="1990" placeholder="Ano de Lançamento maior" name="endYear"></input>
							<p class="tip">Para o caso de saber o ano exato apenas necessita de preencher um valor</p>
		                </div>
		                <div class="control small-100 tiny-100">
			                <label class="all-20 align-right">Ordenar por</label>
							<select name="OrderBy">
			                	<option value="Title">Titulo</option>
								<option value="Year">Ano</option>
			                </select>
			                <select name="OrderByType">
			                	<option value="ASC">Do menor > maior</option>
								<option value="DESC">Do maior > menor</option>
			                </select>   
		                </div>
		                <input type="hidden" name="page" value="1"></input>
		            </fieldset>
	            	<input type="submit" value="Pesquisar" class="ink-button"> 
	    		</form>
    		</div>
    
<%@ include file= "../footer.html" %>        