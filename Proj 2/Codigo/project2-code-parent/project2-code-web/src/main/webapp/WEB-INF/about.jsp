<%@ taglib prefix="s" uri="struts-tags.tld" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file= "header.html" %>
		
	<title>WebFlix - Página de Informações</title>
    </head>

    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li class="active"><a href="Home">Inicio</a></li>
                    </ul>
                </nav>
            </header>


			<div>
				<p>Projeto desenvolvido no âmbito da cadeira de Integração de Sistemas 2017-2018</p>
			</div>


<%@ include file= "footer.html" %>