<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file= "../header.html" %>
		
	<title>WebFlix - Destaques</title>
    </head>

    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li class="active"><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                        <li>
                        	<s:url var="url" action="userProfile" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'><c:out value="${ligacao.username}"/></s:a>
			             </li>
			             <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>


			<!-- Random Movie List with 5 movies ( destaques p. ex ) -->
			<h1>Destaques de Filmes:</h1>
			<div class="column-group gutters">
				<c:forEach items="${ligacao.someRandomMovies}" var="info">
					<c:set var = "id" 		value = "${fn:split(info, ';')[0]}"/>
     				<c:set var = "url" 		value = "${fn:split(info, ';')[1]}"/>
     				<c:set var = "title"	value = "${fn:split(info, ';')[2]}"/>
     				<c:set var = "year"		value = "${fn:split(info, ';')[3]}"/>
                 	
               		<div class="xlarge-20 large-20 medium-25 small-50 tiny-100">
	                 	 <s:url var="movieurl" action="goToMoviePage" >
	                 	 	<s:param name="id"><c:out value="${id}"/></s:param>
		                 </s:url> 
		                 <s:a href='%{#movieurl}'>
			                 <img src="<c:out value="${url}"/>">
			                 <p><c:out value="${title}"/> (<c:out value="${year}"/>)</p>
			             </s:a>
						
              		</div>
				</c:forEach>
				
            </div>
            
			<h1>Destaques de Séries:</h1>
            <div class="column-group gutters">
				<c:forEach items="${ligacao.someRandomTV}" var="info">
					<c:set var = "id" 		value = "${fn:split(info, ';')[0]}"/>
     				<c:set var = "url" 		value = "${fn:split(info, ';')[1]}"/>
     				<c:set var = "title"	value = "${fn:split(info, ';')[2]}"/>
     				<c:set var = "year"		value = "${fn:split(info, ';')[3]}"/>
                 	
                 	<div class="xlarge-20 large-20 medium-25 small-50 tiny-100">
	                 	<s:url var="tvshowurl" action="goToTVShowPage" >
	                 	 	<s:param name="id"><c:out value="${id}"/></s:param>
		                 </s:url> 
		                 <s:a href='%{#tvshowurl}'>
			                 <img src="<c:out value="${url}"/>">
			                 <p><c:out value="${title}"/> (<c:out value="${year}"/>)</p>
			             </s:a>
              		</div>
               			
 					
				</c:forEach>
				
            </div>
			
            


<%@ include file= "../footer.html" %>