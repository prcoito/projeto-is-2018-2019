<%@ taglib prefix="s" uri="../struts-tags.tld" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página de Edição de perfil</title>
	<script type="text/javascript" src="JS/date.js"></script>
    </head>

    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                        <li class="active"><a>${ligacao.userUserName}</a></li>
			            <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="editUser" method="post" class="ink-form all-100 small-100 tiny-100">
                    <fieldset>
                        <div class="control-group column-group gutters">
                            <label for="first-name" class="all-20 align-right">Primeiro Nome</label>
                            <div class="control all-25">
                                <input type="text" name="firstName" value=${ligacao.userFirstName} >
                            </div>
                        	<label for="first-name" class="all-20 align-right">Ultimo Nome</label>
                            <div class="control all-25">
                                <input type="text" name="lastName" value=${ligacao.userLastName} >
                            </div>
                        </div>
                        <div class="control-group column-group gutters">
                            <label for="first-name" class="all-20 align-right">Nome de Utilizador</label>
                            <div class="control all-25">
                                <input type="text" name="username" value=${Ligacao.userUserName} >
                            </div>
                        </div>
                        <div class="control-group column-group gutters">
                            <label for="first-name" class="all-20 align-right">Email</label>
                            <div class="control all-25">
                                <input type="email" name="emailR" value=${ligacao.userEmail} >
                            </div>
                        </div>
                    	<div class="control-group required column-group gutters">
	                    	<label for="first-name" class="all-20 align-right" >Password Antiga</label>
	                        <div class="control all-25">
		                    	<input type="password" name="passwordO" required >
		                    </div>
	                    </div>
	                    <div class="control-group column-group gutters">   
	                        <label for="first-name" class="all-20 align-right" >Nova Password</label>
	                        <div class="control all-25">
		                    	<input type="password" name="passwordR">
		                    </div>
	                        <label for="first-name" class="all-20 align-right">Repetir Nova Password</label>
	                        <div class="control all-25">
		                    	<input type="password" name="passwordRepeat">
		                    </div>
		                </div>
		                <div class="control-group column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Numero Cartao de Credito</label>
	                        <div class="control all-25">
		                    	<input type="number" min="100000000" max="999999999" name="creditCardNumber" value=${ligacao.userCreditCardNumber}>
		                    </div>
		                </div>
		                <div class="control-group column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Data Nascimento</label>
	                        <div class="control all-25">
		                    	<input type="date" name="birthDate" max="2000-01-01" value=${ligacao.userBirthDate} >
		                    </div>
		                </div>
                    	<input type="submit" value="Submit" class="ink-button">    
                    </fieldset>
                </form>
                <div class="all-100 small-100 tiny-100">
	                <s:url var="url" action="deleteUser" >
	                 	 	<s:param name="userID">${ligacao.userID}</s:param>
		            </s:url> 
		            <s:a class="ink-button" href='%{#url}'>
		            	Apagar Conta
		            </s:a>
	            </div>
			</div>	

<%@ include file= "../footer.html" %>