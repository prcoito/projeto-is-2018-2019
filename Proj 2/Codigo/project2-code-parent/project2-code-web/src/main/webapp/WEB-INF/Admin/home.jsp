<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página Inicial</title>
	</head>
	
    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="home">Inicio</a></li>
                        <!--<li><a href="goToAdicionarMedia">Adicionar Media</a></li>
                        <li><a href="goToEditarMedia">Editar Media</a></li>
                        <li><a href="goToEliminarMedia">Eliminar Media</a></li>-->
                        <li><a href="Logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
			<div class="column-group gutters">
				<div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToAdicionarMedia" >
	               	 	<s:param name="type"><c:out value="Movie"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Adicionar Filme</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToAdicionarMedia" >
	               	 	<s:param name="type"><c:out value="TVShow"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Adicionar Série</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToAdicionarMedia" >
	               	 	<s:param name="type"><c:out value="TVShowEpisode"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Adicionar Episodio a Série</s:a>
                 </div>
             </div>
             <div class="column-group gutters">   
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEditarMedia" >
	               	 	<s:param name="type"><c:out value="Movie"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Editar Filme</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEditarMedia" >
	               	 	<s:param name="type"><c:out value="TVShow"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Editar Série</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEditarMedia" >
	               	 	<s:param name="type"><c:out value="TVShowEpisode"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Editar Episodio de Série</s:a>
                </div>
             </div>    
             <div class="column-group gutters">    
                  <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEliminarMedia" >
	               	 	<s:param name="type"><c:out value="Movie"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Eliminar Filme</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEliminarMedia" >
	               	 	<s:param name="type"><c:out value="TVShow"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Eliminar Série</s:a>
                 </div>
                 <div class="all-25 small-100 tiny-100">
					<s:url var="url" action="goToEliminarMedia" >
	               	 	<s:param name="type"><c:out value="TVShowEpisode"/></s:param>
	                 </s:url> 
	                 <s:a class="ink-button" href='%{#url}'>Eliminar Episodio de Série</s:a>
                 </div>
			</div>
            

<%@ include file= "../footer.html" %>