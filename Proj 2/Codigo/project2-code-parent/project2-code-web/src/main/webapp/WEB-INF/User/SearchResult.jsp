<%@page import="struts.ligacao.Ligacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>


    
<%@ include file= "../header.html" %>
	
	<title>WebFlix - Resultados para <c:out value="${Ligacao.searchTerm}"/></title>
	
	</head>

	<body>
		<div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

			<header>
            	<h1>WebFlix Project<small>IS Paulo Coito</small></h1>
            	<nav class="ink-navigation">
                	<ul class="menu horizontal black">
                    	<li><a href="Home">Inicio</a></li>
                    	<li><a href="goToPagPesquisar">Pesquisa Personalizada</a></li>
                        <li>
	                        <form action="pesquisaRapida">
	                        	<div class="control all-100 small-100 tiny-100 append-button" role="search">
	                                <span><input type="text" id="name5" name=searchTerm placeholder="Insira nome do filme/serie"></span>
	                                <button class="ink-button"><i class="fa fa-search"></i>Pesquisa Rápida</button>
	                            </div>
	                        </form>
                        </li>
                        <li>
                        	<s:url var="url" action="getWatchlist" >
		                 	 	<s:param name="username"><c:out value="${ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'>WatchList</s:a>
			             </li>
                    	<li>
                        	<s:url var="url" action="userProfile" >
		                 	 	<s:param name="username"><c:out value="${Ligacao.username}"/></s:param>
			                 </s:url> 
			                 <s:a href='%{#url}'><c:out value="${Ligacao.username}"/></s:a>
			             </li>
			             <li><a href="logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
            
            <h1>Resultados:</h1>
            <div class="column-group gutters">
				<c:forEach items="${ligacao.resultList}" var="info">
					<c:set var = "full" 	value = "${info}"/>
					<c:set var = "type" 	value = "${fn:split(full, ';')[0]}"/>
     				<c:set var = "id" 		value = "${fn:split(full, ';')[1]}"/>
     				<c:set var = "urlW"		value = "${fn:split(full, ';')[2]}"/>
     				<c:set var = "title"	value = "${fn:split(full, ';')[3]}"/>
     				<c:set var = "year"		value = "${fn:split(full, ';')[4]}"/>
     				<c:set var = "sinopse"	value = "${fn:split(full, ';')[5]}"/>
                 	
                 	<c:if test = "${fn:startsWith( type, 'Não foi encontrado ' )}">
                 		<div class="xlarge-75 large-20 medium-25 small-50 tiny-100">
                 		<p><c:out value="${type}"/></p>
                 		</div>
                 	</c:if>
                 	<c:if test = "${not fn:startsWith( type, 'Não foi encontrado ' )}">
	                 	<div class="xlarge-100 large-100 medium-100 small-100 tiny-100">
		                 	<s:url var="mediaURL" action="goToMediaPage" >
			               	 	<s:param name="mediaType"><c:out value="${type}"/></s:param>
			               	 	<s:param name="id"><c:out value="${id}"/></s:param>
			                </s:url> 
			                <s:a class="column-group gutters" href='%{#mediaURL}'>
		                		<div class="xlarge-10 large-20 medium-25 small-0 tiny-0"></div>
		                		<div class="xlarge-25 large-20 medium-25 small-50 tiny-100">
		                			<img src="<c:out value="${urlW}"/>">
				                </div>
				                <div class="xlarge-55 large-20 medium-25 small-50 tiny-100">
		                			<p style="color:black;"><b>Título:</b> <c:out value="${title}"/> (<c:out value="${year}"/>)</p>
				                 	<p align="justify" style="color:black;"><b>Sinopse:</b> <c:out value="${sinopse}"/></p>
				                </div>
				                <div class="xlarge-10 large-20 medium-25 small-0 tiny-0"></div>
				            </s:a>
				        </div>
			        </c:if>
				</c:forEach>
				
				<div class="xlarge-100 large-100 medium-100 small-100 tiny-100">
					<c:set var="isFirst" value = "${ligacao.isFirstResultsPage}" />
					
					<c:if test="${isFirst == '1'}">
						<input class="ink-button" disabled value="Anterior">
					</c:if>
					<c:if test="${isFirst == '0'}">
						<s:url var="resultsURL" action="search" >
							<s:param name="type"><c:out value="${ligacao.searchType}"/></s:param>
							<s:param name="title"><c:out value="${ligacao.searchTitle}"/></s:param>
							<s:param name="director"><c:out value="${ligacao.searchDirector}"/></s:param>
							<s:param name="gen"><c:out value="${ligacao.searchGen}"/></s:param>
							<s:param name="startYear"><c:out value="${ligacao.searchStartYear}"/></s:param>
							<s:param name="endYear"><c:out value="${ligacao.searchEndYear}"/></s:param>
							<s:param name="OrderBy"><c:out value="${ligacao.searchOrderBy}"/></s:param>
							<s:param name="OrderByType"><c:out value="${ligacao.searchOrderByOrder}"/></s:param>
							<s:param name="page"><c:out value="${ligacao.previousPageNumber}"/></s:param>
		                </s:url> 
		                <s:a class="ink-button" href='%{#resultsURL}'>
		                	Anterior
	                	</s:a>
					</c:if>
					
					<c:set var="isLast" value = "${ligacao.isLastResultsPage}" />
					<c:if test="${isLast == '1'}">
						<input class="ink-button" disabled value="Seguinte">
					</c:if>
					<c:if test="${isLast == '0'}">
						<s:url var="resultsURL" action="search" >
							<s:param name="type"><c:out value="${ligacao.searchType}"/></s:param>
							<s:param name="title"><c:out value="${ligacao.searchTitle}"/></s:param>
							<s:param name="director"><c:out value="${ligacao.searchDirector}"/></s:param>
							<s:param name="gen"><c:out value="${ligacao.searchGen}"/></s:param>
							<s:param name="startYear"><c:out value="${ligacao.searchStartYear}"/></s:param>
							<s:param name="endYear"><c:out value="${ligacao.searchEndYear}"/></s:param>
							<s:param name="OrderBy"><c:out value="${ligacao.searchOrderBy}"/></s:param>
							<s:param name="OrderByType"><c:out value="${ligacao.searchOrderByOrder}"/></s:param>
							<s:param name="page"><c:out value="${ligacao.previousPageNumber}"/></s:param>
						</s:url> 
						
		                <s:a class="ink-button" href='%{#resultsURL}'>
		                	Seguinte
	                	</s:a>
	                	
					</c:if>
				</div>
            </div>
            
<%@ include file= "../footer.html" %>