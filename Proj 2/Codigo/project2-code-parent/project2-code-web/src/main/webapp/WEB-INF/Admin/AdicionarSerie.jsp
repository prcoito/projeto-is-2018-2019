<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>

<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página de Adição de Série</title>
	<script type="text/javascript" src="JS/date.js"></script>	
    </head>
	
    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="home">Inicio</a></li>
                        <!--<li><a href="goToAdicionarMedia">Adicionar Media</a></li>
                        <li><a href="goToEditarMedia">Editar Media</a></li>
                        <li><a href="goToEliminarMedia">Eliminar Media</a></li>-->
                        <li><a href="Logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="AdicionarSerie" method="post" class="ink-form all-100 small-100 tiny-100">
                    <fieldset>
                        <div class="control-group required column-group gutters">
                    		<label for="first-name" class="all-20 align-right">Titulo</label>
                            <div class="control all-25">
                                <input type="text" name="title" required value="${AdminAction.title}"> 
                            </div>
                        	<label for="first-name" class="all-20 align-right">Ano</label>
                            <div class="control all-25">
                                <input type="number" min="1900" name="year" required value="${AdminAction.year}">
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Genero</label>
                            <div class="control all-25">
                                <input type="text" name="genre" required value="${AdminAction.genre}">
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Rating</label>
                            <div class="control all-25">
                                <input type="text" min="0" name="rating" required value="${AdminAction.rating}">
                                <p class="tip">de 0-10</p>
                            </div>
                            <label for="first-name" class="all-20 align-right">Runtime</label>
	                        <div class="control all-25">
		                    	<input type="number" min="0" name="runtime" required value="${AdminAction.runtime}">
		                        <p class="tip">em minutos</p>
		                    </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Sinopse</label>
                            <div class="all-80">
                                <input type="text" class="textarea" name="sinopse"  required value="${AdminAction.sinopse}">
                            </div>
                        </div>
                        
                    	<div class="control-group required column-group gutters">
	                    	<label for="first-name" class="all-20 align-right">Diretor</label>
	                        <div class="control all-25">
		                    	<input type="text" name="director" required value="${AdminAction.director}">
		                    </div>
	                        
		                </div>
		                <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">URL Cover Image</label>
	                        <div class="control all-25">
		                    	<input type="url" name="urlCoverImage" required value="${AdminAction.urlCoverImage}">
		                    </div>
		                </div>
	                    <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">MPAA</label>
	                        <div class="control all-25">
		                    	<input type="text" name="MPAA" required value="${AdminAction.MPAA}">
		                    </div>
		                </div>
		                <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Network</label>
	                        <div class="control all-25">
		                    	<input type="text" name="fileLocation" required value="${AdminAction.network}">
		                    </div>
		                </div>
		                
		                <input type="submit" value="Adicionar Série" class="ink-button">    
                    </fieldset>
                </form>
			</div>		

<%@ include file= "../footer.html" %>