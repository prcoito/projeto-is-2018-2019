<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>
  
<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página de Registo</title>
	<script type="text/javascript" src="JS/date.js"></script>	
    </head>
	
    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="login">Inicio</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="register" method="post" class="ink-form all-100 small-100 tiny-100">
                    <fieldset>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Primeiro Nome</label>
                            <div class="control all-25">
                                <input type="text" name="firstName" required>
                            </div>
                        	<label for="first-name" class="all-20 align-right">Ultimo Nome</label>
                            <div class="control all-25">
                                <input type="text" name="lastName" required>
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Nome de Utilizador</label>
                            <div class="control all-25">
                                <input type="text" name="username" required>
                            </div>
                        </div>
                        <div class="control-group required column-group gutters">
                            <label for="first-name" class="all-20 align-right">Email</label>
                            <div class="control all-25">
                                <input type="email" name="emailR" required>
                            </div>
                        </div>
                    	<div class="control-group required column-group gutters">
	                    	<label for="first-name" class="all-20 align-right">Password</label>
	                        <div class="control all-25">
		                    	<input type="password" name="passwordR" required>
		                    </div>
	                        <label for="first-name" class="all-20 align-right">Repetir Password</label>
	                        <div class="control all-25">
		                    	<input type="password" name="passwordRepeat" required>
		                    </div>
		                </div>
	                    <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Numero Cartao de Credito</label>
	                        <div class="control all-25">
		                    	<input type="number" min="100000000" max="999999999" name="creditCardNumber" required>
		                    </div>
		                </div>
		                <div class="control-group required column-group gutters">
	                        <label for="first-name" class="all-20 align-right">Data Nascimento</label>
	                        <div class="control all-25">
		                    	<input id="birthDate" type="date" name="birthDate" max="2000-01-01" required>
		                    </div>
		                </div>
                    	<input type="submit" value="Submit" class="ink-button">    
                    </fieldset>
                </form>
			</div>		

<%@ include file= "../footer.html" %>