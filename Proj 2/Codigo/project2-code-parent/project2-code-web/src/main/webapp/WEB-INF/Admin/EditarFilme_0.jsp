<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix = "s" 	uri = "../struts-tags.tld" %>
<%@ taglib prefix = "c" 	uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fn"	uri = "http://java.sun.com/jsp/jstl/functions" %>
  
<%@ include file= "../header.html" %>
		
	<title>WebFlix - Página de Edição de Filme</title>
	<script type="text/javascript" src="JS/date.js"></script>	
    </head>
	
    <body>
        <div class="ink-grid">

            <!--[if lte IE 9 ]>
            <div class="ink-alert basic" role="alert">
                <button class="ink-dismiss">&times;</button>
                <p>
                    <strong>You are using an outdated Internet Explorer version.</strong>
                    Please <a href="http://browsehappy.com/">upgrade to a modern browser</a> to improve your web experience.
                </p>
            </div>
            -->

            <!-- Add your site or application content here -->

            <header>
                <h1>WebFlix Project<small>IS Paulo Coito</small></h1>
                <nav class="ink-navigation">
                    <ul class="menu horizontal black">
                        <li><a href="home">Inicio</a></li>
                        <li><a href="Logout">Logout</a></li>
                    </ul>
                </nav>
            </header>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionMessages()">
				   <div class="sucess">
				      <s:actionmessage/>
				   </div>
				</s:if>
			</div>
			
			<div class="all-100 small-100 tiny-100">
				<s:if test="hasActionErrors()">
				   <div class="error">
				      <s:actionerror/>
				   </div>
				</s:if>
			</div>
			
            <div class="column-group gutters">
                <form action="getInfoFilme" method="post" class="ink-form all-100 small-100 tiny-100">
                    <fieldset>
                        <select name="id" >
                        	<c:forEach items="${AdminAction.IDTitleandYearFilme}" var="info">
                        		<c:set var="id"				value="${fn:split(info, ';')[0]}"/>
                        		<c:set var="titleAndYear"	value="${fn:split(info, ';')[1]}"/>
                  
								<option value="<c:out value="${id}"/>">
									<c:out value="${titleAndYear}"/>
								</option>
							</c:forEach>
			                
                        </select>

                    	<input type="submit" value="Editar" class="ink-button">    
                    </fieldset>
                </form>
			</div>				

<%@ include file= "../footer.html" %>