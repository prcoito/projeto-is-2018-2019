
package action.interceptor;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import struts.action.LigacaoAction;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.interceptor.ValidationAware;

public class LigacaoInterceptor implements Interceptor {
	private static final long serialVersionUID = 189237412378L;
	private Logger logger = LoggerFactory.getLogger(LigacaoAction.class);
	
	public LigacaoInterceptor(){}
	
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void init() {
		// TODO Auto-generated method stub
		
	}
	
	private void addActionError(ActionInvocation invocation, String message) {
		Object action = invocation.getAction();
		if(action instanceof ValidationAware) {
			((ValidationAware) action).addActionError(message);
		}
	}
	
	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext().getSession();
		// this method intercepts the execution of the action and we get access
		// to the session, to the action, and to the context of this invocation
		if( session.containsKey("loggedin") && (Boolean) session.get("loggedin"))
		{		
			String actionName = invocation.getAction().toString();
			
			logger.debug("[INTERCEPTOR] Role: " + session.get("role"));
			logger.debug("[INTERCEPTOR] Action: " + actionName);
			
			if ( actionName.contains("Admin") && !session.get("role").equals("ADMIN") )
			{
				addActionError(invocation, "Não tem permissoes para aceder a esta funcionalidade.");
				return "login";
			}
			else if ( session.get("role").equals("ADMIN")  && !actionName.contains("Admin") && !isHREF(actionName) ){
				addActionError(invocation, "Não tem permissoes para aceder a esta funcionalidade.");
				return "login";
			}
			
			return invocation.invoke();
		}
		
		addActionError(invocation, "Necessita de fazer o login para aceder a esta funcionalidade.");
		return "login";
	}

	private boolean isHREF(String actionName) {
		String[] href = {"linkToRegistar", "home", "Contactos","About", "xwork2.ActionSupport"};
		
		for (String s : href)
		{ 
			if(actionName.contains(s))
				return true;
		}
		
		return false;
	}
	
}