package struts.action;

import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import struts.ligacao.Admin;

import com.opensymphony.xwork2.ActionSupport;

import ejb.model.LigacaoAdminBean;

public class AdminAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;
	private Map<String, Object> session;

	private Logger logger = LoggerFactory.getLogger(AdminAction.class);
	
	private LigacaoAdminBean lab;
	
	private String type, id, title, year, genre, director, season, episode, rating, sinopse, runtime, urlCoverImage, MPAA, fileLocation, wallpaper;
	private String network;
	private String tvShowID;
	private Admin admin;
	
	public AdminAction(){
		lab = new LigacaoAdminBean();
		
	}
	public AdminAction(LigacaoAdminBean ligacaoAdminBean){
		logger.debug("Ligacao inicializado");
		setAdminBean(ligacaoAdminBean);
	}

	public AdminAction(Admin a) {
		setAdmin(a);
	}
	@Override
	public String execute(){
		return SUCCESS;
	}
	
	public String goToAdicionarMedia(){
		logger.debug("Type="+type);
		return type;
	}
	public String goToEliminarMedia(){
		logger.debug("Type="+type);
		return type;
	}
	public String goToEditarMedia(){
		logger.debug("Type="+type);
		return type;
	}
	
	public String AdicionarFilme(){
		logger.debug("A adicionar filme...");
		String result = getLigacaoAdminBean().AdicionarFilme(title, year, genre, rating,  sinopse, director, runtime,  urlCoverImage, MPAA, fileLocation, wallpaper);
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Filme "+title + " ("+ year +") adicionado com sucesso.");
			setAllEmpty();
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	
	public String EditarFilme(){
		logger.debug("A editar filme...");
		String result = getLigacaoAdminBean().EditarFilme(id, title, year, genre, rating,  sinopse, director, runtime,  urlCoverImage, MPAA, fileLocation, wallpaper);
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Filme "+title + " ("+ year +") editado com sucesso.");
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	public String EliminarFilme(){
		logger.debug("A eliminar filme...");
		String result = getLigacaoAdminBean().EliminarFilme(id);
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Filme com id "+id + " eliminado com sucesso.");
			
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	
	
	public String AdicionarSerie(){
		logger.debug("A adicionar serie...");
		String result = getLigacaoAdminBean().AdicionarSerie(title, year, genre, rating, sinopse, director, runtime, network, urlCoverImage, MPAA);
				
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			logger.debug("Serie "+title + " ("+ year +") adicionado com sucesso.");
			addActionMessage("Serie "+title + " ("+ year +") adicionado com sucesso.");
			setAllEmpty();
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	public String EditarSerie(){
		logger.debug("A editar serie...");
		String result = getLigacaoAdminBean().EditarSerie(id, title, year, genre, rating, sinopse, director, runtime, network, urlCoverImage, MPAA);
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("serie "+title + " ("+ year +") editado com sucesso.");
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	public String EliminarSerie(){
		logger.debug("A eliminar serie...");
		String result = getLigacaoAdminBean().EliminarSerie(id);
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("serie com id "+id + " eliminado com sucesso.");
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	
	public String AdicionarEpisodio(){
		logger.debug("A adicionar Episodio...");
		String result = getLigacaoAdminBean().AdicionarEpisodioSerie(tvShowID, title, year, genre, season, episode, rating, sinopse, runtime, urlCoverImage, fileLocation, wallpaper);
				//.AdicionarEpisodioSerie(getTvShowID(), title, year, genre, season, episode, rating, sinopse, runtime, urlCoverImage, fileLocation, wallpaper);
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Episodio "+title + " ("+ year +") adicionado com sucesso.");
			setAllEmpty();
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	public String EditarEpisodio(){
		logger.debug("A editar serie...");
		String result = getLigacaoAdminBean().EditarEpisodioSerie(id, tvShowID, title, year, genre, season, episode, rating, sinopse, runtime, urlCoverImage, fileLocation, wallpaper);
		getAdmin().setTitle(title);
		getAdmin().setYear(year);
		getAdmin().setDirector(director);
		getAdmin().setGenre(genre);
		getAdmin().setSeason(season);
		getAdmin().setEpisode(episode);
		getAdmin().setRating(rating);
		getAdmin().setSinopse(sinopse);
		getAdmin().setRuntime(runtime);
		getAdmin().setMPAA(MPAA);
		getAdmin().setUrlCoverImage(urlCoverImage);
		getAdmin().setFileLocation(fileLocation);
		getAdmin().setWallpaper(wallpaper);
		getAdmin().setNetwork(network);
		getAdmin().setTvShowID(tvShowID);
		
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Episodio "+title + " ("+ year +") editado com sucesso.");
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	public String EliminarEpisodio(){
		logger.debug("A eliminar serie...");
		String result = getLigacaoAdminBean().EliminarEpisodio(id);
		logger.debug("A adicionar mensagem para o manager...");
		if ( result.equals("OK") ){
			addActionMessage("Episodio com id "+id + " eliminado com sucesso.");
		}
		else{
			addActionError(result);
		}
		logger.debug("Concluido.");
		return "OK";
	}
	
	public String getInfoFilme(){
		ArrayList<String> r = getLigacaoAdminBean().getInfo("Movie", id);
		
		getAdmin().setId(r.get(0));
		getAdmin().setTitle(r.get(1));
		getAdmin().setYear(r.get(2));
		getAdmin().setGenre(r.get(3));
		getAdmin().setRating(r.get(4));
		getAdmin().setSinopse(r.get(5));
		getAdmin().setDirector(r.get(6));
		getAdmin().setRuntime(r.get(7));
		getAdmin().setUrlCoverImage(r.get(8));
		getAdmin().setMPAA(r.get(9));
		getAdmin().setFileLocation(r.get(10));
		getAdmin().setWallpaper(r.get(11));
		
		logger.debug("OK");
		return "OK";
	}
	
	public String getInfoSerie(){
		ArrayList<String> r = getLigacaoAdminBean().getInfo("TVShow", id);
		logger.debug("Info = "+r.toString());
		getAdmin().setId(r.get(0));
		getAdmin().setTitle(r.get(1));
		getAdmin().setYear(r.get(2));
		getAdmin().setGenre(r.get(3));
		getAdmin().setRating(r.get(4));
		getAdmin().setSinopse(r.get(5));
		getAdmin().setDirector(r.get(6));
		getAdmin().setRuntime(r.get(7));
		getAdmin().setUrlCoverImage(r.get(8));
		getAdmin().setMPAA(r.get(9));
		getAdmin().setNetwork(r.get(10));
		logger.debug("Type = " + type);
		return "OK";
	}
	
	public String getInfoEpisodio(){
		ArrayList<String> r = getLigacaoAdminBean().getInfo("TVShowEpisode", id);
		
		getAdmin().setId(r.get(0));
		getAdmin().setTitle(r.get(1));
		getAdmin().setYear(r.get(2));
		getAdmin().setGenre(r.get(3));
		getAdmin().setRating(r.get(4));
		getAdmin().setSinopse(r.get(5));
		getAdmin().setRuntime(r.get(6));
		getAdmin().setUrlCoverImage(r.get(7));
		getAdmin().	setFileLocation(r.get(8));
		getAdmin().	setWallpaper(r.get(9));
		getAdmin().	setSeason(r.get(10));
		getAdmin().	setEpisode(r.get(11));
		getAdmin().	setTvShowID(r.get(12));
		return "OK";
	}
	
	public ArrayList<String> getIDTitleandYearFilme(){
		logger.debug("Getting info of all movies ");
		return getLigacaoAdminBean().getAllMoviesIDTitleAndYear();
	}
	public ArrayList<String> getIDTitleandYearSerie(){
		logger.debug("Getting info of all series ");
		return getLigacaoAdminBean().getAllSeriesIDTitleAndYear();
	}
	public ArrayList<String> getIDTitleandYearEpisode(){
		logger.debug("Getting info of all episodes ");
		return getLigacaoAdminBean().getAllEpisodesIDTitleAndYear();
	}
	
	private void setAllEmpty() {
		logger.debug("setting all empty");
		getAdmin().setId("");
		getAdmin().setTitle("");
		getAdmin().setYear("");
		getAdmin().setDirector("");
		getAdmin().setGenre("");
		getAdmin().setSeason("");
		getAdmin().setEpisode("");
		getAdmin().setRating("");
		getAdmin().setSinopse("");
		getAdmin().setRuntime("");
		getAdmin().setMPAA("");
		getAdmin().setUrlCoverImage("");
		getAdmin().setFileLocation("");
		getAdmin().setWallpaper("");
		getAdmin().setNetwork("");
		getAdmin().setTvShowID("");
		
		logger.debug("done");
	}
	
	public String logout(){
		session.clear();
		session = null;
		addActionMessage("Logout efectuado com sucesso");
		return "OK";
	}
	
	public LigacaoAdminBean getAdminBean() {
		return getLigacaoAdminBean();
	}
	public void setAdminBean(LigacaoAdminBean ab) {
		setLigacaoAdminBean(ab);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		if ( title == null)
			return "";
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		if ( year == null)
			return "";
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getGenre() {
		if ( genre == null)
			return "";
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSeason() {
		if ( season == null)
			return "";
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getEpisode() {
		if ( episode == null)
			return "";
		return episode;
	}
	public void setEpisode(String episode) {
		this.episode = episode;
	}
	public String getRating() {
		if ( rating == null)
			return "";
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getSinopse() {
		if ( sinopse == null)
			return "";
		return sinopse;
	}
	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	public String getRuntime() {
		if ( runtime == null)
			return "";
		return runtime;
	}
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	public String getUrlCoverImage() {
		if ( urlCoverImage == null)
			return "";
		return urlCoverImage;
	}
	public void setUrlCoverImage(String urlCoverImage) {
		this.urlCoverImage = urlCoverImage;
	}
	public String getFileLocation() {
		if ( fileLocation == null)
			return "";
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public String getWallpaper() {
		if ( wallpaper == null)
			return "";
		return wallpaper;
	}
	public void setWallpaper(String wallpaper) {
		this.wallpaper = wallpaper;
	}
	public Map<String, Object> getSession() {
		return session;
	}
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	public String getType() {
		if ( type == null)
			return "";
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public LigacaoAdminBean getLigacaoAdminBean() {
		if ( lab == null )
			lab = new LigacaoAdminBean();
		return lab;
	}
	public void setLigacaoAdminBean(LigacaoAdminBean lab) {
		this.lab = lab;
	}
	public String getMPAA() {
		if ( MPAA == null)
			return "";
		return MPAA;
	}
	public void setMPAA(String mPAA) {
		MPAA = mPAA;
	}
	public String getDirector() {
		if ( director == null)
			return "";
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getNetwork() {
		if ( network == null)
			return "";
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getTvShowID() {
		if ( title == null)
			tvShowID = "";
		return tvShowID;
	}
	public void setTvShowID(String tvShowID) {
		this.tvShowID = tvShowID;
	}
	public Admin getAdmin() {
		if ( admin == null )
			admin = new Admin(getLigacaoAdminBean());
		return admin;
	}
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	
}
