package struts.ligacao;

import ejb.model.LigacaoAdminBean;

public class Admin {
	
	private String type, id, title, year, genre, director, season, episode, rating, sinopse, runtime, urlCoverImage, MPAA, fileLocation, wallpaper;
	private String network;
	private String tvShowID;
	private LigacaoAdminBean ab;
	
	
	public Admin(LigacaoAdminBean ligacaoBean) {
		this.setAb(ligacaoBean);
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		if ( title == null)
			return "";
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		if ( year == null)
			return "";
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getGenre() {
		if ( genre == null)
			return "";
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getSeason() {
		if ( season == null)
			return "";
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public String getEpisode() {
		if ( episode == null)
			return "";
		return episode;
	}
	public void setEpisode(String episode) {
		this.episode = episode;
	}
	public String getRating() {
		if ( rating == null)
			return "";
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getSinopse() {
		if ( sinopse == null)
			return "";
		return sinopse;
	}
	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	public String getRuntime() {
		if ( runtime == null)
			return "";
		return runtime;
	}
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	public String getUrlCoverImage() {
		if ( urlCoverImage == null)
			return "";
		return urlCoverImage;
	}
	public void setUrlCoverImage(String urlCoverImage) {
		this.urlCoverImage = urlCoverImage;
	}
	public String getFileLocation() {
		if ( fileLocation == null)
			return "";
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public String getWallpaper() {
		if ( wallpaper == null)
			return "";
		return wallpaper;
	}
	public void setWallpaper(String wallpaper) {
		this.wallpaper = wallpaper;
	}
	public String getType() {
		if ( type == null)
			return "";
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMPAA() {
		if ( MPAA == null)
			return "";
		return MPAA;
	}
	public void setMPAA(String mPAA) {
		MPAA = mPAA;
	}
	public String getDirector() {
		if ( director == null)
			return "";
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getNetwork() {
		if ( network == null)
			return "";
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public String getTvShowID() {
		if ( tvShowID == null)
			tvShowID = "";
		return tvShowID;
	}
	public void setTvShowID(String tvShowID) {
		this.tvShowID = tvShowID;
	}


	public LigacaoAdminBean getAb() {
		return ab;
	}


	public void setAb(LigacaoAdminBean ab) {
		this.ab = ab;
	}


	
}
