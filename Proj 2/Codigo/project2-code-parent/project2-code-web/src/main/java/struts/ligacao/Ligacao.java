package struts.ligacao;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ejb.model.LigacaoUserBean;

public class Ligacao {
	private Logger logger = LoggerFactory.getLogger(Ligacao.class);
		
	private static LigacaoUserBean ligacaoBean;

	public Ligacao(LigacaoUserBean lb){
		logger.debug("Ligacao initcializado");
		ligacaoBean = lb;
	}
	public Ligacao() {
		// TODO Auto-generated constructor stub
	}
	/*
	 * getters comuns a varias paginas
	 */
	public String getUsername(){
		return getLigacaoBean().getUsername();
	}
	
	/*
	 * getters home.jsp
	 */
	public ArrayList<String> getSomeRandomMovies(){
		return getLigacaoBean().getSomeRandomMovies();
	}
	public ArrayList<String> getSomeRandomTV(){
		return getLigacaoBean().getSomeRandomTV();
	}
	/*
	 * getters da pagina tvShowPage.jsp 
	 */
	public String getTVShowPoster(){
		return getLigacaoBean().getTVShowPoster();
	}
	public String getTVShowTitle(){
		return getLigacaoBean().getTVShowTitle();
	}
	public String getTVShowYear(){
		return getLigacaoBean().getTVShowYear();
	}
	public String getTVShowRating(){
		return getLigacaoBean().getTVShowRating();
	}
	public String getTVShowSinopse(){
		return getLigacaoBean().getTVShowSinopse();
	}
	public String getTVShowDirector(){
		return getLigacaoBean().getTVShowDirector();
	}
	public ArrayList<String> getTVShowCast(){
		return getLigacaoBean().getTVShowCast();
	}
	public String getTVShowGenre(){
		return getLigacaoBean().getTVShowGenre();
	}
	public String getTVShowMPAA(){
		return getLigacaoBean().getTVShowMPAA();
	}
	public String getTVShowRuntime(){
		return getLigacaoBean().getTVShowRuntime();
	}
	public String getEpisodeFileLocation(){
		return getLigacaoBean().getEpisodeFileLocation();
		
	}
	public String getIDTVShow(){
		return getLigacaoBean().getIDTVShow();
	}
	public String getTVShowID(int id){
		return getLigacaoBean().getTVShowID(id);
	}
	
	public static int numEpisodesInSeason(int id, int season){
		return getLigacaoBean().getTVShowNumberEpisodesInSeason(id, season);
	}
	public static int getNumberSeasons(int id){
		return getLigacaoBean().getTVShowNumberSeasons(id);
	}
	public static int getTVShowNumberEpisodesInSeason(int id, int season){
		return getLigacaoBean().getTVShowNumberEpisodesInSeason(id,season);
	}
	@Deprecated
	public static int getNumberSeparators(int id,int season){
		return (int) Math.ceil((double)numEpisodesInSeason(id, season) / 5);				// /5 pois aparecem 5 episodios no ecra
	}
	public static String getEpisodeTitle(int id,int season, int episode){
		return getLigacaoBean().getEpisodeTitle(id,season, episode);
	}
	public static String getEpisodeSinopse(int id,int season, int episode){
		return getLigacaoBean().getEpisodeSinopse(id,season, episode);
	}
	public static String getEpisodeWallpaper(int id,int season, int episode){
		return getLigacaoBean().getEpisodeWallpaper(id,season, episode);
	}
	public String getIDEpisode(){
		return getLigacaoBean().getEpisodeID();
	}public static String getEpisodeID(int id,int season, int episode){
		return getLigacaoBean().getEpisodeID(id, season, episode);
	}
	/*
	 * Metodos para pagina tvShowEpisode.jsp
	 */
	public String getEpisodeWallpaper(){
		return getLigacaoBean().getEpisodeWallpaper();
	}
	public String getEpisodeTitle(){
		return getLigacaoBean().getEpisodeTitle();
	}
	public String getEpisodeYear(){
		return getLigacaoBean().getEpisodeYear();
	}
	public String getEpisodeSeason(){
		return getLigacaoBean().getEpisodeSeason();
	}
	public String getEpisodeEpisode(){
		return getLigacaoBean().getEpisodeEpisode();
	}
	public String getEpisodeRating(){
		return getLigacaoBean().getEpisodeRating();
	}
	public String getEpisodeSinopse(){
		return getLigacaoBean().getEpisodeSinopse();
	}
	public String getEpisodeRuntime(){
		return getLigacaoBean().getEpisodeRuntime();
	}
	public String getEpisodePoster(){
		return getLigacaoBean().getEpisodePoster();
	}
	
	/*
	 * getters da pagina moviePage.jsp 
	 */
	public String getMoviePoster(){
		return getLigacaoBean().getMoviePoster();
	}
	public String getMovieTitle(){
		return getLigacaoBean().getMovieTitle();
	}
	public String getMovieYear(){
		return getLigacaoBean().getMovieYear();
	}
	public String getMovieRating(){
		return getLigacaoBean().getMovieRating();
	}
	public String getMovieSinopse(){
		return getLigacaoBean().getMovieSinopse();
	}
	public String getMovieDirector(){
		return getLigacaoBean().getMovieDirector();
	}	
	public ArrayList<String> getMovieCast(){
		return getLigacaoBean().getMovieCast();
	}
	public String getMovieGenre(){
		return getLigacaoBean().getMovieGenre();
	}
	public String getMovieMPAA(){
		return getLigacaoBean().getMovieMPAA();
	}
	public String getMovieRuntime(){
		return getLigacaoBean().getMovieRuntime();
	}
	public String getMovieFileLocation(){
		return getLigacaoBean().getMovieFileLocation();
	}
	public String getIDMovie(){
		return getLigacaoBean().getIDMovie();
	}
	public String getMovieWallpaper(){
		return getLigacaoBean().getMovieWallpaper();
	}
	
	/*
	 * getters da pagina editProfile.jsp 
	 */
	public String getUserID(){
		return getLigacaoBean().getUserID();
	}
	public String getUserFirstName(){
		return getLigacaoBean().getUserFirstName();
	}
	public String getUserLastName(){
		return getLigacaoBean().getUserLastName();
	}
	public String getUserUserName(){
		return getLigacaoBean().getUserUserName();
	}
	public String getUserEmail(){
		return getLigacaoBean().getUserEmail();
	}
	public String getUserCreditCardNumber(){
		return getLigacaoBean().getUserCreditCardNumber();
	}
	public String getUserBirthDate(){
		return getLigacaoBean().getUserBirthDate();
	}
	public String getMovieWatchListIcon(){ 
		return getLigacaoBean().getMovieWatchListIcon();
	}
	public String getTVShowWatchListIcon(){ 
		return getLigacaoBean().getTVShowWatchListIcon();
	}
	public String getEpisodeWatchListIcon(){ 
		return getLigacaoBean().getEpisodeWatchListIcon();
	}
	/*
	 * getters pagina SearchResult.jsp
	 */
	public ArrayList<String> getResultList(){
		return getLigacaoBean().getResultList();
	}
	public String getSearchTerm(){
		return getLigacaoBean().getSearchTitle();
	}
	public boolean isFirstResultsPage(){
		return getLigacaoBean().isFirstResultsPage();
	}
	public String getIsFirstResultsPage(){
		logger.debug("Boolean.toString(getLigacaoBean().isFirstResultsPage()) = " +Boolean.toString(getLigacaoBean().isFirstResultsPage()));
		if ( getLigacaoBean().isFirstResultsPage() )
			return "1";
		return "0";
	}
	public boolean isLastResultsPage(){
		return getLigacaoBean().isLastResultsPage();
	}
	public String getIsLastResultsPage(){
		logger.debug("Boolean.toString(getLigacaoBean().isLastResultsPage()) = " +Boolean.toString(getLigacaoBean().isLastResultsPage()));
		if ( getLigacaoBean().isLastResultsPage() )
			return "1";
		return "0";
	}
	public String getSearchType(){
		return getLigacaoBean().getSearchType();
	}
	public String getSearchTitle(){
		return getLigacaoBean().getSearchTitle();
	}
	public String getSearchDirector(){
		return getLigacaoBean().getSearchDirector();
	}
	public String getSearchGen(){
		return getLigacaoBean().getSearchGen();
	}
	public String getSearchStartYear(){
		return getLigacaoBean().getSearchStartYear();
	}
	public String getSearchEndYear(){
		return getLigacaoBean().getSearchEndYear();
	}
	public String getSearchOrderBy(){
		return getLigacaoBean().getSearchOrderBy();
	}
	public String getSearchOrderByOrder(){
		return getLigacaoBean().getSearchOrderByOrder();
	}
	public String getSearchCurrentResultsPage(){
		return Integer.toString(getLigacaoBean().getSearchPage());
	}
	public String getPreviousPageNumber(){
		return Integer.toString(getLigacaoBean().getSearchPage()-1);
	}
	public String getNextPageNumber(){
		return Integer.toString(getLigacaoBean().getSearchPage()+1);
	}
	
	/*
	 * Metodos pagina watchlist
	 */
	public ArrayList<String> getMovieWatchList(){
		return getLigacaoBean().getMovieWatchList();
	}
	public ArrayList<String> getEpisodesWatchList(){
		return getLigacaoBean().getEpisodesWatchList();
	}
	
	
	public static LigacaoUserBean getLigacaoBean() {
		return ligacaoBean;
	}
	public void setLigacaoBean(LigacaoUserBean lb) {
		ligacaoBean = lb;
	}
}
