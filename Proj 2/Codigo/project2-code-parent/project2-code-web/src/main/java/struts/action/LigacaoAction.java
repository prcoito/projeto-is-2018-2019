package struts.action;
import ejb.bean.AdminBean;
import ejb.bean.UserBean;
import ejb.model.LigacaoAdminBean;
import ejb.model.LigacaoUserBean;

import com.opensymphony.xwork2.ActionSupport;

import org.apache.struts2.interceptor.SessionAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import struts.ligacao.Admin;
import struts.ligacao.Ligacao;

import java.util.Map;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;



public class LigacaoAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 5590830L;

	private Logger logger = LoggerFactory.getLogger(LigacaoAction.class);
	private Map<String, Object> session;
	
	private String email = null, password = null;
	// Vars para registo
	private String 	firstName = null, lastName = null, username = null, emailR = null,
					passwordO = null, passwordR = null, passwordRepeat = null, 
					creditCardNumber = null, birthDate = null;

	// vars para action de mostar pagina de filme
	private String movieTitle;
	private String movieYear;
	private String movieID;
	
	// vars para action de mostar pagina de tvshow
	private String tvshowTitle;
	private String tvshowYear;
	private String TVShowID;
	
	// var para action de mostrar pagina de episodio de tvshow
	private String episodeID;

	private String season, episode;

	// usada na pagina de edicao do perfil do utilizador ( botao para eliminar )
	private int userID;
	
	// para barra pesquisa rapida
	private String searchTerm;
	
	//para pesquisa personalizada
	private String type, title, director, startYear, endYear, searchGen, OrderBy, OrderByType;
	
	// para pagina dos resultados da pesquisa
	private String mediaType;
	private int id, page;

	@Inject
	private UserBean ub;

	public void SearchForBean(){
		try {
			if (ub==null){
				logger.debug("User bean not injected. Trying lookup...");
			}
			ub = (UserBean) new InitialContext().lookup("java:global/project2-code-ear/IS-project2-code-business-0.0.1-SNAPSHOT/UserBean");
			logger.debug("Lookup sucessful: ub= "+ub);
			logger.debug("ub = "+ub);
			getLigacaoUserBean().setUb(ub);
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String execute(){
		SearchForBean();
		
		
		String result;
		logger.debug("Begining login method");
		
		if (this.session.containsKey("loggedin") && (Boolean) this.session.get("loggedin"))
		{
			logger.debug("Ja tinha efetuado o login...");
			return (String) this.session.get("role");
		}
		
		if(this.email != null && !email.equals("") && this.password != null && !password.equals("")){		
			getLigacaoUserBean().setEmail(email);
			getLigacaoUserBean().setPassword(password);
			
			result= getLigacaoUserBean().getUserMatchesPassword().toUpperCase();
			
			switch (result) {
			case "NORMAL":
			{
				session.put("email", email);
				session.put("loggedin", true);
				session.put("role", "NORMAL");
				return result;
			}
			case "ADMIN":
			{
				session.put("email", email);
				session.put("loggedin", true);
				session.put("role", "ADMIN");
				//setLigacao(new Ligacao(getLigacaoUserBean()));
				Admin a = new Admin(new LigacaoAdminBean());
				session.put("Admin", a);
				session.put("AdminAction", new AdminAction(a));
				//getLigacaoAdminBean().setTipoPessoa(result);
				return result;
			}
			case "NOUSER":
			{
				logger.debug("Email nao existe.");
				logger.debug("Eding login method");
				addActionError("Email não existe.");
				break;
			}
			case "WRONGPASS":
			{
				logger.debug("Password incorretos.");
				logger.debug("Eding login method");
				addActionError("Password incorreta.");
				break;
			}
			default:
				logger.debug("Username ou password incorretos.");
				logger.debug("Eding login method");
				addActionError("Email ou password incorretos.");
				break;
			}
			
			/*
			if( result.equals("NORMAL") || result.equals("ADMIN") ){
				if (result.equals("NORMAL"))
					getLigacaoBean().setTipoPessoa(result);
				else
					getAdminBean().setTipoPessoa(result);
				session.put("email", email);
				session.put("loggedin", true); // this marks the user as logged in
				
				return result;
			}
			else if (result.equals("NOUSER") ) {
				logger.debug("mail nao existe.");
				logger.debug("Eding login method");
				addActionError("Email nao existe.");
				//return "LOGIN";
			}
			
			else {
				logger.debug("Username ou password incorretos.");
				logger.debug("Eding login method");
				addActionError("Email ou password incorretos.");
				//return "LOGIN";
			}*/
		}
		else
		{
			addActionError("Por favor insira o username e password.");
			logger.debug("Adding action error: \"Por favor insira o username e password.\"");
			logger.debug("Ending login method");
			//return LOGIN;
		}
		return "LOGIN";
	}
	
	
	/*
	 * Metodo de registo de um utilizador
	 * 
	 * */
	
	public String register(){
		removeDataFromSession();
		SearchForBean();
		
		String result = this.getLigacaoUserBean()
							.register(	username, firstName, lastName, emailR, 
										passwordR, passwordRepeat, 
										creditCardNumber, birthDate, "normal");
		// reset vars
		if (result.equals("OK"))
		{
			logger.debug("registo completo. adicionar a action message");
			addActionMessage("Registo completado com sucesso!\nPode fazer o login.");
			logger.debug("action message adicionada");
		}
		else
		{
			logger.debug("registo nao completo. adicionar a action error");
			addActionError(result);
			logger.debug("action error adicionada");
			result = "NOTOK";
		}
		logger.debug("returnar a struts2");
		//removeDataFromSession();
		return result;
	}

	public void removeDataFromSession(){
		
		session.remove("loggedin");
		session.remove("ligacaoBean");
		session.remove("ligacao");
		session.clear();
		//session = null;
	}
	public String logout(){
		removeDataFromSession();
		addActionMessage("Logout efectuado com sucesso!");
		return "OK";
	}
	
	public String getUserInfo(){
		logger.debug("A obter info do user "+getUsername());
		String result = getLigacaoUserBean().getUser(getUsername());
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
	}
	
	public String editUser(){
		logger.debug("A obter info do user "+getUsername());
		String result = getLigacaoUserBean().editUser(	username, firstName, lastName, emailR, 
													passwordO, passwordR, passwordRepeat, 
													creditCardNumber, birthDate);
		
		if (result.equals("OK"))
		{
			addActionMessage("Alterações efectuadas com sucesso!");
			logger.debug("Correu tudo bem");
			//return "OK";
		}
		else if (!result.equals("ERRO"))
		{
			addActionMessage(result);
			logger.debug("Surgiu um erro");
		}
		else
			return "ERRO";

		return "OK";	//ok pois ou correu bem ou o erro será apresentado ao user
	}
	public String deleteUser(){
		logger.debug("Action para eliminar user acionada.");
		String result = getLigacaoUserBean().deleteUser(getUserID());
		if ( result.equals("OK") )
		{
			logger.debug("Action para eliminar user concluida com sucesso.");
			addActionMessage("A sua conta foi eliminada com sucesso.");
			removeDataFromSession();
			return "OK";
		}
		else{
			addActionError("Ocorreu um Erro inesperado. Tente novamente mais tarde.");
			return "NOTOK";
		}
	}
	public String getInfoMovie(){	
		logger.debug("A obter info do filme "+id);
		//String result = getLigacaoUserBean().getMovie(movieTitle, movieYear);
		String result = getLigacaoUserBean().getMovieByID(id);
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
		
	}
	public String getInfoTVShow(){
		logger.debug("A obter info da serie "+id);
		//String result = getLigacaoUserBean().getTVShow(tvshowTitle, tvshowYear);
		String result = getLigacaoUserBean().getTVShowByID(id);
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
	}
	public String getMedia(){
		String result;
		if(getMediaType().equals("Movie")){
			result = getLigacaoUserBean().getMovieByID(getId());
			if (result.equals("OK"))
				return "MOVIE";
			else
				logger.debug("Surgiu um erro");
			return result;
		}
		else if(getMediaType().equals("TVShow") ){
			result = getLigacaoUserBean().getTVShowByID(getId());
			if (result.equals("OK"))
				return "TVSHOW";
			else
				logger.debug("Surgiu um erro");
			return result;
		}
		else{
			result = getLigacaoUserBean().getTVShowEpisodeByID(getId());
			if (result.equals("OK"))
				return "TVSHOW";
			else
				logger.debug("Surgiu um erro");
			return result;
		}
		
		
	}
	
	public String getInfoEpisodes(){
		logger.debug("A obter info do episodio S"+getSeason()+"E"+getEpisode()+" da serie "+tvshowTitle);
		String result = getLigacaoUserBean().getTVShowEpisode(getSeason(), getEpisode());
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		logger.info("A redirecionar para pagina tvShowEpisode.jsp");
		return result;
	}
	public String getInfoTVShowEpisode(){
		logger.debug("A obter info do episodio S"+getSeason()+"E"+getEpisode()+" da serie "+tvshowTitle);
		String result = getLigacaoUserBean().getTVShowEpisode(episodeID);
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		logger.info("A redirecionar para pagina tvShowEpisode.jsp");
		return result;
	}
	public String addRemoveShowToWatchList(){
		logger.debug("A adicionar serie "+tvshowTitle+" à watchlist do user "+getLigacaoUserBean().getUsername());
		String result = getLigacaoUserBean().addRemoveTVShowToWatchList(Integer.parseInt(getTVShowID()));
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
	}
	public String addRemoveEpisodeShowToWatchList(){
		logger.debug("A adicionar serie "+tvshowTitle+" à watchlist do user "+getLigacaoUserBean().getUsername());
		String result = getLigacaoUserBean().addRemoveTVShowEpisodeToWatchList(Integer.parseInt(episodeID));
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
	}
	public String addRemoveMovieToWatchList(){
		logger.debug("A adicionar movie "+getLigacaoUserBean().getMovieTitle()+" à watchlist do user "+getLigacaoUserBean().getUsername());
		String result = getLigacaoUserBean().addRemoveMovieToWatchList(Integer.parseInt(movieID));
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		return result;
	}
	
	/*
	 * Metodos de pesquisa
	 */
	public String setPesquisaRapida(){
		logger.debug("Setting seachTearm "+searchTerm );
		getLigacaoUserBean().setQuickSearch(searchTerm);
		logger.debug("A redirecionar para pagina /WEB-INF/SearchResult.jsp");
		return "OK";
	}
	public String pesquisaPersonalizada(){
		logger.info("A pesquisar");
		logger.debug("type: " +type);
		logger.debug("title: " +title);
		logger.debug("director: " +director);
		logger.debug("searchGen: " +searchGen);
		logger.debug("startYear: " +startYear);
		logger.debug("endYear: " +endYear);
		logger.debug("OrderBy" +OrderBy);
		logger.debug("OrderByType: " +OrderByType);
		logger.debug("page: " +page);																							// Page MaxResultsPerPage
		String result = getLigacaoUserBean()
						.pesquisaPersonalizada(type, title, director, searchGen, startYear, endYear, OrderBy, OrderByType, 	page,		10);
		
		if (result.equals("OK"))
			logger.debug("Correu tudo bem");
		else
			logger.debug("Surgiu um erro");
		logger.info("A redirecionar para pagina /WEB-INF/SearchResult.jsp");
		return result;
		
	}
	
	
	
	/*
	 *	SETTER E GETTERS 
	 */
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String mail) {
		this.email = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Map<String, Object> getSession() {
		return session;
	}
	
	
	public LigacaoAction() {}
	
	public LigacaoAction(Map<String, Object> session, String username,
			String password) {
		super();
		this.session = session;
		this.setUsername(username);
		this.password = password;
	}
	
	public AdminBean getLigacaoAdminBean() {
		if(!session.containsKey("adminBean"))
			this.setAdminBean(new AdminBean());
		return (AdminBean) session.get("adminBean");
	}
	public void setAdminBean(AdminBean adminBean) {
		this.session.put("adminBean", adminBean);
	}
	public LigacaoUserBean getLigacaoUserBean() {
		if(!session.containsKey("ligacaoBean"))
			this.setLigacaoBean(new LigacaoUserBean());
			
		return (LigacaoUserBean) session.get("ligacaoBean");
	}
	public void setLigacaoBean(LigacaoUserBean ligacaoBean) {
		this.session.put("ligacaoBean", ligacaoBean);
		
	}
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	public Ligacao getLigacao() {
		if(!session.containsKey("ligacao"))
			setLigacao(new Ligacao(getLigacaoUserBean()));
	
		return (Ligacao) session.get("ligacao");
		
	}
	public void setLigacao(Ligacao ligacao) {
		this.session.put("ligacao", ligacao);
	}
	
	
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsernameR(String usernameR) {
		this.setUsername(usernameR);
	}

	public String getEmailR() {
		return email;
	}

	public void setEmailR(String email) {
		this.emailR = email;
	}

	public String getPasswordR() {
		return passwordR;
	}

	public void setPasswordR(String passwordR) {
		this.passwordR = passwordR;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public void setMovieTitle(String title){
		this.movieTitle = title;
	}	
	public String getMovieTitle(){
		return this.movieTitle;
	}
	public String getTvshowTitle() {
		return tvshowTitle;
	}
	public void setTvshowTitle(String tvshowTitle) {
		this.tvshowTitle = tvshowTitle;
	}
	public String getMovieID() {
		return movieID;
	}
	public void setMovieID(String movieID) {
		this.movieID = movieID;
	}


	public String getTVShowID() {
		return TVShowID;
	}


	public void setTVShowID(String tVShowID) {
		TVShowID = tVShowID;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPasswordO() {
		return passwordO;
	}


	public void setPasswordO(String passwordO) {
		this.passwordO = passwordO;
	}


	public String getSeason() {
		return season;
	}


	public void setSeason(String season) {
		this.season = season;
	}


	public String getEpisode() {
		return episode;
	}


	public void setEpisode(String episode) {
		this.episode = episode;
	}


	public int getUserID() {
		return userID;
	}


	public void setUserID(int userID) {
		this.userID = userID;
	}


	public String getEpisodeID() {
		return episodeID;
	}


	public void setEpisodeID(String episodeID) {
		this.episodeID = episodeID;
	}


	public String getMovieYear() {
		return movieYear;
	}


	public void setMovieYear(String movieYear) {
		this.movieYear = movieYear;
	}


	public String getTvshowYear() {
		return tvshowYear;
	}


	public void setTvshowYear(String tvshowYear) {
		this.tvshowYear = tvshowYear;
	}


	public String getSearchTerm() {
		return searchTerm;
	}


	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}


	public String getMediaType() {
		return mediaType;
	}


	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	public String getOrderBy() {
		return OrderBy;
	}


	public void setOrderBy(String orderBy) {
		OrderBy = orderBy;
	}


	public String getOrderByType() {
		return OrderByType;
	}


	public void setOrderByType(String orderByType) {
		OrderByType = orderByType;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getSearchGen() {
		return searchGen;
	}

	public void setSearchGen(String searchGen) {
		this.searchGen = searchGen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}


	
	
}