package ejb.bean;

import java.util.List;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.Movie;
import data.TVShow;
import data.TVShowEpisode;
import data.User;

@Stateless
public class AdminBean {
	@PersistenceContext(name = "Proj2Database")
	EntityManager em;
	private Logger logger = LoggerFactory.getLogger(AdminBean.class);

	public AdminBean() {
	}



	public void AdicionarFilme(String title, int year,
			String[] genre, double rating, String sinopse, String director,
			int runtime, String urlCoverImage, String MPAA,
			String fileLocation, String wallpaper) {
		
		Movie m = new Movie()
							.Title(title)
							.Year(year)
							.Genre(genre)
							.Rating(rating)
							.Sinopse(sinopse)
							.Director(director)
							.Runtime(runtime)
							.URLCoverImage(urlCoverImage)
							.MPAA(MPAA)
							.FileLocation(fileLocation)
							.Wallpaper(wallpaper);
		em.persist(m);
	}
	public void AdicionarSerie(String title, int year,
				String[] genre, double rating, String sinopse, String director,
				int runtime, String network, String urlCoverImage, String MPAA){
		
		TVShow t = new TVShow()
							.Title(title)
							.Year(year)
							.Genre(genre)
							.Rating(rating)
							.Sinopse(sinopse)
							.Director(director)
							.Runtime(runtime)
							.Network(network)
							.URLCoverImage(urlCoverImage)
							.MPAA(MPAA);
		em.persist(t);
	}
	
	public void AdicionarEpisodioSerie(int tvShowID, String title, int year,
			String[] genre, int season, int episode, double rating, String sinopse,
			int runtime, String urlCoverImage, String fileLocation, String wallpaper){
		
		TVShow t = em.find(TVShow.class, tvShowID);
		List<TVShowEpisode> le = t.getEpisodes();
		
		if ( SerieContemEpisodioComSeasonEpisodio(tvShowID, season, episode) )
			throw new EJBTransactionRolledbackException();
		
		TVShowEpisode e = new TVShowEpisode()
										.Title(title)
										.Year(year)
						                .Genre(genre)
						                .Season(season)
						                .Episode(episode)
						                .Rating(rating)
						                .Sinopse(sinopse)
						                .Runtime(runtime)
						                .URLCoverImage(urlCoverImage)
						                .FileLocation(fileLocation)
						                .Wallpaper(wallpaper)
						                .TVShow(t);
		
		le.add(e);
		em.persist(e);
	}
	
	
	private boolean SerieContemEpisodioComSeasonEpisodio(int tvShowID, int season, int episode) {
		Query q = em.createQuery("Select Count(e.id) FROM TVShowEpisode e WHERE e.t.id = :showID and season=:season and episode=:episode");
		q.setParameter("showID", tvShowID);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		
		return (long) q.getSingleResult() != 0;
	}



	public void EditarFilme(int movieID, String title, int year,
			String[] genre, double rating, String sinopse, String director,
			int runtime, String urlCoverImage, String MPAA,
			String fileLocation, String wallpaper) {
		
		Movie m = em.find(Movie.class, movieID);
		m.Title(title)
			.Year(year)
			.Genre(genre)
			.Rating(rating)
			.Sinopse(sinopse)
			.Director(director)
			.Runtime(runtime)
			.URLCoverImage(urlCoverImage)
			.MPAA(MPAA)
			.FileLocation(fileLocation)
			.Wallpaper(wallpaper);
	}
	public void EditarSerie(int tvShowID, String title, int year,
				String[] genre, double rating, String sinopse, String director,
				int runtime, String network, String urlCoverImage, String MPAA){
		
		TVShow t = em.find(TVShow.class, tvShowID);
		t.Title(title)
			.Year(year)
			.Genre(genre)
			.Rating(rating)
			.Sinopse(sinopse)
			.Director(director)
			.Runtime(runtime)
			.Network(network)
			.URLCoverImage(urlCoverImage)
			.MPAA(MPAA);
		
	}
	
	public void EditarEpisodioSerie(int episodeID, int tvShowID, String title, int year,
			String[] genre, int season, int episode, double rating, String sinopse,
			int runtime, String urlCoverImage, String fileLocation, String wallpaper){
		
		TVShow t = em.find(TVShow.class, tvShowID);
		
		TVShowEpisode e = em.find(TVShowEpisode.class, episodeID);
		
		if ( SerieContemEpisodioComSeasonEpisodio(tvShowID, season, episode, e.getId()) )
			throw new EJBTransactionRolledbackException();
		
		
		e.Title(title)
			.Year(year)
	        .Genre(genre)
	        .Season(season)
	        .Episode(episode)
	        .Rating(rating)
	        .Sinopse(sinopse)
	        .Runtime(runtime)
	        .URLCoverImage(urlCoverImage)
	        .FileLocation(fileLocation)
	        .Wallpaper(wallpaper)
	        .TVShow(t);

	}
	
	private boolean SerieContemEpisodioComSeasonEpisodio(int tvShowID,
			int season, int episode, int currentID) {
		Query q = em.createQuery("Select e.id FROM TVShowEpisode e WHERE e.t.id = :showID and season=:season and episode=:episode");
		q.setParameter("showID", tvShowID);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		
		return  (int)q.getSingleResult() != currentID;
	}



	@SuppressWarnings("unchecked")
	public void EliminarFilme(int id) {
		Movie m = em.find(Movie.class, id);
		Query q = em.createQuery("SELECT u FROM User u");
		List<User> lu = q.getResultList();
		List<Movie> lm;
		
		for (User u : lu){
			lm = u.getWatchListMovie();
			if ( lm.contains(m) ){
				lm.remove(m);
				u.setWatchListMovie(lm);
			}
		}
		em.remove(m);
	}
	
	@SuppressWarnings("unchecked")
	public List<TVShowEpisode> getTVShowEpisodes(int id){
		Query q = em.createQuery("Select t.episodes from TVShow t where t.id = :id");
		q.setParameter("id", id);
		q.setMaxResults(1);
		
		return q.getResultList();
	}
	
	public void EliminarSerie(int id) {
		TVShow t = em.find(TVShow.class, id);
		//Query q = em.createQuery("SELECT u FROM User u");
		//List<User> lu = q.getResultList();
		List<TVShowEpisode> seriesEpisodes = t.getEpisodes();
		
		if ( seriesEpisodes == null ){
			seriesEpisodes = getTVShowEpisodes(id);
		}
		
		for(TVShowEpisode e: seriesEpisodes){
			EliminarEpisodio(e.getId());
		}
		
		
		em.remove(t);
	}
	
	@SuppressWarnings("unchecked")
	public void EliminarEpisodio(int id) {
		TVShowEpisode e = em.find(TVShowEpisode.class, id);
		Query q = em.createQuery("SELECT u FROM User u");
		List<User> lu = q.getResultList();
		List<TVShowEpisode> temp;
		logger.debug("A eliminar episodio " + e.getT().getTitle()+"S"+ e.getSeason() +"E"+e.getEpisode());
		for (User u : lu){
			temp = u.getWatchListTV();
			if ( temp!=null && temp.contains(e) )
			{
				temp.remove(e);
				u.setWatchListTV(temp);
			}
		}
		q = em.createNamedQuery("TVShow.getTVShowByEpisodeID");
		q.setParameter("ep", e);
		q.setMaxResults(1);
		TVShow t = (TVShow) q.getSingleResult();
		List<TVShowEpisode> le = t.getEpisodes();
		if ( le == null ){
			le = getTVShowEpisodes(t.getId());
		}
		
		le.remove(e);
		t.Episodes(le);
		
		em.remove(e);
	}

	public String getPasswordAndUsername(String email) {
		Query q = em.createQuery("Select password FROM Manager WHERE email=:email");
    	q.setParameter("email", email);
    	q.setMaxResults(1);
    	
    	return (String) q.getSingleResult();
	}



	public Movie getFilme(int id) {
		return em.find(Movie.class, id);
	}
	public TVShow getSerie(int id) {
		return em.find(TVShow.class, id);
		
	}public TVShowEpisode getEpisodio(int id) {
		return em.find(TVShowEpisode.class, id);
	}


	@SuppressWarnings("unchecked")
	public List<Object[]> getAllMoviesIDTitleAndYear() {
		Query q = em.createQuery("SELECT id, title, year FROM Movie ORDER BY title");
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllSeriesIDTitleAndYear() {
		Query q = em.createQuery("SELECT id, title, year FROM TVShow ORDER BY title");
		return q.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllEpisodesIDTitleAndYear() {
		Query q = em.createQuery("SELECT id, title, year FROM TVShowEpisode ORDER BY title");
		return q.getResultList();
	}


	
	
}
