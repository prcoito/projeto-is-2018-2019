package ejb.bean;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.Movie;
import data.TVShow;
import data.TVShowEpisode;
import data.User;


/**
 * Session Bean implementation class UserBean
 */
@Stateless
@LocalBean
public class UserBean {//implements UserBeanInterface{
	@PersistenceContext(name = "Proj2Database")
	EntityManager em;
	
	private Logger logger = LoggerFactory.getLogger(UserBean.class);
	private static int MAX_RANDOM_MOVIES = 5;
	private static int MAX_RANDOM_TVSHOWS = 5;
	
	
    public UserBean() {
        logger.debug("USER BEAN INITIALIZED");
    }
    
    /* implementacoes */
    
    public String[] getPasswordAndUsername(String table, String email){
    	//logger.debug("Email=" + email);
    	Query q;
    	if (table.equals("User"))
    		q = em.createQuery("Select password, username FROM User WHERE email=:email");
    	else
    		q = em.createQuery("Select password, username FROM Manager WHERE email=:email");

    	q.setParameter("email", email);
    	q.setMaxResults(1);
    	
    	Object[] r = (Object[]) q.getSingleResult();
    	
    	return  Arrays.asList(r).toArray(new String[r.length]);
		
    }

    public String register(	String usernameR, String firstName, String lastName, 
    						String email, String passwordR,
    						int creditCardNumber, Date birthDate, String role){
    	
    	
    	User temp;
		try {
			temp = new User()
								.Username(usernameR)
								.FirstName(firstName)
								.LastName(lastName)
								.Email(email)
								.Password(passwordR)
								.CreditCardNumber(creditCardNumber)
								.BirthDate(birthDate);
								// Watch Lists inicializadas no construtor
								// assim como a registration date
								//.Role(role);
			
			em.persist(temp);
			return "OK";
			
		} catch ( EntityExistsException e){
			logger.error("O username inserido já existe.\n" + e + "\n");
			return "O username inserido já existe.";
		} 

    }

    @SuppressWarnings("unchecked")
	public List<Movie> getSomeRandomMovies(){
    	logger.debug("getting first 5 results from movies");
		Query q = em.createNamedQuery("Movie.getRandom");
		q.setMaxResults(MAX_RANDOM_MOVIES);
		return q.getResultList();
		
    }

    @SuppressWarnings("unchecked")
	public List<TVShow> getSomeRandomTV() {
    	logger.debug("getting first 5 results from tvshows");
    	Query q = em.createNamedQuery("TVShow.getRandom");
    	q.setMaxResults(MAX_RANDOM_TVSHOWS);
    	return q.getResultList();
    }
	
	public Movie getMovie(String title, int year)
	{
		Query q = em.createNamedQuery("Movie.getMovieByTitleAndYear");
		q.setParameter("title", title);
		q.setParameter("year", year);
		q.setMaxResults(1);
		return (Movie) q.getSingleResult();
	}
	
	public Movie getMovieByID(int MovieID)
	{
		
		Movie m = em.find(Movie.class, MovieID);
		
		if (m==null)
		{
			logger.debug("Nao encontrou a tentar com query");
			Query q = em.createNamedQuery("Movie.getMovieByID");
			q.setParameter("id", MovieID);
			q.setMaxResults(1);
			m = (Movie) q.getSingleResult();
			
		}
		
		return m;
	}

	public TVShow getTVShow(String title, int year) {
		Query q = em.createNamedQuery("TVShow.getTVShowByTitleAndYear");
		q.setParameter("title", title);
		q.setParameter("year", year);
		q.setMaxResults(1);
		return (TVShow) q.getSingleResult();
		
	}
	public TVShow getTVShowByID(int TVShowID) {
		/*Query q = em.createNamedQuery("TVShow.getTVShowByID");
		q.setParameter("id", TVShowID);
		q.setMaxResults(1);
		return (TVShow) q.getSingleResult();*/
		TVShow t = em.find(TVShow.class, TVShowID);
		return t;
	}
	public TVShow getTVShowByEpisode(TVShowEpisode ep) {
		Query q = em.createNamedQuery("TVShow.getTVShowByEpisodeID");
		q.setParameter("ep", ep);
		q.setMaxResults(1);
		return (TVShow) q.getSingleResult();
	}
	public TVShowEpisode getTVShowEpisodeByID(int id) {
		/*Query q = em.createNamedQuery("TVShowEpisode.getTVShowEpisodeByID");
		q.setParameter("id", id);
		q.setMaxResults(1);
		return (TVShowEpisode) q.getSingleResult();*/
		TVShowEpisode e = em.find(TVShowEpisode.class, id);
		return e;
	}

	public TVShowEpisode getTVShowEpisode(int id, int season, int episode) {
		Query q = em.createNamedQuery("TVShowEpisode.getTVShowEpisodeByShowIDSeasonAndEpisode");
		q.setParameter("showID", id);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		q.setMaxResults(1);
		return (TVShowEpisode) q.getSingleResult();
	}
	public TVShowEpisode getTVShowEpisode(int episodeID) {
		Query q = em.createNamedQuery("TVShowEpisode.getTVShowEpisodeByID");
		q.setParameter("episodeID", episodeID);
		q.setMaxResults(1);
		return (TVShowEpisode) q.getSingleResult();
	}

	
	public boolean userHasMovieInWatchlist(String username, int MovieID) {
		Movie m = em.find(Movie.class, MovieID);
		if (m==null)
			m = getMovieByID(MovieID);
		
		Query q2 = em.createNamedQuery("User.getTotalOfWatchListMembersWithID");
		q2.setParameter("username", username);
		q2.setParameter("movie", m);
		long count = (long) q2.getSingleResult();
		return count>=1;
		
	}
	public boolean userHasTVShowInWatchlist(String username, int id) {
		User u = getUser(username);
		TVShow t = em.find(TVShow.class, id);
		if (t==null)
			t = getTVShowByID(id);
		
		List<TVShowEpisode> le = getEpisodesWatchList(u.getID());
		int count = 0;
		for ( TVShowEpisode e : t.getEpisodes() ){
			if ( le.contains(e) ){
				count++;
			}
			else
				return false;
		}
		
		return ( count == le.size() );
		
	}
	
	public boolean userHasEpisodeInWatchlist(String username, int id) {
		User u = getUser(username);
		TVShowEpisode e = getTVShowEpisode(id);
		List<TVShowEpisode> le = getEpisodesWatchList(u.getID());
		return le.contains(e);
	}
	
	public void addRemoveMovieToWatchList(String username, int movieID) {
		
		User u = getUser(username);
		Movie m = getMovieByID(movieID);
		
		List<Movie> lm = u.getWatchListMovie();
		
		if ( lm.contains(m) )
			lm.remove(m);
		else
			lm.add(m);
		
		//logger.debug("List<Movie> = "+lm.toString());
		//logger.debug("New watchlist :");
		//for ( Movie ms : lm){
		//	logger.debug("Title: "+ms.getTitle());
		//}
		
		u.setWatchListMovie( lm );	
		
		//em.persist(u);
		
	}

	public void addRemoveTVShowToWatchList(String username, int showId) {
		User u = getUser(username);
		TVShow t = getTVShowByID(showId);
		int count = 0;
		
		// tem que se fazer esta query visto que a lista nao é automaticamente carregada
		List<TVShowEpisode> le = getEpisodesWatchList(u.getID());
		for (TVShowEpisode e : t.getEpisodes())
		{
			if ( le.contains(e) )
				count++;
		}
		
		if ( count == 0){
			for (TVShowEpisode e : t.getEpisodes())
			{
				le.add(e);
			}
		}
		else 
		{
			for (TVShowEpisode e : t.getEpisodes())
			{
				le.remove(e);
			}
		}

		//logger.debug("List<TVShowEpisode> = "+le.toString());
		u.setWatchListTV( le );	
		
		//em.persist(u);
	}
	public void addRemoveTVShowEpisodeToWatchList(String username, int epID) {
		User u = getUser(username);
		TVShowEpisode e = getTVShowEpisode(epID);
		
		List<TVShowEpisode> le = u.getWatchListTV();
		if ( le.contains(e) )
			le.remove(e);
		else
			le.add(e);
		
		logger.debug("List<Movie> = "+le.toString());
		u.setWatchListTV( le );	
		
		//em.persist(u);
		
	}
	public User getUser(String username) {
		logger.debug("a obter info user "+username);
		Query q = em.createNamedQuery("User.getUserByUsername");
		q.setParameter("username", username);
		q.setMaxResults(1);
		
		return (User) q.getSingleResult();
	}
	public User getUser(int id) {
		User u = em.find(User.class, id);
		if ( u == null ){
			logger.debug("a obter info user com id "+id);
			Query q = em.createNamedQuery("User.getUserByID");
			q.setParameter("id", id);
			q.setMaxResults(1);			
			u = (User) q.getSingleResult();
		}
		
		return u;
	}

	public User editUser(String oldUsername, String username, String firstName, String lastName,
			String passwordR, String email, int creditCardNumber,
			Date birthDate) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		User u = getUser(oldUsername);
		
		u.setUsername(username);
		u.setFirstName(firstName);
		u.setLastName(lastName);
		if (!passwordR.equals("")){
			u.setPassword(passwordR);
		}
		u.setEmail(email);
		u.setCreditCardNumber(creditCardNumber);
		u.setBirthDate(birthDate);
		
		return u;
	}

	public int getNumberSeasonsTVShow(int id) {
		logger.debug("getNumberSeasonsTVShow");

		Query q = em.createNamedQuery("TVShowEpisode.getNumberSeasonsByShowID");
	
		q.setParameter("showid", id);
		q.setMaxResults(1);
		return Integer.parseInt( (Long.toString((long) q.getSingleResult()) ) );
	}

	public String getEpisodeTitle(int id, int season, int episode) {
		logger.debug("getEpisodeTitle");
		Query q = em.createNamedQuery("TVShowEpisode.getTitleByShowID_NumberSeason_NumberEpisode");
		q.setParameter("showid", id);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		q.setMaxResults(1);
		return (String) q.getSingleResult();
	}

	public String getEpisodeSinopse(int id, int season, int episode) {
		logger.debug("getEpisodeSinopse");
		Query q = em.createNamedQuery("TVShowEpisode.getSinopseByShowID_NumberSeason_NumberEpisode");
		q.setParameter("showid", id);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		q.setMaxResults(1);
		return (String) q.getSingleResult();
	}

	public int getTVShowNumberEpisodesInSeason(int id, int season) {
		logger.debug("getTVShowNumberEpisodesInSeason");
		Query q = em.createNamedQuery("TVShowEpisode.getNumberEpisodesInSeasonByShowID");
	
		q.setParameter("showid", id);
		q.setParameter("season", season);
		q.setMaxResults(1);
		return Integer.parseInt( (Long.toString((long) q.getSingleResult()) ) );
	}
	public String getEpisodeWallpaper(int id, int season, int episode) {
		logger.debug("getEpisodeWallpaper");
		Query q = em.createNamedQuery("TVShowEpisode.getWallpaperByShowID_Season_and_Episode");
		logger.debug("Getting wallpaper for show "+id+" S"+season+"E"+episode);
		q.setParameter("showid", id);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		q.setMaxResults(1);
		//logger.debug("wallpaper = "+q.getSingleResult());
		return (String) q.getSingleResult();
		
	}
	public String getEpisodeID(int id, int season, int episode) {
		logger.debug("getEpisodeID");
		Query q = em.createNamedQuery("TVShowEpisode.getIDByShowID_Season_and_Episode");
		q.setParameter("showid", id);
		q.setParameter("season", season);
		q.setParameter("episode", episode);
		q.setMaxResults(1);
		//logger.debug("Episode id ="+q.getSingleResult());
		return Integer.toString( (int) q.getSingleResult() );
	}
	
	
	public String deleteUser(int id){
		//User u = em.find(User.class, id);
		User u = getUser(id);
		em.remove(u);
		return "OK";
	}
	public int getNumberEpisodes(int id) {
		//TVShow t = em.find(TVShow.class, id);
		//return t.getEpisodes().size();
		Query q = em.createNamedQuery("TVShow.getNumberEpisodes");
		q.setParameter("id", id);
		q.setMaxResults(1);
		return ( (BigInteger) q.getSingleResult() ).intValue();
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public String searchMediaBy(String table, String type, String value, String OrderBy, String OrderType, int page, int recordsPerPage){
		// Table TVShow, TVShowEpisode or Movie
		// Type Categoria, titulo, etc.
		// value valor para categoria
		Query q = em.createQuery("SELECT m FROM :table m WHERE m.:type LIKE %:value%"
				+ "ORDER BY :OrderBy :OrderType LIMIT :start,:offset");


		q.setParameter("table", table);
		q.setParameter("type", type);
		q.setParameter("value", value);
		q.setParameter("OrderBy", OrderBy);
		q.setParameter("OrderType", OrderType);
		q.setParameter("start", (page-1)*recordsPerPage);
		q.setParameter("offset", recordsPerPage);
		
		List<Object> r = q.getResultList();
		return "TODO";
	}

	@SuppressWarnings("unchecked")
	public List<Object>  getResultList(String searchTerm) {
		Query qm = em.createNamedQuery("Movie.SearchByTitle");
		Query qt = em.createNamedQuery("TVShow.SearchByTitle");
	
		qm.setParameter("mTitle", "%"+searchTerm+"%");
		qt.setParameter("tTitle", "%"+searchTerm+"%");
		qm.setMaxResults(10);
		qt.setMaxResults(10);
		
		ArrayList<Object> r = new ArrayList<Object>();
		List<Movie> lm = (List<Movie>) qm.getResultList();
		List<TVShow> lt = (List<TVShow>) qt.getResultList() ;
		
		r.add(lm);
		r.add(lt);

		logger.debug("Returning results...");
		return r;
		
	}

	@SuppressWarnings("unchecked")
	public List<Object> searchMediaBy(String searchType, String searchTitle, String searchDirector, String searchGen,
			String searchStartYear, String searchEndYear, String searchOrderBy,
			String searchOrderByOrder, int searchPage,
			int searchMaxResultsPerPage) {
		
		ArrayList<Object> toReturn = new ArrayList<>();
		String alias;
		String query = "";
		
		switch (searchType) {
			case "ALL":
				int totalResults = 0;
				List<Object> list = searchMediaBy("Movie", searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear, searchOrderBy,
														searchOrderByOrder, searchPage, searchMaxResultsPerPage);
				toReturn.add(0, list.get(0));
				totalResults += ( (List<Object[]>) list.get(0)).size();
				if ( totalResults < searchMaxResultsPerPage )
					list = searchMediaBy("TVShow", searchTitle, searchDirector,  searchGen, searchStartYear, searchEndYear, searchOrderBy,
											searchOrderByOrder, searchPage, searchMaxResultsPerPage-totalResults);
				else
				{
					toReturn.add(1, null);
					toReturn.add(2, null);
					return toReturn;
				}
				
				toReturn.add(1, list.get(1));
				totalResults += ( (List<Object[]>) list.get(1)).size();
				
				if ( totalResults < searchMaxResultsPerPage )
					list = searchMediaBy("TVShowEpisode",	searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear, searchOrderBy,
													searchOrderByOrder, searchPage, searchMaxResultsPerPage-totalResults);
				else{
					toReturn.add(2, null);
					return toReturn;
				}
				
				toReturn.add(2, list.get(2));
				return toReturn;
			case "Movie":
				query += "Select m.id, m.urlCoverImage, m.title, m.year, m.sinopse FROM Movie m Where true ";
				alias = "m";
				break;
			case "TVShow":
				query += "Select t.id, t.urlCoverImage, t.title, t.year, t.sinopse FROM TVShow t Where true ";
				alias = "t";
				break;
			case "TVShowEpisode":
				query += "Select e.id, e.urlCoverImage, e.title, e.year, e.sinopse FROM TVShowEpisode e Where true ";
				alias = "e";
				break;
			default:
				return null;
		}
		
		if (searchTitle !=null &&  !searchTitle.equals("")){
			searchTitle = searchTitle.toUpperCase();
			query += "and UPPER("+alias+".title) LIKE '%"+ searchTitle + "%' ";
		}
		
		if (searchDirector!=null && !searchDirector.equals("")){
			searchDirector = searchDirector.toUpperCase();
			query += "and UPPER("+alias+".director) LIKE '%"+ searchDirector + "%' ";
		}
	
		if (searchGen !=null &&  !searchGen.equals("")){
			searchGen = searchGen.toUpperCase();
			query += "and UPPER("+alias+".genre) LIKE '%"+ searchGen + "%' ";
		}
		
		if (searchStartYear !=null && !searchStartYear.equals("")){
			try{
				Integer.parseInt(searchStartYear);
				query += "and "+alias+".year>=" + searchStartYear + " ";
			} catch (Exception e){}
		}
		if (searchEndYear !=null && !searchEndYear.equals("")){
			try{
				Integer.parseInt(searchStartYear);
				query += "and "+alias+".year<=" + searchEndYear + " ";
			} catch (Exception e){}
		}
		
		query += "Order By " + searchOrderBy + " " + searchOrderByOrder + " "; 
		
		query += "LIMIT "+ (searchPage-1)*searchMaxResultsPerPage + "," + searchMaxResultsPerPage + " ";
		
		logger.debug("Final query = "+ query);
		Query q = em.createNativeQuery(query);
		//q.getResultList();
		
		switch (searchType) {
			case "ALL":
				break;
			case "Movie":
				toReturn.add(0, q.getResultList());
				toReturn.add(1, null);
				toReturn.add(2, null);
				break;
			case "TVShow":
				toReturn.add(0, null);
				toReturn.add(1, q.getResultList());
				toReturn.add(2, null);
				break;
			case "TVShowEpisode":
				toReturn.add(0, null);
				toReturn.add(1, null);
				toReturn.add(2, q.getResultList());
				break;
			default:
				break;
		}
		return toReturn;
	}

	public int getTotalResults(String searchType, String searchTitle, String searchDirector, String searchGen,
			String searchStartYear, String searchEndYear) {
		int toReturn = 0;
		String alias;
		String query = "";
		
		switch (searchType) {
			case "ALL":
				toReturn += getTotalResults("Movie", searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear);
				toReturn += getTotalResults("TVShow", searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear);
				toReturn += getTotalResults("TVShowEpisode", searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear);
				return toReturn;
			case "Movie":
				query += "Select COUNT(m.id) FROM Movie m Where true ";
				alias = "m";
				break;
			case "TVShow":
				query += "Select COUNT(t.id) FROM TVShow t Where true ";
				alias = "t";
				break;
			case "TVShowEpisode":
				query += "Select COUNT(e.id) FROM TVShowEpisode e Where true ";
				alias = "e";
				break;
			default:
				return 0;
		}
		
		if (searchTitle !=null &&  !searchTitle.equals("")){
			searchTitle = searchTitle.toUpperCase();
			query += "and UPPER("+alias+".title) LIKE '%"+ searchTitle + "%' ";
		}
		
		if (searchDirector!=null && !searchDirector.equals("")){
			searchDirector = searchDirector.toUpperCase();
			query += "and UPPER("+alias+".director) LIKE '%"+ searchDirector + "%' ";
		}
	
		if (searchGen !=null &&  !searchGen.equals("")){
			searchGen = searchGen.toUpperCase();
			query += "and UPPER("+alias+".genre) LIKE '%"+ searchGen + "%' ";
		}
		
		if (searchStartYear !=null && !searchStartYear.equals("")){
			try{
				Integer.parseInt(searchStartYear);
				query += "and "+alias+".year>=" + searchStartYear + " ";
			} catch (Exception e){}
		}
		if (searchEndYear !=null && !searchEndYear.equals("")){
			try{
				Integer.parseInt(searchStartYear);
				query += "and "+alias+".year<=" + searchEndYear + " ";
			} catch (Exception e){}
		}
		
		Query q = em.createNativeQuery(query);
		//q.getResultList();
		
		switch (searchType) {
			case "ALL":
				break;
			case "Movie":
				toReturn = ( (BigInteger) q.getSingleResult() ).intValue();
				break;
			case "TVShow":
				toReturn = ( (BigInteger) q.getSingleResult() ).intValue();
				break;
			case "TVShowEpisode":
				toReturn = ( (BigInteger) q.getSingleResult() ).intValue();
				break;
			default:
				break;
		}
		return toReturn;
	}

	public List<Movie> getMovieWatchList(int id) {
		User u = getUser(id);
		logger.debug("Printing movie watch list:");
		
		Query q = em.createNamedQuery("User.getMovieWatchList");
		q.setParameter("id", id);
		//q.setMaxResults(1);
		
		List<Movie> lm = q.getResultList();
		return lm;
		/*
		 * Tem que ser com query. u.getWatchListMovie(); devolvia 5 vezes o mesmo filme
		 * 
		for (Movie m : u.getWatchListMovie()){
			logger.debug("title: "+m.getTitle());
		}
		return u.getWatchListMovie();
		*/
	}

	public List<TVShowEpisode> getEpisodesWatchList(int id) {
		User u = getUser(id);
		// tem que se fazer esta query visto que a lista nao é automaticamente carregada
		Query q  = em.createNamedQuery("User.getTVShowWatchlist");
		q.setParameter("id", u.getID());
		return q.getResultList();
	}


	
}
