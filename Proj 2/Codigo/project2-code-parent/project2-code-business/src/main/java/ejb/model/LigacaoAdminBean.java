package ejb.model;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBTransactionRolledbackException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.Movie;
import data.TVShow;
import data.TVShowEpisode;
import ejb.bean.AdminBean;

public class LigacaoAdminBean {
	private static final long serialVersionUID = 5590830L;

	private Logger logger = LoggerFactory.getLogger(LigacaoAdminBean.class);

	private AdminBean ab;
	public LigacaoAdminBean() {
	}
	
	
	public String AdicionarFilme(String title, String year,
			String genre, String rating, String sinopse, String director,
			String runtime, String urlCoverImage, String MPAA,
			String fileLocation, String wallpaper) {
		
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		logger.debug("A adicionar media...");
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			logger.debug("Ano inserido invalido.");
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
			if ( parsedRating < 0 || parsedRating > 10)
				return "Rating inserido invalido.";
		} catch(NumberFormatException e){
			logger.debug("Rating inserido invalido.");
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			logger.debug( "Runtime inserido invalido.");
			return "Runtime inserido invalido.";
		}
		
		try{
			getAdminBean().AdicionarFilme(	title, parsedYear, genre.split(","), parsedRating, 
											sinopse, director, parsedRuntime, urlCoverImage,
											MPAA, fileLocation, wallpaper);
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe um filme com titulo e ano inseridos");
			return "Já existe um filme com titulo e ano inseridos";
		} catch(Exception e){
			e.printStackTrace();
			logger.debug("Ocorreu um erro inesperado.");
			return "Ocorreu um erro inesperado.";
		}
		
		logger.debug("Media adicionada com sucesso");
		return "OK";
	}
	
	public String AdicionarSerie(String title, String year,
			String genre, String rating, String sinopse, String director,
			String runtime, String network, String urlCoverImage, String MPAA) {
		
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
			if ( parsedRating < 0 || parsedRating > 10)
				return "Rating inserido invalido.";
			
		} catch(NumberFormatException e){
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			return "Runtime inserido invalido.";
		}
		
		try{
			getAdminBean().AdicionarSerie(	title, parsedYear, genre.split(","), parsedRating, 
											sinopse, director, parsedRuntime, network, urlCoverImage,
											MPAA);
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe uma Serie com titulo e ano inseridos");
			return "Já existe uma Serie com titulo e ano inseridos";
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		return "OK";
	
	}
	
	public String AdicionarEpisodioSerie(String tvShowID, String title, String year,
			String genre, String season, String episode, String rating, String sinopse,
			String runtime, String urlCoverImage, String fileLocation, String wallpaper){
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		int parsedSeason;
		int parsedEpisode;
		
		logger.debug("A adicionar episodio: ");
		logger.debug("tvShowID "+tvShowID);
		logger.debug("title "+title);
		logger.debug("year "+year);
		logger.debug("genre "+genre);
		logger.debug("season "+season);
		logger.debug("episode "+episode);
		logger.debug("rating "+rating);
		logger.debug("sinopse "+sinopse);
		logger.debug("runtime "+runtime);
		logger.debug("urlCoverImage "+urlCoverImage);
		logger.debug("fileLocation "+fileLocation );
		logger.debug("wallpaper "+wallpaper );
		
		
		try{
			parsedSeason = Integer.parseInt(season);
		} catch(NumberFormatException e){
			return "Temporada inserida invalida.";
		}
		try{
			parsedEpisode = Integer.parseInt(episode);
		} catch(NumberFormatException e){
			return "Numero de episodio inserido invalido.";
		}
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
		} catch(NumberFormatException e){
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			return "Runtime inserido invalido.";
		}
		
		try{
			getAdminBean().AdicionarEpisodioSerie(Integer.parseInt(tvShowID), title, parsedYear, genre.split(""), 
													parsedSeason, parsedEpisode, parsedRating, sinopse, parsedRuntime,
													urlCoverImage, fileLocation, wallpaper);
		
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe um episodio com essa temporada e numero de episodio para essa serie");
			return "Já existe um episodio com essa temporada e numero de episodio para essa serie";
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		return "OK";
	}
	

	public String EditarFilme(String id, String title, String year, String genre,
			String rating, String sinopse, String director, String runtime,
			String urlCoverImage, String MPAA, String fileLocation,
			String wallpaper) {
		
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
			if ( parsedRating < 0 || parsedRating > 10)
				return "Rating inserido invalido.";
			
		} catch(NumberFormatException e){
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			return "Runtime inserido invalido.";
		}
		try{
			getAdminBean().EditarFilme(Integer.parseInt(id), title, parsedYear, genre.split(","), parsedRating, 
										sinopse, director, parsedRuntime, urlCoverImage,
										MPAA, fileLocation, wallpaper);
		
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe um filme com titulo e ano inseridos");
			return "Já existe um filme com titulo e ano inseridos";
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		return "OK";
		
		
	}
	
	public String EditarSerie(String id, String title, String year,
			String genre, String rating, String sinopse, String director,
			String runtime, String network, String urlCoverImage, String MPAA) {
		
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
			if ( parsedRating < 0 || parsedRating > 10)
				return "Rating inserido invalido.";
			
		} catch(NumberFormatException e){
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			return "Runtime inserido invalido.";
		}
		
		try{
			getAdminBean().EditarSerie(Integer.parseInt(id), title, parsedYear, genre.split(","), parsedRating, 
											sinopse, director, parsedRuntime, network, urlCoverImage,
											MPAA);
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe uma serie com titulo e ano inseridos");
			return "Já existe uma serie com titulo e ano inseridos";
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		return "OK";
	
	}
	
	public String EditarEpisodioSerie(String id, String tvShowID, String title, String year,
			String genre, String season, String episode, String rating, String sinopse,
			String runtime, String urlCoverImage, String fileLocation, String wallpaper){
		int parsedYear;
		double parsedRating; 
		int parsedRuntime; 
		int parsedSeason;
		int parsedEpisode;
		try{
			parsedSeason = Integer.parseInt(season);
		} catch(NumberFormatException e){
			return "Temporada inserida invalida.";
		}
		try{
			parsedEpisode = Integer.parseInt(episode);
		} catch(NumberFormatException e){
			return "Numero de episodio inserido invalido.";
		}
		try{
			parsedYear = Integer.parseInt(year);
		} catch(NumberFormatException e){
			return "Ano inserido invalido.";
		}
		try{
			parsedRating = Double.parseDouble(rating);
		} catch(NumberFormatException e){
			return "Rating inserido invalido.";
		}
		
		try{
			parsedRuntime = Integer.parseInt(runtime);			
		} catch(NumberFormatException e){
			return "Runtime inserido invalido.";
		}
		
		try{
			getAdminBean().EditarEpisodioSerie(Integer.parseInt(id), Integer.parseInt(tvShowID), title, parsedYear, genre.split(""), 
													parsedSeason, parsedEpisode, parsedRating, sinopse, parsedRuntime,
													urlCoverImage, fileLocation, wallpaper);
		
		} catch(EJBTransactionRolledbackException e){
			logger.error("Já existe um episodio com essa temporada e numero de episodio para essa serie");
			return "Já existe um episodio com essa temporada e numero de episodio para essa serie";
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		return "OK";
	}
	
	public String EliminarFilme(String id) {
		try{
			getAdminBean().EliminarFilme(Integer.parseInt(id) );
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		
		return "OK";
	}
	
	public String EliminarSerie(String id) {
		try{
			getAdminBean().EliminarSerie(Integer.parseInt(id) );
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		
		return "OK";
	}
	
	public String EliminarEpisodio(String id) {
		try{
			getAdminBean().EliminarEpisodio(Integer.parseInt(id) );
		} catch(Exception e){
			e.printStackTrace();
			return "Ocorreu um erro inesperado.";
		}
		
		return "OK";
	}

	public ArrayList<String> getInfo(String type, String id) {
		ArrayList<String> r = new ArrayList<String>();
		logger.debug("Type = " + type + " id " + id);
		try{
			if(type.equals("Movie") ){
				Movie m = getAdminBean().getFilme( Integer.parseInt(id) );
				
				
				r.add(Integer.toString( m.getId() ) );
				r.add(m.getTitle());
				r.add(Integer.toString( m.getYear() ));
				String genre = "";
				for ( String s : m.getGenre()){
					genre+=s+",";
				}
				r.add(genre.subSequence(0, genre.length()-1).toString());
				r.add(Double.toString(m.getRating()));
				r.add(m.getSinopse());
				r.add(m.getDirector());
				r.add(Integer.toString(m.getRuntime()));
				r.add(m.getUrlCoverImage());
				r.add(m.getMPAA());
				r.add(m.getFileLocation());
				r.add(m.getWallpaper());
				
			}
			
			else if ( type.equals("TVShow") ){
				TVShow t = getAdminBean().getSerie(Integer.parseInt(id) );
				
				
				r.add(Integer.toString( t.getId() ) );
				r.add(t.getTitle());
				r.add(Integer.toString( t.getYear() ));
				String genre = "";
				for ( String s : t.getGenre()){
					genre+=s+",";
				}
				r.add(genre.subSequence(0, genre.length()-1).toString());
				r.add(Double.toString(t.getRating()));
				r.add(t.getSinopse());
				r.add(t.getDirector());
				r.add(Integer.toString(t.getRuntime()));
				r.add(t.getUrlCoverImage());
				r.add(t.getMPAA());
				r.add(t.getNetwork());

			}
			else{
				TVShowEpisode e = getAdminBean().getEpisodio(Integer.parseInt(id) );
				
				
				r.add(Integer.toString( e.getId() ) );
				r.add(e.getTitle());
				r.add(Integer.toString( e.getYear() ));
				String genre = "";
				for ( String s : e.getGenre()){
					genre+=s+",";
				}
				r.add(genre.subSequence(0, genre.length()-1).toString());
				r.add(Double.toString(e.getRating()));
				r.add(e.getSinopse());
				r.add(Integer.toString(e.getRuntime()));
				r.add(e.getUrlCoverImage());
				r.add(e.getFileLocation());
				r.add(e.getWallpaper());
				r.add(Integer.toString(e.getSeason()));
				r.add(Integer.toString(e.getEpisode()));
				r.add(Integer.toString(e.getT().getId()));
				

			}
			
		} catch(Exception e){
			e.printStackTrace();
			r.add("Ocorreu um erro inesperado.");
			return r;
		}
		logger.debug("result = " + r.toString());
		return r;
	}
	
	public ArrayList<String> getAllMoviesIDTitleAndYear() {
		List<Object[]> r = getAdminBean().getAllMoviesIDTitleAndYear();
		ArrayList<String> result = new ArrayList<String>();
		logger.debug("Resultados obtidos com sucesso");
		for(Object[] m : r){
			result.add( m[0].toString() +";"+ m[1].toString() +" ("+m[2].toString()+")");
		}
		logger.debug("List ids and titleYaer:");
		logger.debug(result.toString());
		return result;
	}
	
	public ArrayList<String> getAllSeriesIDTitleAndYear() {
		List<Object[]> r = getAdminBean().getAllSeriesIDTitleAndYear();
		ArrayList<String> result = new ArrayList<String>();
		for(Object[] m : r){
			result.add(m[0].toString() +";"+ m[1].toString() +" ("+m[2].toString()+")");
		}
		logger.debug("List ids and titleYaer:");
		logger.debug(result.toString());
		return result;
	}
	
	public ArrayList<String> getAllEpisodesIDTitleAndYear() {
		List<Object[]> r = getAdminBean().getAllEpisodesIDTitleAndYear();
		ArrayList<String> result = new ArrayList<String>();
		for(Object[] m : r){
			result.add( m[0].toString() +";"+ m[1].toString() +" ("+m[2].toString()+")");
		}
		logger.debug("List ids and titleYaer:");
		logger.debug(result.toString());
		return result;
	}
	
	
	
	/*
	 * getters and setters
	 */
	public void setAdminBean(AdminBean ab2) {
		ab = ab2;
	}
	public AdminBean getAdminBean(){
		if ( ab == null )
			SearchForBean();
		return ab;
	}


	private void SearchForBean() {
		try {
			if (ab==null){
				logger.debug("User bean not injected. Trying lookup...");
			
				ab = (AdminBean) new InitialContext().lookup("java:global/project2-code-ear/IS-project2-code-business-0.0.1-SNAPSHOT/AdminBean");
				logger.debug("Lookup sucessful: ab= "+ab);
				logger.debug("ub = "+ab);
			}
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	


	

}
