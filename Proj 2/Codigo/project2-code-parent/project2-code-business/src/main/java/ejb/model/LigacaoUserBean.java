package ejb.model;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJBTransactionRolledbackException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.Movie;
import data.Person;
import data.TVShow;
import data.TVShowEpisode;
import data.User;
import ejb.bean.UserBean;

public class LigacaoUserBean {
	private Logger logger = LoggerFactory.getLogger(LigacaoUserBean.class);
	
	private String username = null;	// ID
	private String email = null; // email and password supplied by the user
	private String password = null;
	private String tipoPessoa = null;
	
	private Movie currentMovie = null;
	private TVShow currentShow = null;
	private TVShowEpisode currentEpisode = null;
	private User user;
	
	
	//@EJB
	
	//@Inject
	private UserBean ub;

	//vars para pesquisa
	private String 	searchType, searchTitle, searchDirector, searchGen, searchStartYear, searchEndYear, searchOrderBy, searchOrderByOrder;
	private int 	searchPage, searchMaxResultsPerPage, searchTotalResults;
	
	/*
	 * Inicio de metodos
	 */

	public LigacaoUserBean() {
	}

	public String getUserMatchesPassword() {
		try{
			logger.debug("checking if pass is correct");
			logger.debug("email = " +email);
			String[] result;
			String role;
			try{
				result = getUb().getPasswordAndUsername("User", email);
				role = "Normal";
			} catch(javax.persistence.NoResultException|javax.ejb.EJBException e){
				result = getUb().getPasswordAndUsername("Manager", email);
				role="Admin";
			}
			
			logger.debug("result = "+result.toString());
			for(Object o : result)
				logger.debug(o.toString());
			String pass = result[0];
			setUsername(result[1]);
			
			password = User.SHA256(password);

			logger.debug("Email = "+email); 
			logger.debug("PasswordGiven = "+password);
			logger.debug("PasswordExpected = "+pass);
			
			logger.debug("pass is correct ?");
			if (pass.equals(this.password))
			{
				logger.debug("pass is correct");
				pass = null;
				password = null;
				return role;
			}
			else
			{
				logger.debug("pass is incorrect");
				pass = null;
				password = null;
				return "WRONGPASS";
			}
		} catch(javax.persistence.NoResultException e){
			logger.error("Erro! User com mail "+email+" nao existe.");
			return "NOUSER";
		}
		catch (NullPointerException e){
			logger.error("NullPointerException: "+e);
			//e.printStackTrace();
			return "NOUSER";
		} catch(javax.ejb.EJBException e){
			logger.error("Erro! User com mail "+email+" nao existe.");
			return "NOUSER";
		}
		
		catch(Exception e){
			logger.error("EXCEPTION NOT CAUGHT: "+ e);
			//e.printStackTrace();
		}
		password = null;
		return "ERROR";
	}
	
	public String register(	String usernameR, String firstName, String lastName, String email,
							String passwordR, String passwordRepeat, String creditCardNumber,
							String birthDate, String role) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String result;
		
		logger.debug("verificacao de pass");
		if (!passwordR.equals(passwordRepeat))
			return "Passwords não correspondem.";
		
		try {
			logger.debug("birthDate = "+ birthDate);
			logger.debug("sdf.parse(birthDate) = "+ sdf.parse(birthDate));
			
			result = getUb().register(usernameR, firstName, lastName, email, passwordR, Integer.parseInt(creditCardNumber), sdf.parse(birthDate), role);
		
		} catch (NumberFormatException e) {
			logger.error("Credit card not correctly parsed\n" + e + "\n");
			return "Credit card not correctly parsed";
		} catch(EJBTransactionRolledbackException e){
			logger.error("Email ou Credit card já em uso\n" + e + "\n");
			return "Email ou Credit card já em uso";
		} catch (ParseException e) {
			logger.error("Invalid Date "+ e + "\n");
			return "Invalid Date";
		} catch (Exception e){
			logger.error("[register] Undetermined Exception.\n" + e +"\n"+e.getCause() + "\n");
			return "Undetermined Exception.";
		}
		logger.error("Tudo correu bem");
		return result;
		
	}
	
	public ArrayList<String> getSomeRandomMovies(){
		try{
			ArrayList<String> movies = new ArrayList<>(); 
			
			List<Movie> r = getUb().getSomeRandomMovies();
			
			for (Movie m : r){
				movies.add(m.getId()+";"+m.getUrlCoverImage()+";" + m.getTitle()+";"+m.getYear());
			}
			return movies;
			
			
		} catch(Exception e){
			logger.error("Undetermined Exception at [getSomeRandomMovies]: " + e);
			return null;
		}
		
	}
	
	public ArrayList<String> getSomeRandomTV(){
		try{
			ArrayList<String> tv = new ArrayList<>(); 
			
			List<TVShow> r = getUb().getSomeRandomTV();
			
			for (TVShow t : r){
				tv.add(t.getId()+";"+t.getUrlCoverImage()+";" + t.getTitle()+";"+t.getYear());
			}
			return tv;
			
			
		} catch(Exception e){
			logger.error("Undetermined Exception at [getSomeRandomTV]: " + e);
			return null;
		}
		
	}
	
	public String getUser(String username_){
		try{
			logger.debug("A obter info do user "+username_);
			this.user = getUb().getUser(username_);
			logger.debug("User info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Exeption at [getUser]: " + e);
		}
		return "ERROR";
	}
	public String getMovie(String title, String year) {
		// titleAndYear = title (year)
		try{
			title = title.replace("&#039;", "'");
			int year_ = Integer.parseInt(year);
			
			logger.debug("Movie title: "+ title);
			logger.debug("Movie year: "+ year);

			this.currentMovie = getUb().getMovie(title, year_);
			logger.debug("Movie info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getMovie]: "+e);
		}
		return "ERROR";
	}
	public String getTVShow(String title, String year) {
		// titleAndYear = title (year)
		try{
			title = title.replace("&#039;", "'");
			int year_ = Integer.parseInt(year);
			
			logger.debug("TVShow title: "+ title);
			logger.debug("TVShow year: "+ year);
			
			
			this.currentShow = getUb().getTVShow(title, year_);
			
			logger.debug("TVShow info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShow]: "+e);
		}
		return "ERROR";
	}
	public String getTVShowEpisode(String episodeID) {
		try{
			logger.debug("TVShowEpisodeID: "+ episodeID);
			
			this.currentEpisode = getUb().getTVShowEpisode(Integer.parseInt(episodeID));
			
			currentShow = getUb().getTVShowByEpisode(currentEpisode);
			
			logger.debug("TVShowEpisode info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShow]: "+e);
		}
		return "ERROR";
	}
	@Deprecated
	public String getTVShowEpisode(String season, String episode) {
		try{
			logger.debug("TVShow id: "+ getIDTVShow());
			logger.debug("TVShow title: "+ getTVShowTitle());
			logger.debug("TVShow year: "+ getTVShowYear());
			logger.debug("Season "+season+" Episode "+episode);
			
			this.currentEpisode = getUb().getTVShowEpisode(currentShow.getId(), Integer.parseInt(season), Integer.parseInt(episode));
			
			logger.debug("TVShowEpisode info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShow]: "+e);
		}
		return "ERROR";
	}
	public String getMovieByID(int id) {
		try{
			logger.debug("Movie ID: "+ id);
			
			this.currentMovie = getUb().getMovieByID(id);
			
			logger.debug("Movie info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getMovieByID]: "+e);
		}
		return "ERROR";
	}
	public String getTVShowEpisodeByID(int id) {
		try{
			logger.debug("TVShowEpisode ID: "+ id);
			
			this.currentEpisode = getUb().getTVShowEpisodeByID(id);
			
			logger.debug("Movie info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getMovieByID]: "+e);
		}
		return "ERROR";
	}
	public String getTVShowByID(int id) {
		try{
			logger.debug("TVShow ID: "+ id);
			
			this.currentShow = getUb().getTVShowByID(id);
			
			logger.debug("Serie info obtida com sucesso");
			return "OK";
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowByID]: "+e);
		}
		return "ERROR";
		
	}
	
	
	/*
	 * Metodos da pagina tvShowPage.jsp 
	 */
	public String getTVShowPoster(){
		try{
			return getCurrentShow().getUrlCoverImage();
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowPoster]: "+e);
			return "";
		}
	}
	public String getTVShowTitle(){
		try{
			return getCurrentShow().getTitle();
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowTitle]: "+e);
			return "";
		}
	}
	public String getTVShowYear(){
		try{
			return Integer.toString(getCurrentShow().getYear());
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowYear]: "+e);
			return "";
		}
	}
	public String getTVShowRating(){
		try{
			return Double.toString(getCurrentShow().getRating());
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowRating]: "+e);
			return "";
		}
	}
	public String getTVShowSinopse(){
		try{
			return getCurrentShow().getSinopse();
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowSinopse]: "+e);
			return "";
		}
	}
	public String getTVShowDirector(){
		try{
			/*String[] dir = getCurrentShow().getDirector();
			String toReturn = dir[0];
			
			
			for (int i=1; i< dir.length ; i++)
				toReturn+=", "+dir[i];
	
			return toReturn;
			*/			
			
			return getCurrentShow().getDirector();
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowDirector]: "+e);
			return "";
		}
	}
	public ArrayList<String> getTVShowCast(){
		try{
			ArrayList<String> toReturn = new ArrayList<String>();
			String[] roles = getCurrentShow().getRoles();
			int index = 0;
			for(Person p : getCurrentShow().getCast()){
				toReturn.add(p.getUrlPersonImage()+";"+p.getName()+";"+roles[index]);
				index++;
			}
			return toReturn;
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowCast]: "+e);
			return new ArrayList<String>();
		}
	}
	public String getTVShowGenre(){
		try{
			String[] genres = getCurrentShow().getGenre();
			String toReturn = genres[0];
			
			
			for (int i=1; i< genres.length ; i++)
				toReturn+=", "+genres[i];
						
			return toReturn;
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowGenre]: "+e);
			return "";
		}
		
	}
	public String getTVShowMPAA(){
		try{
			return getCurrentShow().getMPAA();
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowMPAA]: "+e);
			return "";
		}
	}
	public String getTVShowRuntime(){
		try{
			return Integer.toString(getCurrentShow().getRuntime());
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowRuntime]:"+e);
			return "";
		}
	}
	public String getEpisodeFileLocation(){
		try{
			return getCurrentEpisode().getFileLocation();
		} catch(Exception e){
			logger.error("Unexpected error at [getEpisodeFileLocation]: "+e);
			return "";
		}
	}
	public String getIDTVShow(){
		try{
			return Integer.toString(currentShow.getId());
		
		} catch(Exception e){
			logger.error("Unexpected error at [getIDTVShow]: "+e);
			return "";
		}
	}	

	public int getTVShowNumberSeasons(int id) {
		try{
			int num = getUb().getNumberSeasonsTVShow(id);
			logger.debug("NumSeason = " + num);
			return num;
		
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowNumberSeasons]: "+e);
			return -1;
		}
	}
	public int getTVShowNumberEpisodes(int id) {
		try{
			return getUb().getNumberEpisodes(id);
		
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowNumberEpisodes]: "+e);
			return -1;
		}
	}
	public int getTVShowNumberEpisodesInSeason(int id, int season){
		try{
			return getUb().getTVShowNumberEpisodesInSeason(id, season);
		
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowNumberEpisodesInSeason]: "+e);
			return -1;
		}
	}
	public String getEpisodeTitle(int id, int season, int episode) {
		try{
			return getUb().getEpisodeTitle(id, season, episode);
		
		} catch(Exception e){
			logger.error("Unexpected error at [getEpisodeTitle]: "+e);
			return "";
		}
	}
	public String getEpisodeSinopse(int id, int season, int episode) {
		try{
			String s = getUb().getEpisodeSinopse(id, season, episode);
			//return s;
			if ( s.length() > 150)
				s = s.substring(0, Math.min(s.length(), 150))+"...";
			
			return s;
		
		} catch(Exception e){
			logger.error("Unexpected error at [getIDTVShow]: "+e);
			return "";
		}
	}
	public String getEpisodeWallpaper(int id, int season, int episode) {
		try{
			return getUb().getEpisodeWallpaper(id, season, episode);
		
		} catch(Exception e){
			logger.error("Unexpected error at [getEpisodeWallpaper]: "+e);
			return "";
		}
	}
	public String getEpisodeID() {
		try{
			return Integer.toString(currentEpisode.getId());
		
		} catch(Exception e){
			logger.error("Unexpected error at [getEpisodeID]: "+e);
			return "";
		}
	}
	public String getEpisodeID(int id, int season, int episode) {
		try{
			return getUb().getEpisodeID(id, season, episode);
		
		} catch(Exception e){
			logger.error("Unexpected error at [getEpisodeID]: "+e);
			return "";
		}
	}
	public String getTVShowID(int id) {
		return Integer.toString(id);
	}
	
	/*
	 * Metodos da pagina moviePage.jsp 
	 */
	public String getMoviePoster(){
		try{
			return currentMovie.getUrlCoverImage();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMoviePoster]:" +e);
		} 
		return "";
	}
	public String getMovieTitle(){
		try{
			return currentMovie.getTitle();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieTitle]:" +e);
		} 
		return "";
	}
	public String getMovieYear(){
		try{
			return Integer.toString(currentMovie.getYear());
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieYear]:" +e);
		} 
		return "";
	}
	public String getMovieRating(){
		try{
			return Double.toString(currentMovie.getRating());
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieRating]:" +e);
		} 
		return "";
	}
	public String getMovieSinopse(){
		try{
			return currentMovie.getSinopse();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieSinopse]:" +e);
		} 
		return "";
	}
	public String getMovieDirector(){
		try{
			return currentMovie.getDirector();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieDirector]:" +e);
		} 
		return "";
	}	
	public ArrayList<String> getMovieCast(){
		try{
			ArrayList<String> toReturn = new ArrayList<String>();
		
			String[] roles = currentMovie.getRoles();
			int index = 0;
			for(Person p : currentMovie.getCast()){
				toReturn.add(p.getUrlPersonImage()+";"+p.getName()+";"+roles[index]);
				index++;
			}
			return toReturn;
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieCast]:" +e);
		}
		return new ArrayList<String>();
	}
	public String getMovieGenre(){
		try{
			String[] genres = currentMovie.getGenre();
			String toReturn = genres[0];
			
			for (int i=1; i< genres.length ; i++)
				toReturn+=", "+genres[i];
						
			return toReturn;
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieGenre]:" +e);
		} 
		return "";
	}
	public String getMovieMPAA(){
		try{
			return currentMovie.getMPAA();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieMPAA]:" +e);
		} 
		return "";
	}
	public String getMovieRuntime(){
		try{
			return Integer.toString(currentMovie.getRuntime());
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieRuntime]:" +e);
		} 
		return "";
	}
	public String getMovieFileLocation(){
		try{
			return currentMovie.getFileLocation();
		} catch ( Exception e ){
			logger.debug("ERROR AT [getMovieFileLocation]:" +e);
		} 
		return "";
	}
	public String getIDMovie(){
		try{
			return Integer.toString(currentMovie.getId());
		
		} catch(Exception e){
			logger.error("Unexpected error at [getIDMovie]: "+e);
			return "";
		}
	}
	public String getMovieWallpaper() {
		try{
			return currentMovie.getWallpaper();
		} catch (Exception e){
			logger.error("Unexpected error at [getMovieWallpaper]: "+e);
			return "";
		}
	}
	public String addRemoveMovieToWatchList(int movieId) {
		try{
			getUb().addRemoveMovieToWatchList(getUsername(), movieId);
			
		} catch ( Exception e ){
			logger.error("Unexpected error at [addMovieToWatchList]: "+e);
			return "";
		}
		return "OK";
	}
	public String addRemoveTVShowToWatchList(int TVid) {
		try{
			getUb().addRemoveTVShowToWatchList(getUsername(), TVid);
			
		} catch ( Exception e ){
			logger.error("Unexpected error at [addRemoveTVShowToWatchList]: "+e);
			return "";
		}
		return "OK";
	}
	
	public String addRemoveTVShowEpisodeToWatchList(int epID) {
		try{
			getUb().addRemoveTVShowEpisodeToWatchList(getUsername(), epID);
			
		} catch ( Exception e ){
			logger.error("Unexpected error at [addRemoveTVShowEpisodeToWatchList]: "+e);
			return "";
		}
		return "OK";
	}
	
	// metodos pagina userProfile()
	public String getUserID() {
		try{
			return Integer.toString(user.getID());
		} catch(Exception e ){
			logger.error("Exception at [getUserID]: "+ e);
		}
		return "";
	}

	public String getUserFirstName(){
		try{
			return user.getFirstName();
		} catch(Exception e ){
			logger.error("Exception at [getUserFirstName]: "+ e);
		}
		return "";
	}
	public String getUserLastName(){
		try{
			return user.getLastName();
		} catch(Exception e ){
			logger.error("Exception at [getUserLastName]: "+ e);
		}
		return "";
	}
	public String getUserUserName(){
		try{
			return user.getUsername();
		} catch(Exception e ){
			logger.error("Exception at [getUserUserName]: "+ e);
		}
		return "";
	}
	public String getUserEmail(){
		try{
			return user.getEmail();
		} catch(Exception e ){
			logger.error("Exception at [getUserEmail]: "+ e);
		}
		return "";
	}
	public String getUserCreditCardNumber(){
		try{
			return Integer.toString(user.getCreditCardNumber());
		} catch(Exception e ){
			logger.error("Exception at [getUserCreditCardNumber]: "+ e);
		}
		return "";
	}
	public String getUserBirthDate(){
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			logger.debug("formated date: "+ sdf.format(user.getBirthDate()));
			return sdf.format(user.getBirthDate()) ;
		} catch(Exception e ){
			logger.error("Exception at [getUserBirthDate]: "+ e);
		}
		return "";
	}
	

	public String editUser(String username2, String firstName, String lastName,
			String emailR, String passwordO, String passwordR,
			String passwordRepeat, String creditCardNumber, String birthDate) {
		try {
			logger.debug("input date = "+birthDate);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			logger.debug("date parsed ="+  sdf.parse(birthDate).toString());
			if (user.getPassword().equals(User.SHA256(passwordO))){
				if ( passwordR.equals(passwordRepeat) )
				{
					user = getUb().editUser(user.getUsername(), username2, firstName, lastName,
								passwordR, emailR, Integer.parseInt(creditCardNumber), sdf.parse(birthDate));
					username = user.getUsername();
				}
				else
					return "Novas Passwords Introduzidas nao correspondem.";
				return "OK";
			}
			else
			{
				return "Password Introduzida não está correta.";
			}
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException | NumberFormatException | ParseException e) {
			logger.error("Exception at [editUser]: "+e);
			return "ERRO";
		}
		
	}
	public String deleteUser(int id){
		try{
			return getUb().deleteUser(id);
			
		} catch(Exception e){
			logger.error("Unexpected error at [deleteUser]: "+e);
			return "";
		}
	}
	
	
	// metodo para pagina do tvhow e movies
	public String getMovieWatchListIcon(){ 
		try{
			boolean bool = ub.userHasMovieInWatchlist(username, currentMovie.getId());
			if ( bool ){ // se count == 1 => ja está na watchlist
				logger.debug("Esta na watchlist");
				return "Icons/RemoveWatchListIcon.png";
			}
			else
			{
				logger.debug("Nao esta na watchlist");
				
				return "Icons/AddWatchListIcon.png";
			}
			
		} catch(Exception e){
			logger.error("Unexpected error at [getMovieWatchListIcon]: "+e);
			return "";
		}
		
	}
	
	public String getTVShowWatchListIcon(){
		// ver se tem o tvshow na watchList
		try{
			boolean bool = ub.userHasTVShowInWatchlist(username, currentShow.getId());
			if ( bool ){ // se count == 1 => ja está na watchlist
				logger.debug("Esta na watchlist");
				return "Icons/RemoveWatchListIcon.png";
			}
			else
			{
				logger.debug("Nao esta na watchlist");
				
				return "Icons/AddWatchListIcon.png";
			}
		
		} catch(Exception e){
			logger.error("Unexpected error at [getTVShowWatchListIcon]: "+e);
			return "";
		}			
	}
	
	public String getEpisodeWatchListIcon(){
		try{
			boolean bool = ub.userHasEpisodeInWatchlist(username, currentEpisode.getId());
			if ( bool ){
				logger.debug("Esta na watchlist");
				return "Icons/RemoveWatchListIcon.png";
			}
			else
			{
				logger.debug("Nao esta na watchlist");
				
				return "Icons/AddWatchListIcon.png";
			}
			
		} catch(Exception e){
			logger.error("Unexpected error at [deleteUser]: "+e);
			return "";
		}
	}
	/*
	 * Metodos pagina tvShowEpisode.jsp
	 */
	public String getEpisodeWallpaper(){
		return currentEpisode.getWallpaper();
	}
	public String getEpisodeTitle(){
		return currentEpisode.getTitle();
	}
	public String getEpisodeYear(){
		return Integer.toString(currentEpisode.getYear());
	}
	public String getEpisodeSeason(){
		return Integer.toString(currentEpisode.getSeason());
	}
	public String getEpisodeEpisode(){
		return Integer.toString(currentEpisode.getEpisode());
	}
	public String getEpisodeRating(){
		return Double.toString(currentEpisode.getRating());
	}
	public String getEpisodeSinopse(){
		return currentEpisode.getSinopse();
	}
	public String getEpisodePoster() {
		return currentEpisode.getUrlCoverImage();
	}
	public String getEpisodeRuntime(){
		return Integer.toString(currentEpisode.getRuntime());
	}
	
	// PesquisaPersonalizada
	public String pesquisaPersonalizada(String type, String title, String diretor, String gen,
										String startYear, String endYear, 
										String orderBy, String orderByOrder, 
										int page, int maxResultsPerPage) {
		setSearchType(type);
		setSearchTitle(title);
		setSearchDirector(diretor);
		setSearchGen(gen);
		setSearchStartYear(startYear);
		setSearchEndYear(endYear);
		setSearchOrderBy(orderBy);
		setSearchOrderByOrder(orderByOrder);
		setSearchPage(page);
		setSearchMaxResultsPerPage(maxResultsPerPage);
		setSearchTotalResults(getTotalResults());
		logger.debug("Total Results found: " + getSearchTotalResults() );
		return "OK";
	}

	@Deprecated
	public String pesquisaPersonalizada(int page, int maxResultsPerPage){
		return "TODO";
	}
	// quickSearch
	public String setQuickSearch(String title){
		setSearchType("ALL");
		setSearchTitle(title);
		setSearchDirector("");
		setSearchGen("");
		setSearchStartYear("");
		setSearchEndYear("");
		setSearchOrderBy("Title");
		setSearchOrderByOrder("ASC");
		setSearchPage(1);
		setSearchMaxResultsPerPage(10);
		setSearchTotalResults(getTotalResults());
		logger.debug("Total Results found: " + getSearchTotalResults() );
		return "OK";
	}
	/*
	 *  Metodos pagina resultList
	 */
	public int getTotalResults(){
		return getUb()
					.getTotalResults(	getSearchType(), getSearchTitle(), getSearchDirector(), getSearchGen(), 
										getSearchStartYear(), getSearchEndYear());
	}
	
	

	@SuppressWarnings("unchecked")
	public ArrayList<String> getResultList() {
		logger.debug("Getting results for search tearm "+getSearchTitle());
		try{
			//List<Object> r = getUb().getResultList(getSearchTerm());
			logger.debug("Parametros de pesquisa:");
			logger.debug("Type: "+getSearchType());
			logger.debug("Title: "+getSearchTitle());
			logger.debug("Diretor: "+ getSearchDirector());
			logger.debug("Gen: "+ getSearchGen());
			logger.debug("StartYear: "+getSearchStartYear());
			logger.debug("EndYear:" + getSearchEndYear());
			logger.debug("OrderBy: " +getSearchOrderBy());
			logger.debug("OrderByOrder: "+getSearchOrderByOrder());
			logger.debug("SearchPage: "+getSearchPage());
			logger.debug("MaxResultsPerPage: " +getSearchMaxResultsPerPage());
			
			List<Object> r = getUb()
					.searchMediaBy(	getSearchType(), getSearchTitle(), getSearchDirector(), getSearchGen(), getSearchStartYear(), getSearchEndYear(),
									getSearchOrderBy(), getSearchOrderByOrder(), getSearchPage(), getSearchMaxResultsPerPage());
					
			List<Object[]> lm = (List<Object[]>) r.get(0);
			List<Object[]> lt = (List<Object[]>) r.get(1);
			List<Object[]> le = (List<Object[]>) r.get(2);
			
			ArrayList<String> res = new ArrayList<String>();
			String toAdd;
			
			if(lm!=null){
				for ( Object[] lo : lm){
					toAdd = "Movie";
					for (Object o : lo)
						toAdd += ";" + o;
					
					//logger.debug(toAdd);
					res.add(toAdd);
				}
				
				
			}
			if(lt!=null){
				for ( Object[] lo : lt){
					toAdd = "TVShow";
					for (Object o : lo)
						toAdd += ";" + o;
					
					//logger.debug(toAdd);
					res.add(toAdd);
				}
			}
			if(le!=null){
				for ( Object[] lo : le){
					toAdd = "TVShowEpisode";
					for (Object o : lo)
						toAdd += ";" + o;
					
					//logger.debug(toAdd);
					res.add(toAdd);
				}
			}
			
			if(res.size() == 0)
			{
				logger.debug("Nao encontrou resultado. A adicionar mensagem...");
				res.add("Não foi encontrado nenhum resultado para o titulo "+searchTitle);
			}
			return res;
			
		}  catch(Exception e ){
			logger.error("Exception at [getResultList]: "+ e + " Stack:");
			e.printStackTrace();
		}
		return new ArrayList<String>();
		
	}
	public boolean isLastResultsPage() {
		return getSearchPage() >= ( getSearchTotalResults()/getSearchMaxResultsPerPage() );
	}

	public boolean isFirstResultsPage() {
		return getSearchPage() == 1;
	}
	
	/*
	 * Metodos pagina watchlist
	 */
	public ArrayList<String> getMovieWatchList() {
		try{
			List<Movie> lm = getUb().getMovieWatchList(user.getID());
			ArrayList<String> movies = new ArrayList<String>();
			for (Movie m : lm){
				logger.debug("Adding "+m.getTitle()+" to result");
				movies.add(m.getId()+";"+m.getUrlCoverImage()+";" + m.getTitle()+";"+m.getYear());
			}
			
			if (movies.size() == 0)
				movies.add("Não tem nenhum filme na watchlist.;");
			return movies;
		}
		catch(NullPointerException e){
			logger.debug("Exception "+ e);
			ArrayList<String> movies = new ArrayList<String>();
			movies.add("Não tem nenhum filme na watchlist.;");
			return movies;
		}
		catch(Exception e){
			logger.debug("Exception "+ e);
			ArrayList<String> movies = new ArrayList<String>();
			movies.add("Não tem nenhum filme na watchlist.;");
			return movies;
			
		}
		//return null;
	}

	public ArrayList<String> getEpisodesWatchList() {
		try{
			List<TVShowEpisode> le = getUb().getEpisodesWatchList(user.getID());
			ArrayList<String> episodes = new ArrayList<String>();
			for (TVShowEpisode e : le){
				episodes.add(e.getId()+";"+e.getUrlCoverImage()+";" + e.getTitle()+";"+e.getYear());
			}
			
			if (episodes.size() == 0)
				episodes.add("Não tem nenhum serie na watchlist.;");
			return episodes;
		}
		catch(NullPointerException e){
			logger.debug("Exception "+ e);
			ArrayList<String> episodes = new ArrayList<>();
			episodes.add("Não tem nenhuma serie na watchlist.;");
			return episodes;
		}
		catch(Exception e){
			logger.debug("Exception "+ e);
			ArrayList<String> episodes = new ArrayList<>();
			episodes.add("Não tem nenhuma serie na watchlist.;");
			return episodes;
			
		}
	}

	
	
	
	// Getters e Setters
	public void setEmail(String mail) {
		this.email = mail;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setTipoPessoa(String string){
		this.tipoPessoa = string;
	}
	public String getTipoPessoa() {
		return tipoPessoa;
	}
	public Movie getCurrentMovie() {
		return currentMovie;
	}
	public void setCurrentMovie(Movie currentMovie) {
		this.currentMovie = currentMovie;
	}
	public TVShow getCurrentShow() {
		return currentShow;
	}
	public void setCurrentShow(TVShow currentShow) {
		this.currentShow = currentShow;
	}
	public TVShowEpisode getCurrentEpisode() {
		return currentEpisode;
	}
	public void setCurrentEpisode(TVShowEpisode currentEpisode) {
		this.currentEpisode = currentEpisode;
	}
	public String getUsername() {
		return username;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserBean getUb() {
		return ub;
	}

	public void setUb(UserBean ub2) {
		this.ub = ub2;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchTitle() {
		return searchTitle;
	}

	public void setSearchTitle(String searchTitle) {
		this.searchTitle = searchTitle;
	}

	public int getSearchMaxResultsPerPage() {
		return searchMaxResultsPerPage;
	}

	public void setSearchMaxResultsPerPage(int maxResultsPerPage) {
		this.searchMaxResultsPerPage = maxResultsPerPage;
	}

	public int getSearchPage() {
		return searchPage;
	}

	public void setSearchPage(int page) {
		this.searchPage = page;
	}

	public String getSearchOrderBy() {
		return searchOrderBy;
	}

	public void setSearchOrderBy(String searchOrderBy) {
		this.searchOrderBy = searchOrderBy;
	}

	
	public String getSearchStartYear() {
		return searchStartYear;
	}

	public void setSearchStartYear(String searchStartYear) {
		this.searchStartYear = searchStartYear;
	}

	public String getSearchEndYear() {
		return searchEndYear;
	}

	public void setSearchEndYear(String searchEndYear) {
		this.searchEndYear = searchEndYear;
	}

	public String getSearchOrderByOrder() {
		return searchOrderByOrder;
	}

	public void setSearchOrderByOrder(String searchOrderByOrder) {
		this.searchOrderByOrder = searchOrderByOrder;
	}

	public int getSearchTotalResults() {
		return searchTotalResults;
	}

	public void setSearchTotalResults(int searchTotalResults) {
		this.searchTotalResults = searchTotalResults;
	}

	public String getSearchGen() {
		return searchGen;
	}

	public void setSearchGen(String searchGen) {
		this.searchGen = searchGen;
	}

	private void setSearchDirector(String diretor) {
		this.searchDirector = diretor;
		
	}
	public String getSearchDirector() {
		return this.searchDirector;
	}

}