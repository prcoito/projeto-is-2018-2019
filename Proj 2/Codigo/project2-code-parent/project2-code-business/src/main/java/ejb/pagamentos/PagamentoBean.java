package ejb.pagamentos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Future;

import javax.annotation.Resource;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.User;

@Stateless
public class PagamentoBean {
	@Resource(name="java:/jboss/mail/gmail")
	private Session session;
	private static final Logger logger = LoggerFactory.getLogger(PagamentoBean.class);
	@PersistenceContext(name = "Proj2Database")
	EntityManager em;
	
	// apenas para testes 
	int mes = 1;
	boolean change = true;
	
	public PagamentoBean() {
	}

	//@Schedule // para cada dia à meia-noite
	@Schedule(hour="*", minute="*") // para cada minuto ( ou seja 1 min corresponde a um dia )
									
	public void pagar(){
		Random r = new Random();
		
		
		/* casos para quando corre de dia a dia
		int dia = Calendar.getInstance().get(Calendar.DAY_OF_MONTH); 
		int mes = Calendar.getInstance().get(Calendar.MONTH);
		*/
		// caso de teste
		int dia = Calendar.getInstance().get(Calendar.MINUTE);
		if (dia > 30)
		{
			dia = (dia %30) +1; // assume-se que um mes tem sempre 30 dias
			if ( change )
			{
				mes = (mes % 12) + 1;
				change = false;
			}
		}
		else
			change = true;
		
		Query q = em.createQuery("SELECT firstName,lastName, email FROM User "
								+ "WHERE DAY(registrationDate)=:dia and MONTH(registrationDate)=:mes");
		
		q.setParameter("dia", dia);
		q.setParameter("mes", mes);	// mes+1
		
		logger.debug("Dia = "+dia );
		logger.debug("Mes = "+mes );
		
		//mes = (mes % 12) + 1;
		
		List<Object[]> lu = q.getResultList();
		
		// usado nextBoolean para que em +- metade das vezes desse erro ( efeitos de teste )
		for(Object[] u : lu){
			logger.debug("Sending email to user "+ u[0]);
			sendEmail((String)u[0], (String)u[1], (String)u[2], new GregorianCalendar(2018, mes, dia).getTime(), r.nextBoolean() );
		}
		
	}

	
	@Asynchronous								
	public Future<String> sendEmail(String firstName,String lastName, String email, Date timeStamp, boolean sucesso) {
		String status; 
		
		// visto que os emails dos users sao ficticios
		email = "projetois2018@gmail.com";
		
		try {
			logger.info("----------------------------------> Session = " + session);
			Message message = new MimeMessage(session);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
			message.setSubject("WebFlix - Informação de Pagamento");
			message.setHeader("X-Mailer", "JavaMail");

			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy"); //getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
			// caso nao fosse de teste usaria-se este timeStamp
			//Calendar timeStamp = Calendar.getInstance();
			
			String messageBody = "Caro utilizador "+firstName+" "+lastName+"\n\n";
			messageBody+="Serve o presente email para confirmar que o pagamento da sua conta do Webflix ";
			if ( sucesso ){
				messageBody += "foi realizado com sucesso no dia "+dateFormatter.format(timeStamp);
			}
			else
				messageBody += "NÃO foi realizado com sucesso no dia "+dateFormatter.format(timeStamp);
			
			messageBody+="\n\n";
			messageBody+="WebFlix" ;
			
			message.setText(messageBody);
			message.setSentDate(timeStamp);
			Transport.send(message);
			
			status = "Sent";
			logger.info("Info de pagamento enviada para o email "+ email);
		}
		catch (MessagingException ex) { 
			logger.error("Error in sending message.");
			status = "Encountered an error: " + ex.getMessage();
			logger.error(ex.getMessage());
		}
		return new AsyncResult<>(status);
	}
}
